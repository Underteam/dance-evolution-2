#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("sIZkZGVtZJHlCGapivRkATmbnmiEuu5lqwPBv443AQBg4HEfw06J8R9pPV6VXhUWeB7uf2NyBcPXaucdyxZ4WXp2pfhPjlif/RJk4WN3zJZD8XJRQ351eln1O/WEfnJycnZzcOuPnJxxc76kk3N/IB4eXvcYX6ikCc8yg/TQ8TwVQSZTolyWd/SFW7GB07Fyk9LOIC4GMcdF2LnL0gYeK4CFiPKqiZPa+57FYeK8DX2Z3jo6M8G9JCtJVvf1I4UuNs28dxz/1pbxcnxzQ/FyeXHxcnJz1Ndh2MUHDWWF4ZGZJTBBF00WG6gmYptY5VZGSyyNRQuaT+INvVbOro5tQRaht+0W0VX/F0QsnHKa7gniPPZYU8CMA48TkrsRLCvhKnFwcnNy");
        private static int[] order = new int[] { 10,6,13,8,10,8,13,12,11,11,13,13,12,13,14 };
        private static int key = 115;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
