﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;

[CustomEditor(typeof(AdsSettings))]
public class AdsSettingsInspector : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		return;

		AdsSettings settings = (AdsSettings)target;
		if(settings == null) return;

		List<string> scenesList = settings.scenesList;
		if(scenesList == null) scenesList = new List<string>();
		scenesList.Clear ();
		var scenes = UnityEditor.EditorBuildSettings.scenes;
		foreach (var scene in scenes) {
			string name = scene.path.Substring(scene.path.LastIndexOf('/')+1);
			name = name.Substring(0,name.Length-6);
			scenesList.Add(name);
		}

		List<bool> showAds = settings.showAds;
		if (showAds.Count < scenesList.Count) {
			for(int i = showAds.Count; i < scenesList.Count; i++)
				showAds.Add(true);
		}
		if (showAds.Count > scenesList.Count) {
			for(int i = showAds.Count-1; i >= scenesList.Count; i--)
				showAds.RemoveAt(i);
		}

		List<bool> showTopBanner = settings.showTopBanner;
		if (showTopBanner.Count < scenesList.Count) {
			for(int i = showTopBanner.Count; i < scenesList.Count; i++)
				showTopBanner.Add(true);
		}
		if (showTopBanner.Count > scenesList.Count) {
			for(int i = showTopBanner.Count-1; i >= scenesList.Count; i--)
				showTopBanner.RemoveAt(i);
		}

		RestorePrefs (settings);

        GUILayout.Space (10);

		settings.showAdsOnLoad = EditorGUILayout.Toggle("Show ads on scene loaded", settings.showAdsOnLoad);

		GUILayout.Space (10);

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField ("Scene\tAds on reload\tBanner");
		EditorGUILayout.EndHorizontal();

		GUILayout.Space (10);

		if (settings.showAdsOnLoad) {

			for (int i = 0; i < scenesList.Count; i++) {
				EditorGUILayout.BeginHorizontal();
				showAds [i] = EditorGUILayout.Toggle (scenesList [i], showAds [i]);
				showTopBanner [i] = EditorGUILayout.Toggle ("", showTopBanner [i]);
				EditorGUILayout.EndHorizontal();
			}

			PlayerPrefs.SetInt ("showAdsOnLoad", 1);

		} else PlayerPrefs.SetInt ("showAdsOnLoad", 0);

		PlayerPrefs.SetInt ("NumScenes", scenesList.Count);
		for (int i = 0; i < showAds.Count; i++) {
			if (showAds [i])
				PlayerPrefs.SetInt ("showOnScene" + i, 1);
			else
				PlayerPrefs.SetInt ("showOnScene" + i, 0);
		}

		StorePrefs (settings);
	}

	private void StorePrefs(AdsSettings settings) {
	
		//PlayerPrefs.SetInt ("Ads.AdsProvider", (int)settings.adsProvider);

		StoreBool ("Ads.showAdsOnReload", settings.showAdsOnLoad);
		
		for (int i = 0; i < settings.showAds.Count; i++) {
			StoreBool ("Ads.showAds" + i, settings.showAds [i]);
		}

		for (int i = 0; i < settings.showTopBanner.Count; i++) {
			StoreBool ("Ads.showTopBanner" + i, settings.showTopBanner [i]);
		}

	}

	private void RestorePrefs(AdsSettings settings) {

		//settings.adsProvider = (AdsController.AdsProvider)PlayerPrefs.GetInt ("Ads.AdsProvider");

		settings.showAdsOnLoad = RestoreBool ("Ads.showAdsOnReload");

		for (int i = 0; i < settings.showAds.Count; i++) {
			settings.showAds [i] = RestoreBool ("Ads.showAds" + i);
		}

		for (int i = 0; i < settings.showTopBanner.Count; i++) {
			settings.showTopBanner [i] = RestoreBool ("Ads.showTopBanner" + i);
		}

	}

	private void StoreBool(string key, bool b) {
	
		if (b)
			PlayerPrefs.SetInt (key, 1);
		else
			PlayerPrefs.SetInt (key, 0);
	}

	private bool RestoreBool(string key) {

		return PlayerPrefs.GetInt (key, 1) == 1;
	}
}
#endif