﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

public class DummyAd : MonoBehaviour {

	private float timer;

	public Text lblTimer;

	[System.Serializable]
	public class OnClose : UnityEvent<bool> {}
	public OnClose onClose;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Show (float t) {
	
		gameObject.SetActive (true);

		timer = t;

		if (lblTimer != null)
			lblTimer.gameObject.SetActive (false);

		if (t > 0)
			StartCoroutine (Timer ());
	}

	public void Close () {
	
		if (onClose != null)
			onClose.Invoke (timer <= 0);

		gameObject.SetActive (false);
	}

	private IEnumerator Timer () {
	
		if (lblTimer != null)
			lblTimer.gameObject.SetActive (true);
		
		while (timer > 0) {
			timer -= Time.deltaTime;
			if (lblTimer != null)
				lblTimer.text = "" + (int)timer;
			yield return null;
		}

		if (lblTimer != null)
			lblTimer.gameObject.SetActive (false);
	}
}
