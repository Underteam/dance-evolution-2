﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GMADownloader : MonoBehaviour {

	private static GMADownloader instance;

	public static GMADownloader Instance() {

		//Debug.LogWarning ("[GMADownloader]");

		if (instance == null) {
			instance = FindObjectOfType<GMADownloader> ();
		}

		if (instance == null) {

			GameObject go = new GameObject("Downloader");
			instance = go.AddComponent<GMADownloader>();
		}

		return instance;
	}

	void Awake() {

		//Debug.LogWarning ("[GMADownloader] Awake " + (instance != null));

		if (instance != null && instance != this) Destroy(this);
		else instance = this;
	}

	private static Queue queue = new Queue();
	private Queue downloadingQueue = Queue.Synchronized (queue);

	private List<string> downloadingFiles = new List<string>();

	private class DownloadParams {
		public string url;
		public Action<object> onDownloadFinished = null;
		public Action<object> onDownloadError = null;

		public DownloadParams (string url, Action<object> onDownloadFinished, Action<object> onDownloadError = null) {
			this.url = url;
			this.onDownloadFinished = onDownloadFinished;
			this.onDownloadError = onDownloadError;
		}

		float currentProgress;
		float progress = -0.1f;
		public void SetProgress(float p) {

			currentProgress = p;
			if (p >= progress + 0.1f || p < progress) {
				GMALogger.Log ("[Downloader " + this.GetHashCode() + "] download progress " + (100 * p) + "%");
				progress = (float)((int)(10*p))/10.0f;
			}
		}

		public float GetProgress() {

			return currentProgress;
		}
	}

	// Use this for initialization
	void Start () {

		// Файлы не копируются в iCloud/iTunes.
		#if UNITY_IPHONE
		UnityEngine.iOS.Device.SetNoBackupFlag(Application.persistentDataPath + "/Files");
		#endif
	}

	// Update is called once per frame
	void Update () {

		if (downloadingQueue.Count > 0) {

			object data = downloadingQueue.Dequeue();
			DownloadParams prms = (DownloadParams)data;
			downloadingFiles.Add (prms.url);
			StartCoroutine (DownloadCoroutine(prms));
		}
	}

	public delegate float ProgressProvider();

	public ProgressProvider Download(string url, Action<object> onDownloadFinished, Action<object> onDownloadError = null) {

		if (downloadingFiles.Contains (url)) return null;

		DownloadParams prms = new DownloadParams (url, onDownloadFinished, onDownloadError);

		downloadingQueue.Enqueue (prms);

		return prms.GetProgress;
	}

	IEnumerator DownloadCoroutine(DownloadParams prms)
	{
		if(prms == null) yield break;

		string url = prms.url;
		Action<object> onDownloadFinished = prms.onDownloadFinished;
		Action<object> onDownloadError = prms.onDownloadError;

		url = url.Trim();

		GMALogger.Log ("[Downloader.DownloadCoroutine] Download file : " + url);

		WWW www = new WWW(url);
		float progress = 0;
		while (!www.isDone) {

			yield return null;

			if(www.progress > progress+0.01f  || www.progress < progress) {
				progress = www.progress;
				prms.SetProgress(progress);
			}
		}//*/

		int trynum = 0;
		while (trynum < 3 && !string.IsNullOrEmpty(www.error)) {

			trynum++;
			GMALogger.Log("[Downloader.DownloadCoroutine] " + trynum + " error: " + www.error);
			www.Dispose();
			www = null;

			yield return new WaitForSeconds(1);

			www = new WWW(url);
			progress = 0;
			while (!www.isDone) {

				yield return null;

				if(www.progress > progress+0.01f || www.progress < progress) {
					progress = www.progress;
					prms.SetProgress(progress);
				}
			}//*/
		}

		if(string.IsNullOrEmpty(www.error))
		{
			GMALogger.Log("[Downloader.DownloadCoroutine] Download finished " + url);
			if(onDownloadFinished != null) onDownloadFinished(www);

		} else {

			GMALogger.Log("[Downloader.DownloadCoroutine] Last error: " + www.error);

			if(onDownloadError != null) onDownloadError(www);
		}

		if (downloadingFiles.Contains (url)) downloadingFiles.Remove(url);

		try {
			www.Dispose();
		} catch {
		
		}

		www = null;
		Resources.UnloadUnusedAssets();
	}

	public bool FileIsDownloading(string name) {

		return downloadingFiles.Contains(name);
	}
}
