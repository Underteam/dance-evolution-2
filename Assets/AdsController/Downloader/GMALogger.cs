﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GMALogger : MonoBehaviour {

	public Text console;

	public bool disable = false;

	private static GMALogger instance;

	void Awake() {

		if (instance != null) {

			DestroyImmediate(gameObject);
			return;
		}

		instance = this;
	}

	public static void Log(string s) {

		if (instance == null) return;
		
		if (instance.disable) return;
		
		Debug.Log (s);
		if(instance == null) return;

		if(instance.console == null) return;

		instance.console.text += s + "\n";
	}

	public static void LogWarning(string s) {

		if (instance.disable) return;

		Debug.LogWarning (s);
		if(instance == null) return;

		if(instance.console == null) return;

		instance.console.text += "!!!!!" + s + "\n";
	}

	public static void LogError(string s) {

		if (instance.disable) return;

		Debug.LogError (s);
		if(instance == null) return;

		if(instance.console == null) return;

		instance.console.text += "?????" + s + "\n";
	}

	public static void Clear() {

		if(instance == null) return;

		if(instance.console == null) return;

		instance.console.text = "";
	}
}
