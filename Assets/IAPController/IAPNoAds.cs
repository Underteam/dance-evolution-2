﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPNoAds : MonoBehaviour {
	
    public static bool noAds = false;
    public GameObject restoreButton;
	public GameObject buyButton;

    private List<GameObject> listeners = new List<GameObject>();

	// Use this for initialization
	void Start () {
#if UNITY_ANDROID
        if(restoreButton != null) restoreButton.gameObject.SetActive(false);
#endif
    }

    public void NoAds()
    {
        noAds = true;
#if ADS_CONTROLLER
        AdsController.Instance().NoAds();
#endif

        for(int i = 0; i < listeners.Count; i++)
        {
            if (listeners[i] == null) continue;
            listeners[i].SendMessage("IAPBuyed", SendMessageOptions.DontRequireReceiver);
        }

        if (restoreButton != null) restoreButton.SetActive(false);
        if (buyButton != null) buyButton.SetActive (false);
    }
	
	public void AddListener(GameObject go)
    {
        if (!listeners.Contains(go)) listeners.Add(go);
    }
}
