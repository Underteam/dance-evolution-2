﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Morpher2 : MonoBehaviour {

    public Material mat1;
    public Material mat2;

    public AnimationCurve curve;

    public MeshRenderer leftWave;
    public MeshFilter leftWaveFilter;
    private Mesh leftWaveMesh;

    List<Vector3> waveVertices = new List<Vector3>();
    List<Vector3> leftWaveVertices = new List<Vector3>();
    List<Vector2> leftWaveUVs = new List<Vector2>();

    public MeshRenderer rightWave;
    public MeshFilter rightWaveFilter;
    private Mesh rightWaveMesh;

    List<Vector3> rightWaveVertices = new List<Vector3>();
    List<Vector2> rightWaveUVs = new List<Vector2>();

    [Range(0f, 1f)]
    public float t = 0;
    private float prevT;

    public bool anotherMode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(t != prevT)
        {
            prevT = t;
            Apply ();
        }
	}

    public void Apply ()
    {
        {
            float f = Mathf.Clamp(t * 20 - 19, 0, 1);
            rightWave.sharedMaterial.SetFloat("_Transition", f);
            leftWave.sharedMaterial.SetFloat("_Transition", f);
        }

        List<Vector3> newVertices = new List<Vector3>(leftWaveVertices);
        for (int i = 0; i < newVertices.Count / 2; i++)
        {
            Vector3 v = newVertices[i * 2 + 0];
            v.x = t;
            newVertices[i * 2 + 0] = v;

            v = newVertices[i * 2 + 1];
            if (anotherMode)
                v.x += Mathf.Clamp(waveVertices[i].x - (1 - t) * 1, 0, 1);
            else
                v.x += waveVertices[i].x * t;
            newVertices[i * 2 + 1] = v;

            float f = Mathf.Clamp(t * 5 - 4, 0, 1);
            Vector2 uv = leftWaveUVs[i * 2 + 1];
            uv.x = 1f * (1 - f) + waveVertices[i].x * f;
            leftWaveUVs[i * 2 + 1] = uv;
        }

        leftWaveMesh.SetVertices(newVertices);
        leftWaveMesh.SetUVs(0, leftWaveUVs);
        leftWaveFilter.mesh = leftWaveMesh;


        newVertices = new List<Vector3>(rightWaveVertices);
        for (int i = 0; i < newVertices.Count / 2; i++)
        {
            Vector3 v = newVertices[i * 2 + 0];
            v.x = -t;
            newVertices[i * 2 + 0] = v;

            v = newVertices[i * 2 + 1];
            if (anotherMode)
                v.x -= Mathf.Clamp((1-waveVertices[i].x) - (1 - t) * 1, 0, 1);
            else
                v.x -= (1-waveVertices[i].x) * t;
            newVertices[i * 2 + 1] = v;

            float f = Mathf.Clamp(t * 5 - 4, 0, 1);
            Vector2 uv = rightWaveUVs[i * 2 + 1];
            uv.x = 0f * (1 - f) + waveVertices[i].x * f;
            rightWaveUVs[i * 2 + 1] = uv;
        }

        rightWaveMesh.SetVertices(newVertices);
        rightWaveMesh.SetUVs(0, rightWaveUVs);
        rightWaveFilter.mesh = rightWaveMesh;
    }

    [EditorButton]
    public void Init ()
    {
        MakeLeftSide();
        MakeRightSide();
    }

    private void MakeLeftSide ()
    {
        GameObject leftWaveGO = new GameObject("Left Wave");

        leftWaveGO.transform.SetParent(transform);
        leftWaveGO.transform.localPosition = new Vector3(-1.5f, -0.5f, 0);

        leftWaveFilter = leftWaveGO.AddComponent<MeshFilter>();

        leftWave = leftWaveGO.AddComponent<MeshRenderer>();
        leftWave.sharedMaterial = mat1;

        leftWaveMesh = new Mesh();

        waveVertices.Clear();
        leftWaveVertices.Clear();
        leftWaveUVs.Clear();

        List<int> indices = new List<int>();

        for (int i = 0; i <= 100; i++)
        {
            float t = (100 - i) / 100f;
            float c = curve.Evaluate(t);
            leftWaveVertices.Add(new Vector3(0, i / 100f, 0));
            leftWaveVertices.Add(new Vector3(1, i / 100f, 0));
            leftWaveUVs.Add(new Vector2(0, i / 100f));
            leftWaveUVs.Add(new Vector2(1, i / 100f));
            waveVertices.Add(new Vector3(c, i / 100f, 0));
        }

        for (int i = 0; i < 100; i++)
        {
            indices.Add(i * 2 + 0);
            indices.Add(i * 2 + 2);
            indices.Add(i * 2 + 1);

            indices.Add(i * 2 + 2);
            indices.Add(i * 2 + 3);
            indices.Add(i * 2 + 1);
        }

        leftWaveMesh.SetVertices(leftWaveVertices);
        leftWaveMesh.SetTriangles(indices, 0);
        leftWaveMesh.SetUVs(0, leftWaveUVs);
        leftWaveFilter.mesh = leftWaveMesh;
    }

    private void MakeRightSide()
    {
        GameObject rightWaveGO = new GameObject("Right Wave");

        rightWaveGO.transform.SetParent(transform);
        rightWaveGO.transform.localPosition = new Vector3(1.5f, -0.5f, 0);

        rightWaveFilter = rightWaveGO.AddComponent<MeshFilter>();

        rightWave = rightWaveGO.AddComponent<MeshRenderer>();
        rightWave.sharedMaterial = mat2;

        rightWaveMesh = new Mesh();

        rightWaveVertices.Clear();
        rightWaveUVs.Clear();

        List<int> indices = new List<int>();

        for (int i = 0; i <= 100; i++)
        {
            float t = (100 - i) / 100f;
            float c = curve.Evaluate(t);
            rightWaveVertices.Add(new Vector3(0, i / 100f, 0));
            rightWaveVertices.Add(new Vector3(-1, i / 100f, 0));
            rightWaveUVs.Add(new Vector2(1, i / 100f));
            rightWaveUVs.Add(new Vector2(0, i / 100f));
        }

        for (int i = 0; i < 100; i++)
        {
            indices.Add(i * 2 + 0);
            indices.Add(i * 2 + 1);
            indices.Add(i * 2 + 2);

            indices.Add(i * 2 + 2);
            indices.Add(i * 2 + 1);
            indices.Add(i * 2 + 3);
        }

        rightWaveMesh.SetVertices(rightWaveVertices);
        rightWaveMesh.SetTriangles(indices, 0);
        rightWaveMesh.SetUVs(0, rightWaveUVs);
        rightWaveFilter.mesh = rightWaveMesh;
    }
}
