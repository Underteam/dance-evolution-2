﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteMixer : MonoBehaviour {

	public SpriteRenderer sprite;

	public Shape shape1;

	public Shape shape2;

	private Sprite sprite1;

	private Sprite sprite2;

	private List<Vector2> shapePoints1;

	private List<Vector2> shapePoints2;

	Texture2D tex;
	
	Color[] colors;

	float size1X;

	float size1Y;

	float size2X;
	
	float size2Y;

	float scaleX;

	float scaleY;

	// Use this for initialization
	void Start () {

		tex = new Texture2D (128, 128, TextureFormat.ARGB32, false);
		
		Color c = new Color (1, 1, 1, 0);
		colors = new Color[128*128];
		for (int i = 0; i < colors.Length; i++)
			colors [i] = c;
	}

	private bool inited = false;
	public void Init() {
	
		shapePoints1 = shape1.shape;
		
		shapePoints2 = shape2.shape;
		
		sprite1 = shape1.sprite;
		
		sprite2 = shape2.sprite;
		
		size1X = sprite1.rect.width / sprite1.pixelsPerUnit;
		
		size1Y = sprite1.rect.height / sprite1.pixelsPerUnit;
		
		size2X = sprite2.rect.width / sprite2.pixelsPerUnit;
		
		size2Y = sprite2.rect.height / sprite2.pixelsPerUnit;
		
		scaleX = sprite.transform.localScale.x;
		
		scaleY = sprite.transform.localScale.y;

		inited = true;
	}

	// Update is called once per frame
	void Update () {
		
	}
	
	public void Mix(float v) {

		if(!inited) Init ();

		float a = (v-0.05f)*1.15f;

		if(a < 0) a = 0;
		if(a > 1) a = 1;

		float sizeX = (1 - a) * size1X + a * size2X;

		float sizeY = (1 - a) * size1Y + a * size2Y;

		if (v <= 0) {

			sprite.transform.localScale = new Vector3 (scaleX, scaleY, 1);
			sprite.sprite = sprite1;
			return;
		}

		if (v >= 1) {
		
			sprite.transform.localScale = new Vector3 (scaleX, scaleY, 1);
			sprite.sprite = sprite2;
			return;
		}

		sprite.transform.localScale = new Vector3 (scaleX*sizeX, scaleY*sizeY, 1);

		List<Vector2> shape = Shape.Morph (shapePoints1, shapePoints2, a);
		
		List<Vector2> filled = Drawer.Fill (128, 128, shape);
		
		Color c = new Color (Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), 1);
		
		tex.SetPixels (0, 0, 128, 128, colors);
		
		for (int i = 0; i < filled.Count; i++)
			tex.SetPixel ((int)filled [i].x, (int)filled [i].y, c);
		
		tex.Apply ();
		
		sprite.sprite = Sprite.Create (tex, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 128.0f, 0, SpriteMeshType.Tight, new Vector4(-1, 1, -1, 1));
	
	}
}
