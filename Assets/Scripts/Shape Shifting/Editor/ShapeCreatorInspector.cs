﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ShapeCreator))]
public class ShapeCreatorInspector : Editor {

	private ShapeCreator creator;

	public override void OnInspectorGUI() {

		DrawDefaultInspector ();

		creator = (ShapeCreator)target;

		EditorGUILayout.BeginVertical ();
		GUILayout.Space(10);
		if (GUILayout.Button ("Create")) {

			if(!Directory.Exists(Application.dataPath + "/Shapes")) Directory.CreateDirectory(Application.dataPath + "/Shapes");
			string path;
			int l = -1;
			do {
				l++;
				path = Application.dataPath + "/Shapes/Shape" + l + ".asset";
			} while(File.Exists(path));
			Shape shape = CreateInstance<Shape>();

			creator.CreateShape(ref shape);

			AssetDatabase.CreateAsset(shape, "Assets/Shapes/Shape" + l + ".asset");
			AssetDatabase.SaveAssets();

		}
		EditorGUILayout.EndVertical ();
	}
}
#endif
