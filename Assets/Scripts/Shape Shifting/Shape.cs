﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ShapeType {

	alive,
	lifeless,
	angry
}

public class Shape : ScriptableObject  {

	public ShapeType type;

	public List<Vector2> shape;

	public Sprite sprite;

	public Vector4 bounds;

	public List<Vector2> MorphTo(Shape another, float amount) {
	
		List<Vector2> newShape = new List<Vector2> ();

		int N1 = shape.Count;
		int N2 = another.shape.Count;

		for(int j = 0; j < N2; j++) {

			int i = (int)(j * (float)N1 / (float)N2);

			newShape.Add((Vector2)((amount-1)*shape[i]+amount*another.shape[j]));
		}

		return newShape;
	}

	public static List<Vector2> Morph(List<Vector2> shape1, List<Vector2> shape2, float amount) {

		List<Vector2> newShape = new List<Vector2> ();
		
		int N1 = shape1.Count;
		int N2 = shape2.Count;

		//Debug.Log (N1 + " " + N2);

		for(int j = 0; j < N2; j++) {
			
			int i = (int)(j * (float)N1 / (float)N2);

			//Debug.Log(j + " <- " + i);

			newShape.Add((1-amount)*shape1[i]+amount*shape2[j]);
		}
		
		return newShape;
	}
}
