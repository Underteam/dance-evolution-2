﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Drawer : MonoBehaviour {

	public SpriteRenderer sprite;

	public Shape shape;

	Texture2D tex;

	Color[] colors;

	float sizeX;
	
	float sizeY;
	
	float scaleX;
	
	float scaleY;

	// Use this for initialization
	void Start () {
	
		tex = new Texture2D (128, 128, TextureFormat.ARGB32, false);

		Color c = new Color (1, 1, 1, 0);
		colors = new Color[128*128];
		for (int i = 0; i < colors.Length; i++)
			colors [i] = c;

		sizeX = sprite.sprite.texture.width / sprite.sprite.pixelsPerUnit;
		
		sizeY = sprite.sprite.texture.height / sprite.sprite.pixelsPerUnit;

		scaleX = sprite.transform.localScale.x;
		
		scaleY = sprite.transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private static int[,] map = new int[128, 128]; 
	public static List<Vector2> Fill(int width, int height, List<Vector2> baseShape) {

		for (int i = 0; i < 128; i++)
			for (int j = 0; j < 128; j++)
				map [i, j] = 0;

		List<Vector2> list = new List<Vector2> ();

		for (int i = 0; i < baseShape.Count; i++) {
			int x = (int)baseShape [i].x;
			int y = (int)baseShape [i].y;
			map [x, y] = 1;
		}

		for (int j = 0; j < height; j++) {

			bool fill = false;
			bool change = true;
			for(int i = 0; i < width; i++) {

				if(map[i, j] == 1) {

					if(change) {
						fill = !fill;
						change = false;
					}

				} else {

					if(fill) map[i, j] = 1;

					change = true;
				}

				if(map[i, j] == 1)
					list.Add(new Vector2(i, j));
			}
		}

		for(int j = 0; j < height; j++)
			for(int i = 0; i < width; i++)
				if(map[i, j] == 1) list.Add(new Vector2(i, j));

		return list;
	}

	public void Draw() {
	
		Color c = Color.red;

		List<Vector2> shapePoints = shape.shape;

		List<Vector2> filled = Drawer.Fill (128, 128, shapePoints);

		tex.SetPixels (0, 0, 128, 128, colors);

		for (int i = 0; i < filled.Count; i++) {

			int x = (int)(127.0f*(shape.bounds.x + filled [i].x*(shape.bounds.y-shape.bounds.x)/127.0f)/(float)shape.sprite.texture.width);
			int y = (int)(127.0f*(shape.bounds.z + filled [i].y*(shape.bounds.w-shape.bounds.z)/127.0f)/(float)shape.sprite.texture.height);

			tex.SetPixel (x, y, c);
		}

		tex.Apply ();

		sprite.sprite = Sprite.Create (tex, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 128.0f, 0, SpriteMeshType.Tight, new Vector4(-1, 1, -1, 1));

		sprite.transform.localScale = new Vector3 (scaleX*sizeX, scaleY*sizeY, 1);
	}
}
