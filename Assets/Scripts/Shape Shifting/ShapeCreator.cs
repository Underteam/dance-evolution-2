﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class ShapeCreator : MonoBehaviour {

	public Sprite sprite;

	// Use this for initialization
	void Start () {

	}
	
	public void CreateShape(ref Shape shape) {

		if (sprite == null) {
			SpriteRenderer renderer = GetComponent<SpriteRenderer>();
			if(renderer == null) return;
			if(renderer.sprite == null) return;
			sprite = renderer.sprite;
		}

		Texture2D tex = CreateTexture (sprite);// sprite.texture;

		StoreTexture (tex, "tex1");

		Vector2 startPoint = FindFirstPixel (tex);
		Debug.Log ("Found first pixel " + startPoint + " " + sprite.rect);
		List<KeyValuePair<int, int>> path = FindPath (tex, startPoint);
		Vector4 bounds = new Vector4 ();
		List<Vector2> shapePoints = CreateShape (path, 1000, out bounds);

		StoreShape (shapePoints, 1);

		shape.bounds = bounds;
		shape.shape = shapePoints;
		shape.sprite = sprite;
	}

	public Vector2 FindFirstPixel(Texture2D tex) {

		int width = tex.width;
		int height = tex.height;

		for (int j = height-1; j >= 0; j--) {
			
			for(int i = 0; i < width; i++) {
				
				Color c = tex.GetPixel(i, j);
				
				if(c.a != 0) return new Vector2(i, j);
			}
		}

		return Vector2.zero;
	}

	public bool PointOk(int width, int height, Vector2 point) {
		
		if(point.x < 0 || point.x >= width || point.y < 0 || point.y >= height) return false;
		
		return true;
	}

	//Color clear = new Color(0, 0, 0, 0);
	public List<KeyValuePair<int, int>> FindPath(Texture2D tex, Vector2 startPoint) {

		Color[] colors = tex.GetPixels ();

		List<KeyValuePair<int, int>> path = new List<KeyValuePair<int, int>>();

		path.Add (new KeyValuePair<int, int> ((int)startPoint.x, (int)startPoint.y));

		Vector2 currentPoint = startPoint;

		Vector2 nextPoint = startPoint;

		int dir = 0;

		bool changeDir = false;

		int counter = 0;

		while (true) {
		
			if(changeDir) {
				dir = (dir + 7) % 8;
				counter = 0;
			}
			changeDir = false;

			//if(CheckDirection(tex, currentPoint, dir, out nextPoint)) {
			if(CheckDirection(colors, tex.width, tex.height, currentPoint, dir, out nextPoint)) {

				KeyValuePair<int, int> kvp = new KeyValuePair<int, int>((int)nextPoint.x, (int)nextPoint.y);
				if(path.Contains(kvp)) {

					/*for(int p = 0; p < path.Count; p++) {
						int key = (int)path[p].Key + (int)path[p].Value*width;
						colors[key] = clear;
						//tex.SetPixel((int)path[p].Key, (int)path[p].Value, clear);
					}
					changeDir = true;//*/

					return path;

				} else {
					path.Add(kvp);
					currentPoint = nextPoint;
					changeDir = true;
					continue;
				}
			}

			dir = (dir + 1) % 8;
			counter++;

			if(counter >= 7) {
				//Debug.Log("Return here 2");
				return path;

			}
		}
	}

	public bool CheckDirection(Color[] colors, int width, int height, Vector2 currentPoint, int dir, out Vector2 nextPoint) {
		
		nextPoint = currentPoint;
		
		if (dir == 0) {
			nextPoint.x += 1;
			nextPoint.y += 0;
		}
		
		if (dir == 1) {
			nextPoint.x += 1;
			nextPoint.y -= 0;
		}
		
		if (dir == 2) {
			nextPoint.x += 0;
			nextPoint.y -= 1;
		}
		
		if (dir == 3) {
			nextPoint.x -= 1;
			nextPoint.y -= 1;
		}
		
		if (dir == 4) {
			nextPoint.x -= 1;
			nextPoint.y -= 0;
		}
		
		if (dir == 5) {
			nextPoint.x -= 1;
			nextPoint.y += 1;
		}
		
		if (dir == 6) {
			nextPoint.x += 0;
			nextPoint.y += 1;
		}
		
		if (dir == 7) {
			nextPoint.x += 1;
			nextPoint.y += 1;
		}

		if (!PointOk (width, height, nextPoint)) {
			nextPoint = currentPoint;
			return false;
		}

		int key = (int)nextPoint.x + (int)nextPoint.y * width;
		Color c = colors [key];
		
		if(c.a != 0) return true;

		nextPoint = currentPoint;
		return false;
	}

	public List<Vector2> CreateShape(List<KeyValuePair<int, int>> path, int maxPoints, out Vector4 bounds) {

		if (maxPoints == 0)
			maxPoints = path.Count;
		else {
			if (maxPoints < 10)
				maxPoints = 10;
			if (maxPoints > 1000)
				maxPoints = 1000;
		}

		bounds.x = 0;
		bounds.y = 0;
		bounds.z = 0;
		bounds.w = 0;

		List<Vector2> shape = new List<Vector2> ();

		if(path.Count < 1) return shape;

		int minX = path[0].Key;
		int maxX = path[0].Key;
		int minY = path[0].Value;
		int maxY = path[0].Value;

		for (int i = 0; i < path.Count; i++) {

			if(path[i].Key < minX) minX = path[i].Key;
			if(path[i].Key > maxX) maxX = path[i].Key;
			if(path[i].Value < minY) minY = path[i].Value;
			if(path[i].Value > maxY) maxY = path[i].Value;
		}

		bounds.x = minX;
		bounds.y = maxX;
		bounds.z = minY;
		bounds.w = maxY;

		for (int i = 0; i < maxPoints; i++) {
		
			int p = (int)(i * (float)path.Count / (float)maxPoints);
			shape.Add (new Vector2 (127.0f * (path [p].Key - minX) / (maxX - minX), 127.0f * (path [p].Value - minY) / (maxY - minY)));
		}

		return shape;
	}

	/*public bool CheckDirection(Texture2D tex, Vector2 currentPoint, int dir, out Vector2 nextPoint) {

		nextPoint = currentPoint;

		if (dir == 0) {
			nextPoint.x += 1;
			nextPoint.y += 0;
		}

		if (dir == 1) {
			nextPoint.x += 1;
			nextPoint.y -= 0;
		}

		if (dir == 2) {
			nextPoint.x += 0;
			nextPoint.y -= 1;
		}

		if (dir == 3) {
			nextPoint.x -= 1;
			nextPoint.y -= 1;
		}

		if (dir == 4) {
			nextPoint.x -= 1;
			nextPoint.y -= 0;
		}

		if (dir == 5) {
			nextPoint.x -= 1;
			nextPoint.y += 1;
		}

		if (dir == 6) {
			nextPoint.x += 0;
			nextPoint.y += 1;
		}

		if (dir == 7) {
			nextPoint.x += 1;
			nextPoint.y += 1;
		}

		if(!PointOk(tex.width, tex.height, nextPoint)) return false;

		if(tex.GetPixel((int)nextPoint.x, (int)nextPoint.y).a != 0) return true;
			
		return false;
	}//*/

	public Texture2D CreateTexture(Texture2D tex, int width, int height, List<KeyValuePair<int, int>> path) {
	
		Texture2D res = new Texture2D (width, height, TextureFormat.ARGB32, false);

		if(path.Count < 1) return tex;

		Color[] colors = tex.GetPixels ();

		int minX = path[0].Key;
		int maxX = path[0].Key;
		int minY = path[0].Value;
		int maxY = path[0].Value;

		for (int i = 0; i < path.Count; i++) {
			
			if(path[i].Key < minX) minX = path[i].Key;
			if(path[i].Key > maxX) maxX = path[i].Key;
			if(path[i].Value < minY) minY = path[i].Value;
			if(path[i].Value > maxY) maxY = path[i].Value;
		}

		Debug.Log (tex.width + " " + tex.height);
		Debug.Log (minX + " " + maxX + " " + minY + " " + maxY);

		for (int j = 0; j < height; j++) {
			for(int i = 0; i < width; i++) {
				
				int x = (int)(minX+i*(float)(maxX-minX)/(float)(width-1));
				int y = (int)(minY+j*(float)(maxY-minY)/(float)(height-1));

				int key = x+ y*tex.width;

				//Color c = tex.GetPixel(x, y);
				Color c = Color.black;
				try{
				c = colors[key];
				} catch {
					Debug.Log("Stop at " + i + " " + j + " " + x + " " + y + " " + key + " " + colors.Length);
					return res;
				}

				res.SetPixel(i, j, c);
			}
		}

		return res;
	}

	public Texture2D CreateTexture(Sprite sp) {

		int width = (int)sp.rect.width;
		int height = (int)sp.rect.height;
		int x = (int)sp.rect.x;
		int y = (int)sp.rect.y;

		Debug.Log (sp.rect + " " + sp.texture.width + " " + sp.texture.height);

		Texture2D source = sp.texture;
		Texture2D dest = new Texture2D (width, height, TextureFormat.ARGB32, false);
		
		Color[] sourceColors = source.GetPixels ();
		Color[] destColors = dest.GetPixels ();

		for (int j = 0; j < height; j++) {
			for(int i = 0; i < width; i++) {

				int sourceKey = (x+i) + (y+j)*source.width;
				int destKey = i+j*width;
				destColors[destKey] = sourceColors[sourceKey];
			}
		}

		dest.SetPixels (destColors);
		dest.Apply ();

		return dest;
	}

	public void StoreShape(Texture2D tex, List<KeyValuePair<int, int>> path, int ind) {
	
		Texture2D newTex = new Texture2D (tex.width, tex.height, TextureFormat.RGB24, true);

		for(int i = 0; i < path.Count; i++) {
		
			int x = path[i].Key;
			int y = path[i].Value;
			Color c = new Color(1, 0, 0, 1);//tex.GetPixel(x, y);
			newTex.SetPixel(x, y, c);
		}

		newTex.Apply ();

		byte[] dataToSave = newTex.EncodeToPNG();
		//string destination = System.IO.Path.Combine(Application.dataPath,System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
		string destination = Application.dataPath + "/shape" + ind + ".png";

		//if (System.IO.File.Exists (destination)) System.IO.File.Delete (destination);
		//System.IO.File.WriteAllBytes(destination, dataToSave);

		Debug.Log ("Stored to " + destination);
	}

	public void StoreShape(List<Vector2> path, int ind) {
		
		Texture2D newTex = new Texture2D (128, 128, TextureFormat.RGB24, true);
		
		for(int i = 0; i < path.Count; i++) {
			
			int x = (int)path[i].x;
			int y = (int)path[i].y;
			Color c = new Color(1, 0, 0, 1);//tex.GetPixel(x, y);
			newTex.SetPixel(x, y, c);
		}
		
		newTex.Apply ();
		
		byte[] dataToSave = newTex.EncodeToPNG();
		//string destination = System.IO.Path.Combine(Application.dataPath,System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
		string destination = Application.dataPath + "/shape" + ind + ".png";
		
		if (System.IO.File.Exists (destination)) System.IO.File.Delete (destination);
		System.IO.File.WriteAllBytes(destination, dataToSave);
		
		Debug.Log ("Stored to " + destination);
	}

	public void StoreTexture(Texture2D tex, string fName) {
		
		byte[] dataToSave = tex.EncodeToPNG();
		//string destination = System.IO.Path.Combine(Application.dataPath,System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
		string destination = Application.dataPath + "/" + fName + ".png";
		
		if (System.IO.File.Exists (destination)) System.IO.File.Delete (destination);
		System.IO.File.WriteAllBytes(destination, dataToSave);
		
		Debug.Log ("Stored to " + destination);
	}
}
#endif