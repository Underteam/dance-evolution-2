﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorEvent : MonoBehaviour {

    public UnityEvent onEvent;

	// Use this for initialization
	void Start () {
		
	}
	
	public void OnEvent ()
    {
        if (onEvent != null) onEvent.Invoke();
    }
}
