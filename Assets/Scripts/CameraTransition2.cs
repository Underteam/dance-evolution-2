﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraTransition2 : MonoSingleton<CameraTransition2>
{

    public List<DanceFloor> floors;

    private int currentFloor;
    //private int targetFloor;

    private Camera cam;

    private bool inTransition;

    public BGFitter fitter1;

    public BGFitter fitter2;

    public int position = 1;

    public float xPos;

    public List<GameObject> disablers;

    public List<MenuPanel> panels;

    public System.Action<int> onGoTo;

    public Transform blackCloud;

    // Use this for initialization
    void Start()
    {

        cam = GetComponent<Camera>();

        targetPosition = position;

        position = 2;
        targetPosition = 2;
        Vector3 pos = panels[position].transform.position;
        pos.z = cam.transform.position.z;
        cam.transform.position = pos;
    }

    private bool drag;
    private Vector2 tapPos;
    private float targetXPos;
    [SerializeField]
    private int targetPosition;

    // Update is called once per frame
    void Update()
    {

        if (disablers.Count > 0)
        {
            drag = false;
        }

        if (Input.GetMouseButtonDown(0) && disablers.Count == 0 && !inTransition)
        {
            drag = true;
            tapPos = new Vector3(xPos * Screen.width, 0, 0) + Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0) && drag)
        {
            drag = false;
            targetXPos = 0;
            int prev = targetPosition;
            targetPosition = position;
            if (xPos < -0.3f && position > 0)
            {
                targetPosition = position - 1;
                targetXPos = (panels[targetPosition].transform.position - panels[position].transform.position).x;
            }

            if (xPos > 0.3f && position < panels.Count - 1)
            {
                targetPosition = position + 1;
                targetXPos = (panels[targetPosition].transform.position - panels[position].transform.position).x;
            }

            if (targetPosition != prev && onGoTo != null) onGoTo(targetPosition);

        }

        if (drag)
        {
            xPos = (tapPos.x - Input.mousePosition.x) / Screen.width;
            //if (position == 0 && xPos < 0) xPos = 0;
            //if (position == panels.Count - 1 && xPos > 0) xPos = 0;

            //Vector3 pos = panels[position].transform.position;
            //pos.x += xPos;
            //pos.z = cam.transform.position.z;
            //cam.transform.position = pos;

            int ind = position;
            if (xPos < 0) ind--;
            if (xPos > 0) ind++;
            Vector3 pos = panels[ind].transform.position;
            pos.x -= 100 * xPos;
            blackCloud.position = pos;
        }
        else if (Mathf.Abs(xPos) > 0)
        {
            xPos = Mathf.Lerp(xPos, targetXPos, 10 * Time.deltaTime);
            Vector3 pos = panels[position].transform.position;
            pos.x += xPos;
            pos.z = cam.transform.position.z;
            cam.transform.position = pos;

            if (Mathf.Abs(xPos - targetXPos) < 0.1f)
            {
                position = targetPosition;
                xPos = -(panels[position].transform.position - cam.transform.position).x;
                targetXPos = 0;
            }
        }
    }

    public void GoTo(int i)
    {
        if (i < 0 || i > floors.Count) return;

        Debug.Log("Go to " + i);
        targetPosition = i;
        xPos = 0.01f;
    }

    private IEnumerator TransitingUp(SpriteRenderer fl1, SpriteRenderer fl2, System.Action onFinish)
    {
        inTransition = true;

        Color color = fl1.color;
        color.a = 0;
        fl1.color = color;

        fitter1.gameObject.SetActive(true);

        fitter2.gameObject.SetActive(true);

        fitter1.back.sprite = fl1.sprite;
        fitter1.back.sortingOrder = fl1.sortingOrder;

        fitter2.back.sprite = fl2.sprite;

        color = fitter1.back.color;
        color.a = 1;
        fitter1.back.color = color;

        color = fitter2.back.color;
        color.a = 0;
        fitter2.back.color = color;

        fitter2.back.sortingOrder = fitter1.back.sortingOrder + 100;

        fitter1.partOfScreen = new Vector2(1f, 1f);

        fitter1.partOfScreen = new Vector2(3f, 3f);

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;
            cam.orthographicSize = 5 + 20 * t;
            color = fitter2.back.color;
            color.a = t;
            fitter2.back.color = color;
            fitter2.partOfScreen = new Vector2(3f - 2 * t, 3f - 2 * t);
            yield return null;
        }

        fitter1.gameObject.SetActive(false);

        fitter2.gameObject.SetActive(false);

        color = fl1.color;
        color.a = 1;
        fl1.color = color;

        cam.orthographicSize = 5;

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private IEnumerator TransitingDown(SpriteRenderer fl1, SpriteRenderer fl2, System.Action onFinish)
    {
        inTransition = true;

        Color color = fl1.color;
        color.a = 0;
        fl1.color = color;

        fitter1.gameObject.SetActive(true);

        fitter2.gameObject.SetActive(true);

        fitter1.back.sprite = fl1.sprite;
        fitter1.back.sortingOrder = fl1.sortingOrder;

        fitter2.back.sprite = fl2.sprite;

        color = fitter1.back.color;
        color.a = 1;
        fitter1.back.color = color;

        color = fitter2.back.color;
        color.a = 0;
        fitter2.back.color = color;

        fitter2.back.sortingOrder = fitter1.back.sortingOrder + 100;

        fitter1.partOfScreen = new Vector2(1f, 1f);

        fitter1.partOfScreen = new Vector2(1f, 1f);

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;
            cam.orthographicSize = 5 - 4 * t;
            color = fitter2.back.color;
            color.a = t;
            fitter2.back.color = color;
            fitter1.partOfScreen = new Vector2(1f + 2 * t, 1f + 2 * t);
            yield return null;
        }

        fitter1.gameObject.SetActive(false);

        fitter2.gameObject.SetActive(false);

        color = fl1.color;
        color.a = 1;
        fl1.color = color;

        cam.orthographicSize = 5;

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private void SetCamPosition(Vector3 pos)
    {
        pos.z = cam.transform.position.z;
        cam.transform.position = pos;
    }

    //[EditorButton]
    //public void Transit2()
    //{
    //    StartCoroutine(TransitingUp(floors[0].bg, floors[1].bg, () => { SetCamPosition(floors[1].transform.position);}));
    //}

    //[EditorButton]
    //public void Transit3()
    //{
    //    StartCoroutine(TransitingDown(floors[1].bg, floors[0].bg, () => { SetCamPosition(floors[0].transform.position); }));
    //}

    public void Transit(int to)
    {
        if (inTransition) return;

        if (currentFloor == to) return;

        if (to > currentFloor)
        {
            StartCoroutine(TransitingUp(floors[currentFloor].bg, floors[to].bg, () => { SetCamPosition(floors[to].transform.position); }));
        }
        else
        {
            StartCoroutine(TransitingDown(floors[currentFloor].bg, floors[to].bg, () => { SetCamPosition(floors[to].transform.position); }));
        }

        currentFloor = to;
    }

    //[EditorButton]
    //public void Transit ()
    //{
    //    if (inTransition) return;

    //    targetFloor = (currentFloor + 1) % floors.Count;

    //    StartCoroutine(Transition());
    //}

    //private IEnumerator Transition ()
    //{
    //    inTransition = true;

    //    yield return null;

    //    Vector3 targetPos = floors[targetFloor].transform.position;
    //    targetPos.z = transform.position.z;

    //    float size = cam.orthographicSize;

    //    float s = size;

    //    while(s > 0.01f)
    //    {
    //        s = Mathf.Lerp(s, 0f, 15 * Time.deltaTime);
    //        if (s < 0.01f) s = 0.01f;
    //        cam.orthographicSize = s;
    //        yield return null;
    //    }

    //    cam.transform.position = targetPos;

    //    while(s < size-0.1f)
    //    {
    //        s = Mathf.Lerp(s, size, 15 * Time.deltaTime);
    //        if (s > size) s = size;
    //        cam.orthographicSize = s;
    //        yield return null;
    //    }

    //    cam.orthographicSize = size;

    //    currentFloor = targetFloor;

    //    inTransition = false;
    //}
}
