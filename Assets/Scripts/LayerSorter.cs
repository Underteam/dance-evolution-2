﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LayerSorter : MonoBehaviour {

	public class Pair {
	
		public float pos;
		public int i;

		public int CompareTo(Pair another)
		{
		
			if (another.pos > pos + 0.1f)
				return 0;

			return 1;
		}
	}

	public class PairComparer : IComparer<Pair> {
	
		public int Compare(Pair x, Pair y)
		{

			if (x.pos < y.pos)
				return -1;
			else
				return 1;
		}
	}

	PairComparer comparer = new PairComparer();

	List<Pair> positions = new List<Pair>(1000);

	List<Transform> tempList = new List<Transform>();

	int n = 0;

	// Use this for initialization
	void Start () {
	
		for (int i = 0; i < 1000; i++)
			positions.Add (new Pair ());
	}
	
	// Update is called once per frame
	void Update () {

		List<Transform> list = LayeredObject.list;
		for (int i = 0; i < list.Count; i++) {
			positions [i].pos = list [i].position.y;
			positions [i].i = i;
		}
		n = list.Count;

		/*for (int i = 0; i < n-1; i++) {
			if (positions [i].pos > positions [i+1].pos) {
				Pair pair = positions [i];
				positions [i] = positions [i + 1];
				positions [i + 1] = pair;
				//Debug.Log ("Resort");
			}
		}//*/

		positions.Sort (0, n, comparer);

		tempList.Clear ();
		for (int i = 0; i < n; i++) {

			tempList.Add (list [positions [i].i]);
			Vector3 pos = tempList [i].parent.position;
			pos.z = -1 - n*0.01f + i * 0.01f;
			tempList [i].parent.position = pos;
		}

		LayeredObject.list = tempList;
		tempList = list;

	}
}
