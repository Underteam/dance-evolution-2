﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BGFitter : MonoBehaviour {

	public enum Align {
		center,
		up,
		down,
	}

	public Camera cam;

	public SpriteRenderer back;

	public Align align;

	public Vector2 partOfScreen = new Vector2(1, 1);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
		if(cam == null || back == null) return;

		if(!cam.orthographic) return;

		float screetRatio = ((float)Screen.width) / Screen.height;

		float viewRatio = (Screen.width * partOfScreen.x) / (Screen.height * partOfScreen.y);

		float spriteRatio = back.sprite.rect.width / back.sprite.rect.height;

		float scale = 1;
		if (spriteRatio > viewRatio) {//подгоняем высоту
		
			float height = 2*cam.orthographicSize*partOfScreen.y;
			float spriteSizeY = back.sprite.rect.height/back.sprite.pixelsPerUnit;
			scale = height / spriteSizeY;

		} else {// подгоняем ширину

			float width = 2*cam.orthographicSize * screetRatio * partOfScreen.x;
			float spriteSizeX = back.sprite.rect.width/back.sprite.pixelsPerUnit;
			scale = width / spriteSizeX;
		}

		transform.localScale = new Vector3 (scale, scale, scale);

		if (align == Align.center) {
			Vector3 pos = transform.position;
			pos.x = cam.transform.position.x;
			pos.y = cam.transform.position.y;
			transform.position = pos;
		}

		if (align == Align.up) {
		
			float delta = -cam.orthographicSize + 0.5f * scale * back.sprite.rect.height / back.sprite.pixelsPerUnit + 2*cam.orthographicSize*(1-partOfScreen.y);
			Vector3 pos = transform.position;
			pos.x = cam.transform.position.x;
			pos.y = cam.transform.position.y + delta;
			transform.position = pos;
		}

		if (align == Align.down) {
			
			float delta = cam.orthographicSize - 0.5f * scale * back.sprite.rect.height / back.sprite.pixelsPerUnit - 2*cam.orthographicSize*(1-partOfScreen.y);
			Vector3 pos = transform.position;
			pos.x = cam.transform.position.x;
			pos.y = cam.transform.position.y + delta;
			transform.position = pos;
		}
	}
}
