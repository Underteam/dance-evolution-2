﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

    public List<Morph> morphs;

    [EditorButton]
	public void Do ()
    {
        for (int i = 0; i < morphs.Count; i++) morphs[i].Do();
    }
}
