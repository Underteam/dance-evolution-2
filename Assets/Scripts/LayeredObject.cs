﻿using UnityEngine;
using System.Collections.Generic;

public class LayeredObject : MonoBehaviour {

	public static List<Transform> list = new List<Transform>();

	void OnEnable() {
	
		if (!list.Contains (transform)) {
			int i = 0;
			while (i < list.Count && list [i].position.y < transform.position.y)
				i++;
			if (i < list.Count)
				list.Insert (i, transform);
			else
				list.Add (transform);
		}
	}

	void OnDisabled() {
	
		if (list.Contains (transform))
			list.Remove (transform);
	}

	void OnDestroy() {
	
		if (list.Contains (transform))
			list.Remove (transform);
	}
}
