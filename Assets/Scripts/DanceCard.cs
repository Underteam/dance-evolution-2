﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DanceCard : MonoBehaviour {

    public Image imgBack;

    public Image imgCard;

    public Image imgIndicator;

    public Sprite indicatorOn;
    public Sprite indicatorOff;

    //public Sprite cardSprite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSprite (Sprite sprite, bool active)
    {
        imgBack.sprite = sprite;

        imgCard.gameObject.SetActive(active);

        if (imgIndicator != null) imgIndicator.gameObject.SetActive(active);
    }

    public void SetState(bool on)
    {
        if (imgIndicator != null) imgIndicator.sprite = on ? indicatorOn : indicatorOff;
    }
}
