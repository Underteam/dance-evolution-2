﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDancerMenu : MonoSingleton<NewDancerMenu> {

    [System.Serializable]
    public class Epoch
    {
        public string epochName;
        public Sprite epochSign;
        public List<Sprite> unknownDancers;
        public List<Sprite> dancers;
    }

    public List<Epoch> epoches;

    public Image epochSign;
    public Text epochName;

    public List<GameObject> dots;
    public GameObject note;

    public List<Image> dancers;

    public Image avatar;

    public int lastDiscovered = -1;

    public FlyingCoin crystalPrefab;

	// Use this for initialization
	void Start () {

        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show (int dancerID)
    {
        //if (!inited) Init();

        if (dancerID > lastDiscovered) lastDiscovered = dancerID;
        else return;

        int e = 0;

        while(e < epoches.Count && dancerID >= epoches[e].dancers.Count)
        {
            dancerID -= epoches[e].dancers.Count;
            e++;
        }

        if(e >= 0 && e < epoches.Count)
        {
            var epoch = epoches[e];

            epochSign.sprite = epoch.epochSign;
            epochName.text = epoch.epochName;

            for(int i = 0; i < dancers.Count; i++)
            {
                if (i <= dancerID) dancers[i].sprite = epoch.dancers[i];
                else dancers[i].sprite = epoch.unknownDancers[i];
            }

            avatar.sprite = epoch.dancers[dancerID];

            note.transform.SetParent(dots[dancerID].transform);
            note.transform.localPosition = Vector3.zero;

            gameObject.SetActive(true);
        }
    }

    public void Check (int dancerID)
    {
        //Debug.Log("Check " + dancerID + " " + lastDiscovered);
        if (dancerID > lastDiscovered) Show(dancerID);
    }

    public void Close ()
    {
        Wallet wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        gameObject.SetActive(false);
        GameController.Instance().AnimateFlyingCoins(crystalPrefab, 3, 0.2f, (n) => { wallet.Add(n); }, null);
    }
}
