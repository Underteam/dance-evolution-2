﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour {

    public Animator anim;

    public int numTransitions;

    public bool pause;

    private float timer;

    private void Start()
    {
        timer = Random.Range(5f, 10f);
    }

    [EditorButton]
	public void Tranit ()
    {
        anim.SetTrigger("Transit");

        //UnityEditor.EditorApplication.isPaused = pause;
    }

    private void Update()
    {
        if (anim == null) return;

        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                if (numTransitions > 1) anim.SetInteger("TransitNum", Random.Range(0, numTransitions));
                anim.SetTrigger("Transit");
                if (pause) timer = Random.Range(1f, 2f);
                else timer = Random.Range(5f, 10f);
            }
        }
    }

}
