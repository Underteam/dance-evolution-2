﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISignable
{
    Transform GetSignPlace();
    bool Overlayed();
}

public class SignsManager : MonoSingleton<SignsManager> {

    public Sign signPrefab;
    public Sign overlayedSignPrefab;

    private Dictionary<ISignable, Sign> signs = new Dictionary<ISignable, Sign>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowSign(ISignable element)
    {
        if (element == null) return;
        if (signs.ContainsKey(element)) return;

        var sign = Instantiate(element.Overlayed() ? overlayedSignPrefab : signPrefab);
        sign.gameObject.SetActive(true);

        sign.transform.SetParent(signPrefab.transform.parent);
        sign.transform.localScale = signPrefab.transform.localScale;
        (sign.transform as RectTransform).sizeDelta = (signPrefab.transform as RectTransform).sizeDelta;

        sign.SetTarget(element.GetSignPlace());
        signs.Add(element, sign);
    }

    public void RemoveSign(ISignable element)
    {
        if (element == null) return;
        if (!signs.ContainsKey(element)) return;

        var sign = signs[element];
        signs.Remove(element);

        Destroy(sign.gameObject);

        Debug.Log("Remove");
    }
}
