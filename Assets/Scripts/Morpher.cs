﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Morpher : MonoBehaviour {

    public MeshFilter leftSide;

    private Mesh newMesh;

    private List<Vector3> vertices;

    private MeshFilter newObject;


    private float maxDist;

    [Range(0f, 1f)]
    public float t = 0;

    private float prevT = 0;

    public bool anotherMode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(t != prevT)
        {
            Apply();
            prevT = t;
        }
	}

    [EditorButton]
    public void Init ()
    {
        Mesh mesh = leftSide.sharedMesh;

        vertices = new List<Vector3>();
        mesh.GetVertices(vertices);

        Debug.Log(vertices.Count);
        maxDist = 0;
        for(int i = 0; i < vertices.Count; i++)
        {
            Vector3 v = vertices[i];
            if (v.x > maxDist) maxDist = v.x;
        }

        newObject = Instantiate(leftSide);

        newObject.transform.SetParent(leftSide.transform.parent);
        newObject.transform.localPosition = leftSide.transform.localPosition;
        newObject.transform.localScale = leftSide.transform.localScale;

        newMesh = newMesh = Instantiate(mesh);
        newMesh.SetVertices(vertices);
        //newMesh.SetIndices()
        newObject.mesh = newMesh;
    }

    public void Apply ()
    {
        List<Vector3> newVertices = new List<Vector3>(vertices);
        for (int i = 0; i < vertices.Count; i++)
        {
            Vector3 v = newVertices[i];
            if (anotherMode)
                v.x = Mathf.Clamp(v.x - (1 - t) * maxDist, 0, maxDist);
            else
                v.x *= t;
            newVertices[i] = v;
        }

        newMesh.SetVertices(newVertices);
        newObject.mesh = newMesh;
    }
}
