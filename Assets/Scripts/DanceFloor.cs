﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Numerics;

public class DanceFloor : MonoBehaviour, ISaveable {

    public SpriteRenderer bg;

    //[HideInInspector]
    public int floorId;

    //public float spawnTime;

    //public float timer;

    public Elevator elevatorPrefab;
    public Elevator goldElevatorPrefab;

    public List<Dancer> prefabs;

    private List<Dancer> dancers = new List<Dancer>();

    public int capacity = 16;

    //public int typeToSpawn = -1;

    private BigInteger earnings = new BigInteger();

    public string _earnings;

    //public DanceFloor nextFloor;

    //public ProgressBar spawnBar;

    public GameObject floorButton;

    public Transform dancersOrigin;

    public AudioSource music;

    public GameObject clouds;

    // Use this for initialization
    void Start()
    {

    }

    public bool log;

    // Update is called once per frame
    void Update()
    {
        //if (typeToSpawn < 0) return;

        //bool canSpawn = dancers.Count < capacity;

        //if (typeToSpawn >= prefabs.Count)
        //{
        //    if (log) Debug.Log (typeToSpawn + " " + prefabs.Count);

        //    DanceFloor fl = this;
        //    int t = typeToSpawn;
        //    while (fl != null && t >= fl.prefabs.Count)
        //    {
        //        t -= fl.prefabs.Count;
        //        fl = fl.nextFloor;
        //    }

        //    if (fl == null) canSpawn = false;
        //    else canSpawn = fl.dancers.Count < fl.capacity;

        //    if (log) Debug.Log(canSpawn + " " + fl + " " + (fl == null ? "null" : "" + fl.dancers.Count), fl);
        //}

        //if (canSpawn) timer += Time.deltaTime;

        //if (timer >= spawnTime && canSpawn)
        //{
        //    timer = 0;

        //    Dancer dancer = Spawn(typeToSpawn);

        //    Debug.Log("Spawn " + typeToSpawn + " " + dancer, dancer);

        //    dancer.gameObject.SetActive(false);
        //    Elevator elevator = Instantiate(elevatorPrefab);

        //    elevator.transform.SetParent(dancersOrigin);
        //    elevator.transform.position = dancer.transform.position;
        //    elevator.dancer = dancer;

        //    GameController.Instance().DancerSpawned();
        //}

        //if (spawnBar != null) spawnBar.SetValue(timer / spawnTime);
    }

    public Dancer Spawn (int type)
    {
        if (type < 0 || type >= prefabs.Count) return null;

        //Debug.Log("Spawn " + type, this);
        
        Vector3 pos = 2 * Random.insideUnitCircle;

        //pos += dancersOrigin.position;

        Dancer dancer = Instantiate(prefabs[type]);

        dancer.danceFloor = this;

        dancer.type = type;

        dancer.transform.SetParent(dancersOrigin);
        dancer.transform.localPosition = pos;

        dancers.Add(dancer);

        earnings += dancer.GetEarnings();
        _earnings = earnings.Short();

        //Debug.Log("Spawn dancer " + dancer + " " + dancer.GetEarnings().Short() + " " + dancer.GetEarnings().ToString() + " " + earnings.Short());

        floorButton.SetActive(true);

        return dancer;
    }

    public BigInteger GetEarningsRate ()
    {
        earnings = 0;

        for (int i = 0; i < dancers.Count; i++)
        {
            if (!dancers[i].gameObject.activeSelf) continue;
            earnings += dancers[i].GetEarnings();
        }

        _earnings = earnings.Short();

        return earnings;
    }

    private void RemoveDancer (Dancer dancer)
    {
        if (dancers.Contains(dancer))
        {
            dancers.Remove(dancer);
            //earnings -= dancer.GetEarnings();
        }

        //_earnings = earnings.Short();
    }

    public void RemoveAllDancers ()
    {
        for(int i = 0; i < dancers.Count; i++)
        {
            if (dancers[i].elevator != null) Destroy(dancers[i].elevator.gameObject);
            Destroy(dancers[i].gameObject);
        }

        dancers.Clear();
    }

    public bool Merge (Dancer dancer1, Dancer dancer2)
    {
        //Debug.Log("Merge " + dancer1 + " " + dancer2, this);

        int typeToSpawn = dancer1.type + 1;

        var newDancer = GameController.Instance().Spawn(dancer1.type + 1, true);

        if (newDancer != null)
        {
            if (newDancer.danceFloor != this)
            {
                //Debug.Log("New dancer " + newDancer, newDancer);
                //Debug.Log(newDancer.danceFloor.DancersCount(), newDancer.danceFloor);
                if (newDancer.danceFloor.DancersCount() > newDancer.danceFloor.capacity)
                {
                    newDancer.danceFloor.RemoveDancer(newDancer);
                    Destroy(newDancer.gameObject);
                    return false;
                }
            }

            var clouds = Instantiate(this.clouds);

            newDancer.transform.position = (dancer1.transform.position + dancer2.transform.position) / 2;
            clouds.transform.position = newDancer.transform.position + 0.4f * Vector3.up + 0.1f * Vector3.left;

            var floorOrigin = dancer1.danceFloor;

            RemoveDancer(dancer1);

            RemoveDancer(dancer2);

            Destroy(dancer1.gameObject);

            Destroy(dancer2.gameObject);

            GameController.Instance().DancersMerged(newDancer);

            //Debug.Log(floorOrigin + " " + newDancer.danceFloor + " " + this);

            if (newDancer.danceFloor != this)
            {
                
                Dancer dummy = Instantiate(newDancer);
                //Debug.Log("Here", dummy);
                //UnityEditor.EditorApplication.isPaused = true;
                dummy.GetComponent<DragNDrop>().disabled = true;
                dummy.merging = true;
                dummy.gameObject.AddComponent<Disappear>();

                dummy.transform.SetParent(floorOrigin.dancersOrigin);
                dummy.transform.position = newDancer.transform.position;

                Vector3 shift = newDancer.transform.position - floorOrigin.dancersOrigin.position;
                newDancer.transform.position = newDancer.danceFloor.dancersOrigin.position + shift;
            }

            return true;
        }

        return false;
    }

    public int DancersCount ()
    {
        return dancers.Count;
    }

    public Dancer GetDancer(int i)
    {
        return dancers[i];
    }

    public int GetDancerID (int type)
    {
        return type;
    }

    public void Clear ()
    {
        for (int i = 0; i < dancers.Count; i++)
        {
            Merger.Instance().UnregisterDancer(dancers[i]);
            Destroy(dancers[i].gameObject);
        }
        dancers.Clear();
    }

    public void UpdateDancer (Dancer dancer)
    {
        DancerData data = new DancerData();
        if (Saver.Instance().current.GetValue<DancerData>("DanceFloor.dancers", dancer.indexInSave, ref data))
        {
            data.position = dancer.transform.position;
            Saver.Instance().SetValue<DancerData>("DanceFloor.dancers", dancer.indexInSave, data);
        }
    }

    public void PrepareForSaving(SaveableData data)
    {
        //data.dancers.Clear();
    }

    public void Save(SaveableData data)
    {
        for (int j = 0; j < DancersCount(); j++)
        {
            DancerData dancer = new DancerData();
            dancer.floor = floorId;
            dancer.type = GetDancer(j).type;
            dancer.position = GetDancer(j).transform.position;
            dancer.active = GetDancer(j).gameObject.activeSelf;
            dancer.buyed = GetDancer(j).buyed;
            GetDancer(j).indexInSave = data.GetArrayCount("DanceFloor.dancers");
            data.AddValue<DancerData>("DanceFloor.dancers", dancer);
        }

        
        //if (floorId == 0) data.SetValue<int>("DanceFloor.typeToSpawn", typeToSpawn);

        if (floorId == 0) floorButton.SetActive(true);

        data.SetValue<bool>("IsFloorAvaliable" + floorId, floorButton.activeSelf);
    }

    public void Load(SaveableData data)
    {
        Clear();

        DancerData d = new DancerData();
        int cnt = data.GetArrayCount("DanceFloor.dancers");
        //Debug.Log("Load " + cnt + " dancers", this);
        for (int i = 0; i < cnt; i++)
        {
            data.GetValue<DancerData>("DanceFloor.dancers", i, ref d);
            if (d.floor != floorId) continue;

            Vector3 pos = d.position;

            Dancer dancer = GameController.Instance().Spawn(d.type);
            dancer.transform.SetParent(dancersOrigin);
            dancer.transform.position = pos;
            dancer.indexInSave = i;
            if(!d.active)
            {
                var prefab = elevatorPrefab;
                if (d.buyed) prefab = goldElevatorPrefab;

                dancer.gameObject.SetActive(false);
                Elevator elevator = Instantiate(prefab);
                elevator.transform.SetParent(dancersOrigin);
                elevator.transform.localPosition = dancer.transform.localPosition;
                //Vector3 scale = elevator.transform.parent.lossyScale;
                //scale.x = 1f / scale.x;
                //scale.y = 1f / scale.y;
                //scale.z = 1f / scale.z;
                //scale.Scale(elevatorPrefab.transform.lossyScale);
                elevator.transform.localScale = elevatorPrefab.transform.localScale;
                elevator.dancer = dancer;
                elevator.Stand();
                
                dancer.elevator = elevator;
            }
        }

        //typeToSpawn = -1;
        //if (floorId == 0)
        //{
        //    typeToSpawn = 0;
        //    data.GetValue<int>("DanceFloor.typeToSpawn", ref typeToSpawn);
        //}

        bool b = false;
        if (floorId == 0) b = true;
        else data.GetValue<bool>("IsFloorAvaliable" + floorId, ref b);

        floorButton.SetActive(b);
    }

    [System.Serializable]
    private class DancerData
    {
        public int floor;
        public int type;
        public Vector2 position;
        public bool active;
        public bool buyed;
    }
}
