﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraFitter : MonoBehaviour {

    public float width;

    public Camera cam;

	// Use this for initialization
	void Start () {

        if (cam != null)
        {

            float aspect = (float)Screen.width / Screen.height;

            cam.orthographicSize = 0.5f * width / aspect;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (cam == null) return;

        float aspect = (float)Screen.width / Screen.height;

        cam.orthographicSize = 0.5f * width / aspect;
	}
}
