﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MenuPanel : MonoBehaviour {

    //public enum Align
    //{
    //    center,
    //    up,
    //    down,
    //}

    public Camera cam;

    public SpriteRenderer back;

    //public Align align;

    private Vector2 partOfScreen = new Vector2(1, 1);

    public Vector2 size;

    public Vector2 position;

    public CameraTransition transiter;

    public List<RectTransform> menus;

    // Use this for initialization
    void Start()
    {
        transiter = cam.GetComponent<CameraTransition>();
    }

    private void Update()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (menus.Count > 0 && transiter != null)
        {
            for (int i = 0; i < menus.Count; i++)
            {
                var menu = menus[i];
                float w = menu.rect.width;
                Vector3 pos = menu.localPosition;
                float w2 = Screen.width * cam.orthographicSize * 2 / Screen.height;
                pos.x = (transform.position - transiter.transform.position).x * w / w2;
                menu.localPosition = pos;
            }
        }

        FitBackground();
    }

    public void FitBackground ()
    {
        if (cam == null || back == null) return;

        if (!cam.orthographic) return;

        float screetRatio = ((float)Screen.width) / Screen.height;

        float viewRatio = (Screen.width * partOfScreen.x) / (Screen.height * partOfScreen.y);

        float spriteRatio = back.sprite.rect.width / back.sprite.rect.height;

        float scale = 1;
        if (spriteRatio > viewRatio)
        {//подгоняем высоту
            float height = 2 * cam.orthographicSize * partOfScreen.y;
            float spriteSizeY = back.sprite.rect.height / back.sprite.pixelsPerUnit;
            scale = height / spriteSizeY;
            size.y = height;
            size.x = spriteRatio * height;
        }
        else
        {// подгоняем ширину
            float width = 2 * cam.orthographicSize * screetRatio * partOfScreen.x;
            float spriteSizeX = back.sprite.rect.width / back.sprite.pixelsPerUnit;
            scale = width / spriteSizeX;
            size.x = width;
            size.x = width / spriteRatio;
        }

        transform.localScale = new Vector3(scale, scale, scale);

        //if (align == Align.center)
        //{
        //    Vector3 pos = transform.position;
        //    pos.x = cam.transform.position.x;
        //    pos.y = cam.transform.position.y;
        //    transform.position = pos;
        //}

        //if (align == Align.up)
        //{
        //    float delta = -cam.orthographicSize + 0.5f * scale * back.sprite.rect.height / back.sprite.pixelsPerUnit + 2 * cam.orthographicSize * (1 - partOfScreen.y);
        //    Vector3 pos = transform.position;
        //    pos.x = cam.transform.position.x;
        //    pos.y = cam.transform.position.y + delta;
        //    transform.position = pos;
        //}

        //if (align == Align.down)
        //{
        //    float delta = cam.orthographicSize - 0.5f * scale * back.sprite.rect.height / back.sprite.pixelsPerUnit - 2 * cam.orthographicSize * (1 - partOfScreen.y);
        //    Vector3 pos = transform.position;
        //    pos.x = cam.transform.position.x;
        //    pos.y = cam.transform.position.y + delta;
        //    transform.position = pos;
        //}

        Vector3 pos = transform.position;

        pos.x = 0;// cam.transform.position.x;
        pos.x += position.x * size.x;
        pos.y = cam.transform.position.y;
        pos.y += position.y * size.y;

        transform.position = pos;
    }
}
