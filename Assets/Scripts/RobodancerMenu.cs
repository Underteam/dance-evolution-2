﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class RobodancerMenu : MonoBehaviour, ISaveable {

    public Robodancer player;
    public Robodancer opponent;

    public List<Robodance> dances;

    public Image btnBattle;
    //public Image btnInterface;
    //public Image btnTournament;

    public Sprite activeButton;
    public Sprite inactiveButton;

    public GameObject pnlBattle;
    public GameObject pnlInterface;
    //public GameObject pnlTournament;
    public GameObject pnlTimer;
    public GameObject pnlReward;

    public GameObject robot;
    public UIAnchored background;
    public RectTransform anchor1;
    public RectTransform anchor2;

    private bool isBattle;
    private DateTime endOfDance;
    private int playerDanceID;
    private int opponentDanceID;

    public Text lblBattleTime;

    public ScrollRect scroller;

    public Sprite sPlay;
    public Sprite sPause;

    public string stime;

    private Robodance currentDance;

    public Text lblCoinsReward;
    public Text lblCrystalsReward;
    private bool shouldReward;
    private int rewardedDance;
    private bool showrewardPanel;

    void Awake ()
    {
        Saver.Instance().Add(this);
    }

	// Use this for initialization
	void Start () {

        btnBattle.gameObject.SetActive(false);

        //ShowInterface();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (showrewardPanel)
        {
            showrewardPanel = false;
            if (rewardedDance >= 0 && rewardedDance < dances.Count)
            {
                currentDance = dances[rewardedDance];
                pnlBattle.SetActive(true);
                pnlInterface.SetActive(false);
                ShowRewardPanel();
            }
        }

        if (isBattle)
        {
            scroller.verticalNormalizedPosition = Mathf.Lerp(scroller.verticalNormalizedPosition, 1, 5 * Time.deltaTime);

            TimeSpan time = endOfDance - DateTime.Now;
            float ts = (float)time.TotalSeconds;
            if (ts > 0.5f)
            {
                lblBattleTime.text = TimeTostring(time);
                stime = TimeTostring(time);
            }
            else
            {
                isBattle = false;
                background.GetComponent<Animator>().SetBool("Battle", false);
                ShowRewardPanel();
                //FinishBattle();

                shouldReward = true;
                rewardedDance = currentDance.danceID;
                Saver.Instance().Save();
            }
        }

        if (currentDance != null && currentDance.danceID ==  player.Current())
        {
            int n = (int)(player.progress * currentDance.cards.Count);
            for(int i = 0; i < currentDance.cards.Count; i++) currentDance.cards[i].SetState(i == n);
           
        }
    }

    private void ShowRewardPanel ()
    {
        pnlTimer.SetActive(false);
        pnlReward.SetActive(true);

        if (currentDance != null)
        {
            lblCoinsReward.text = currentDance.coinsReward.Short();
            lblCrystalsReward.text = currentDance.crystalsReward.Short();
        }
    }

    public void FinishBattle ()
    {
        pnlBattle.SetActive(false);
        pnlInterface.SetActive(true);

        if(currentDance != null)
        {
            Wallet.GetWallet(Wallet.CurrencyType.coins).Add(currentDance.coinsReward);
            Wallet.GetWallet(Wallet.CurrencyType.crystals).Add(currentDance.crystalsReward);
        }

        shouldReward = false;
        Saver.Instance().SetValue<bool>("Robodancer.ShouldReward", false);
        Saver.Instance().Save();
    }

    public void Play(Robodance dance)
    {
        for(int i = 0; i < dances.Count; i++)
        {
            dances[i].btnPlay.sprite = sPlay;
        }

        if (player.Current() == dance.danceID)
        {
            currentDance = null;
            StopBlinks();
            player.SetDance(6);
            btnBattle.gameObject.SetActive(false);
        }
        else
        {
            currentDance = dance;
            player.SetDance(dance.danceID);
            dance.btnPlay.sprite = sPause;
            btnBattle.gameObject.SetActive(true);
        }
    }

    private void StopBlinks ()
    {
        for (int d = 0; d < dances.Count; d++)
        {
            for (int i = 0; i < dances[d].cards.Count; i++) dances[d].cards[i].SetState(false);
        }
    }

    public void PlayDance (int id)
    {
        player.SetDance(id);
    }

    public void Error ()
    {
        player.SetDance(5);
        Invoke("Idle", 1);

        btnBattle.gameObject.SetActive(false);

        for (int i = 0; i < dances.Count; i++)
        {
            dances[i].btnPlay.sprite = sPlay;
        }
    }

    public void Idle ()
    {
        player.SetDance(6);
    }

    public void StartBattle ()
    {
        pnlBattle.SetActive(true);
        pnlInterface.SetActive(false);

        pnlTimer.SetActive(true);
        pnlReward.SetActive(false);

        background.GetComponent<Animator>().SetBool("Battle", true);

        endOfDance = DateTime.Now;
        endOfDance = endOfDance.AddHours(12);
        endOfDance = endOfDance.AddMinutes(30);
        //endOfDance = endOfDance.AddSeconds(30);

        isBattle = true;

        opponent.SetDance(UnityEngine.Random.Range(0, 5));
    }

    public void ReduceBattleTime(int m)
    {
        endOfDance = endOfDance.AddMinutes(-m);
    }

    public void PrepareForSaving(SaveableData data)
    {
        
    }

    public void Save(SaveableData data)
    {
        data.SetValue<bool>("IsInBattle", isBattle);
        data.SetValue<string>("EndOfBattle", endOfDance.ToString());
        data.SetValue<int>("PlayerDanceID", player.Current());
        data.SetValue<int>("OpponentDanceID", opponent.Current());
        data.SetValue<bool>("Robodancer.ShouldReward", shouldReward);
        data.SetValue<int>("Robodancer.RewardedDance", rewardedDance);
    }

    public void Load(SaveableData data)
    {
        data.GetValue<bool>("IsInBattle", ref isBattle);
        string str = "";
        data.GetValue<string>("EndOfBattle", ref str);
        data.GetValue<int>("PlayerDanceID", ref playerDanceID);
        data.GetValue<int>("OpponentDanceID", ref opponentDanceID);

        if(isBattle && DateTime.TryParse(str, out endOfDance))
        {
            player.SetDance(playerDanceID);
            opponent.SetDance(opponentDanceID);

            for (int i = 0; i < dances.Count; i++)
            {
                if(dances[i].danceID == playerDanceID) dances[i].btnPlay.sprite = sPause;
            }

            btnBattle.gameObject.SetActive(true);

            pnlBattle.SetActive(true);
            pnlInterface.SetActive(false);

            pnlTimer.SetActive(true);
            pnlReward.SetActive(false);

            background.GetComponent<Animator>().SetBool("Battle", true);
        }

        shouldReward = false;
        data.GetValue<bool>("Robodancer.ShouldReward", ref shouldReward);

        rewardedDance = -1;
        data.GetValue<int>("Robodancer.RewardedDance", ref rewardedDance);

        showrewardPanel = !isBattle && shouldReward;
    }

    public string TimeTostring (TimeSpan time)
    {
        int d = time.Days;
        int h = time.Hours;
        int m = time.Minutes;
        int s = time.Seconds;

        //string str_d = "" + d;
        //if (d < 10) str_d = "0" + d;

        string str_h = "" + h;
        if (h < 10) str_h = "0" + h;

        string str_m = "" + m;
        if (m < 10) str_m = "0" + m;

        //string str_s = "" + s;
        //if (s < 10) str_s = "0" + s;

        return str_h + "h " + str_m + "m";
    }

    public int ReadyCardsNum ()
    {
        int n = 0;
        for (int i = 0; i < dances.Count; i++) n += dances[i].ReadyCardsNum();
        return n;
    }
}
