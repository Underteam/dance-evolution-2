﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControllingDisabler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {
        if (!enabled) return;

        if (!CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Add(gameObject);
    }

    private void OnDisable()
    {
        if (quit) return;
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    private bool quit;
    private void OnApplicationQuit()
    {
        quit = true;
    }
}
