﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TableColumns
{
    public int level;
    public string price;
}
