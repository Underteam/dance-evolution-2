﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrades : MonoSingleton<Upgrades> {

    public List<Table> tables;

	// Use this for initialization
	void Start () {
		
	}
	
	public string GetPrice (string parameter, int level)
    {
        if (level < 0) return "0";

        for(int i = 0; i < tables.Count; i++)
        {
            if (tables[i].parameterName.Equals(parameter))
            {
                if(level < tables[i].rows.Count) return tables[i].rows[level].price;
            }
        }

        return "0";
    }

    public bool IsUpgradeAvaliable (string parameter, int level)
    {
        if (level < 0) return false;

        for (int i = 0; i < tables.Count; i++)
        {
            if (tables[i].parameterName.Equals(parameter))
            {
                return level < tables[i].rows.Count;
            }
        }

        return false;
    }
}
