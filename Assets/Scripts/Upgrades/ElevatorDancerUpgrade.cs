﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Numerics;

public class ElevatorDancerUpgrade : MonoBehaviour, ISaveable
{
    //public DanceFloor danceFloor;

    private int level;

    public Text lblPrice;

    public Text lblInfo;

    public Image imgCoin;

    public Image imgCrystal;

    private Wallet currentWallet;

    private Wallet crystalsWallet;

    private Wallet coinsWallet;

    private BigInteger price;

    private void Awake()
    {
        Saver.Instance().Add(this);
    }

    // Use this for initialization
    void Start()
    {
        coinsWallet = Wallet.GetWallet(Wallet.CurrencyType.coins);
        crystalsWallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        currentWallet = coinsWallet;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Apply()
    {
        if (Upgrades.Instance().IsUpgradeAvaliable("ElevatorDancer", level))
        {
            price = new BigInteger(Upgrades.Instance().GetPrice("ElevatorDancer", level));

            if (price.IsNegative)
            {
                price = -price;
                imgCoin.gameObject.SetActive(false);
                imgCrystal.gameObject.SetActive(true);
                currentWallet = crystalsWallet;
            }
            else
            {
                imgCoin.gameObject.SetActive(true);
                imgCrystal.gameObject.SetActive(false);
                currentWallet = coinsWallet;
            }

            lblPrice.text = price.Short();
        }
        else
        {
            lblPrice.text = "--";
            lblPrice.transform.parent.gameObject.SetActive(false);
        }

        GameController.Instance().typeToSpawn = level;
        Debug.LogWarning("set to " + level);

        lblInfo.text = "Уровень " + (level + 1);

        DancersShop.Instance().NewDancer(level);

        DancersShop.Instance().CheckSigns();
    }

    public void Upgrade()
    {
        if (!Upgrades.Instance().IsUpgradeAvaliable("ElevatorDancer", level)) return;

        if (currentWallet.amount < price) return;
        currentWallet.Spend(price);

        level++;
        Saver.Instance().SetValue<int>("ElevatorDancerUpgrades", level);
        Apply();
    }

    public void PrepareForSaving(SaveableData data)
    {

    }

    public void Load(SaveableData data)
    {
        level = 0;
        data.GetValue<int>("ElevatorDancerUpgrades", ref level);
        Apply();
    }

    public void Save(SaveableData data)
    {
        data.SetValue<int>("ElevatorDancerUpgrades", level);
    }
}
