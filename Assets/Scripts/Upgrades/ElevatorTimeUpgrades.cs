﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Numerics;

public class ElevatorTimeUpgrades : MonoBehaviour, ISaveable
{
    //public DanceFloor danceFloor;

    private int level;

    public Text lblPrice;

    public Text lblInfo;

    public Image imgCoin;

    public Image imgCrystal;

    private Wallet currentWallet;

    private Wallet crystalsWallet;

    private Wallet coinsWallet;

    private BigInteger price;

    private void Awake()
    {
        Saver.Instance().Add(this);
    }

    // Use this for initialization
    void Start () {

        coinsWallet = Wallet.GetWallet(Wallet.CurrencyType.coins);
        crystalsWallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        currentWallet = coinsWallet;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Apply ()
    {
        if (Upgrades.Instance().IsUpgradeAvaliable("ElevatorTime", level))
        {
            price = new BigInteger(Upgrades.Instance().GetPrice("ElevatorTime", level));

            if (price.IsNegative)
            {
                price = -price;
                imgCoin.gameObject.SetActive(false);
                imgCrystal.gameObject.SetActive(true);
                currentWallet = crystalsWallet;
            }
            else
            {
                imgCoin.gameObject.SetActive(true);
                imgCrystal.gameObject.SetActive(false);
                currentWallet = coinsWallet;
            }

            lblPrice.text = price.Short();
        }
        else
        {
            lblPrice.text = "--";
            lblPrice.transform.parent.gameObject.SetActive(false);
        }

        float spawnTime = 1 * (10 - level);

        lblInfo.text = spawnTime + " сек";

        GameController.Instance().spawnTime = spawnTime;
    }

    public void Upgrade ()
    {
        if (!Upgrades.Instance().IsUpgradeAvaliable("ElevatorTime", level)) return;

        if (currentWallet.amount < price) return;
        currentWallet.Spend(price);

        level++;
        Saver.Instance().SetValue<int>("ElevatorTimeUpgrades", level);
        Apply();
    }

    public void PrepareForSaving(SaveableData data)
    {
        
    }

    public void Load(SaveableData data)
    {
        level = 0;
        data.GetValue<int>("ElevatorTimeUpgrades", ref level);
        Apply();
    }

    public void Save(SaveableData data)
    {
        data.SetValue<int>("ElevatorTimeUpgrades", level);
    }
}
