﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Numerics;

public class DanceCardChanceUpgrades : MonoBehaviour, ISaveable, ISignable
{
    private int level;

    public Text lblPrice;

    public Text lblInfo;

    public Image imgCoin;

    public Image imgCrystal;

    private Wallet currentWallet;

    private Wallet crystalsWallet;

    private Wallet coinsWallet;

    private BigInteger price;

    public Transform signPos;

    public SignPos signOnMenu;

    private bool showSign;

    private int currentPage = 2;

    private void Awake()
    {
        Saver.Instance().Add(this);
    }

    // Use this for initialization
    void Start()
    {
        coinsWallet = Wallet.GetWallet(Wallet.CurrencyType.coins);
        crystalsWallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        currentWallet = coinsWallet;

        CameraTransition.Instance().onGoTo += (p) =>
        {
            if (currentPage == 4 && p != 4)
            {
                SignsManager.Instance().RemoveSign(signOnMenu);
            }
            currentPage = p;
        };
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Apply()
    {
        if (Upgrades.Instance().IsUpgradeAvaliable("DanceCardChance", level))
        {
            price = new BigInteger(Upgrades.Instance().GetPrice("DanceCardChance", level));

            if (price.IsNegative)
            {
                price = -price;
                imgCoin.gameObject.SetActive(false);
                imgCrystal.gameObject.SetActive(true);
                currentWallet = crystalsWallet;
            }
            else
            {
                imgCoin.gameObject.SetActive(true);
                imgCrystal.gameObject.SetActive(false);
                currentWallet = coinsWallet;
            }

            lblPrice.text = price.Short();
        }
        else
        {
            lblPrice.text = "--";
            lblPrice.transform.parent.gameObject.SetActive(false);
        }

        lblInfo.text = (1+level) * 5 + "%";

        GameController.Instance().cardChance = (1 + level) * 0.05f;
    }

    public void Upgrade()
    {
        if (!Upgrades.Instance().IsUpgradeAvaliable("DanceCardChance", level)) return;

        if (currentWallet.amount < price) return;
        currentWallet.Spend(price);

        level++;
        Saver.Instance().SetValue<int>("DanceCardChanceUpgrades", level);
        Apply();

        if (showSign)
        {
            showSign = false;
            SignsManager.Instance().RemoveSign(this);
            SignsManager.Instance().RemoveSign(signOnMenu);
            Saver.Instance().SetValue<bool>("DanceCardChanceUpgrades.ShowSign", false);
            Saver.Instance().Save();
        }
    }

    public void PrepareForSaving(SaveableData data)
    {

    }

    public void Load(SaveableData data)
    {
        level = 0;
        data.GetValue<int>("DanceCardChanceUpgrades", ref level);
        Apply();

        bool avaliable = false;
        Saver.Instance().GetValue<bool>("IsDanceCardUpgradeAvaliable", ref avaliable);
        if (!avaliable) gameObject.SetActive(false);

        showSign = false;
        Saver.Instance().GetValue<bool>("DanceCardChanceUpgrades.ShowSign", ref showSign);

        //Debug.LogError (avaliable + " " + showSign);

        if (showSign)
        {
            SignsManager.Instance().ShowSign(this);
            SignsManager.Instance().ShowSign(signOnMenu);
        }
    }

    public void Save(SaveableData data)
    {
        //Debug.LogError(gameObject.activeSelf + " " + showSign);

        data.SetValue<int>("DanceCardChanceUpgrades", level);
        Saver.Instance().SetValue<bool>("DanceCardChanceUpgrades.ShowSign", showSign);
        Saver.Instance().SetValue<bool>("IsDanceCardUpgradeAvaliable", gameObject.activeSelf);
    }

    public Transform GetSignPlace()
    {
        return signPos;
    }

    public bool Overlayed ()
    {
        return false;
    }

    [EditorButton]
    public void Show ()
    {
        //Debug.LogError("show");
        gameObject.SetActive(true);
        SignsManager.Instance().ShowSign(this);
        SignsManager.Instance().ShowSign(signOnMenu);
        showSign = true;
        Saver.Instance().SetValue<bool>("DanceCardChanceUpgrades.ShowSign", true);
        Saver.Instance().SetValue<bool>("IsDanceCardUpgradeAvaliable", true);
        Saver.Instance().Save();
    }
}
