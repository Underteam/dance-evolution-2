﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Table", menuName = "Table/Table", order = 2)]
public class Table : ScriptableObject {

    public string parameterName;

    public List<TableColumns> rows;
}
