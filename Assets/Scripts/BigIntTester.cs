﻿using Numerics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigIntTester : MonoBehaviour {

    public string number;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [EditorButton]
    public void Test ()
    {
        //BigInteger.log = true;
        BigInteger num = new BigInteger(number);
        //BigInteger.log = false;

        Debug.Log(num + " -> " + num.Short());
    }
}
