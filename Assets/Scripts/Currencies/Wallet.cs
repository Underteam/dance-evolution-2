﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Numerics;

public class Wallet : MonoBehaviour, ISaveable {

    public enum CurrencyType
    {
        coins,
        crystals,
        ads,
    }

    public string description;

    public CurrencyType currency;

    public int defaultAmount = 0;

    private static List<Wallet> wallets = new List<Wallet>();

    public System.Action<Wallet> onValueChanged;

    [SerializeField]
    public BigInteger amount { get; private set; }

    [HideInInspector]
    public bool developerMode;

	// Use this for initialization
	void Awake () {

        Saver.Instance().Add(this);
    }

    private bool inited;
    private void Init ()
    {
        if (!wallets.Contains(this)) wallets.Add(this);
        
        string value = "" + defaultAmount;
        Saver.Instance().GetValue<string>("Wallet_" + currency + "_" + description, ref value);
        amount = new BigInteger(value);

        inited = true;
    }

    private static List<Wallet> results = new List<Wallet>();
    public static Wallet GetWallet (CurrencyType currency, string description = "")
    {
        results.Clear();

        for(int i = 0; i < wallets.Count; i++)
        {
            if(wallets[i] == null)
            {
                wallets.RemoveAt(i);
                i--;
            }

            if (wallets[i].currency == currency) results.Add(wallets[i]);
        }

        if(results.Count > 0)
        {
            if (string.IsNullOrEmpty(description)) return results[0];
            for (int i = 0; i < results.Count; i++)
                if (results[i].description.Equals(description)) return results[i];
        }

        results.Clear();

        var list = FindObjectsOfType<Wallet>();
        for(int i = 0; i < list.Length; i++)
        {
            if(list[i].currency == currency)
            {
                wallets.Add(list[i]);
                results.Add(list[i]);
            }
        }

        if (results.Count > 0)
        {
            if (string.IsNullOrEmpty(description)) return results[0];
            for (int i = 0; i < results.Count; i++)
                if (results[i].description.Equals(description)) return results[i];
        }

        GameObject go = new GameObject("Wallet" + currency);

        var wallet = go.AddComponent<Wallet>();
        wallet.currency = currency;
        wallet.description = description;
        wallet.Init();
        wallets.Add(wallet);

        return wallet;
    }

    public bool Spend (BigInteger amount)
    {
        if (!inited) Init();

        if (developerMode) Add(2 * amount);

        if (this.amount < amount) return false;

        this.amount -= amount;

        Saver.Instance().SetValue<string>("Wallet_" + currency + "_" + description, this.amount.ToString());

        if (onValueChanged != null) onValueChanged(this);

        return true;
    }

    public void Add (BigInteger amount)
    {
        if (!inited) Init();

        //Debug.Log("Add " + amount);

        if (amount <= 0) return;

        this.amount += amount;

        Saver.Instance().SetValue<string>("Wallet_" + currency + "_" + description, this.amount.ToString());

        if (onValueChanged != null) onValueChanged(this);
    }

    public void PrepareForSaving(SaveableData data)
    {
        
    }

    public void Save(SaveableData data)
    {
        data.SetValue<string>("Wallet_" + currency + "_" + description, this.amount.ToString());
    }

    public void Load(SaveableData data)
    {
        string value = "" + defaultAmount;
        data.GetValue<string>("Wallet_" + currency + "_" + description, ref value);

        amount = new BigInteger(value);
    }
}
