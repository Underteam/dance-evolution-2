﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DragNDropImage : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	private Image target;

	private Vector2 pointerStartPos;

	private Vector2 targetStartPos;

	private RectTransform canvasRT;

	private float canvasWidth;
	private float canvasHeight;
	private float screenWidth;
	private float screenHeight;

	public UnityEvent onDragStart;

	public UnityEvent onDrop;

	private bool inited;

	private void Init() {
	
		target = GetComponent<Image> ();

		canvasRT = target.canvas.GetComponent<RectTransform>();

		canvasWidth = canvasRT.sizeDelta.x;

		canvasHeight = canvasRT.sizeDelta.y;

		screenWidth = Screen.width;

		screenHeight = Screen.height;

		inited = true;
	}

	private PointerEventData pointer;

	public void OnPointerDown(PointerEventData data) {
	
		if (!inited) Init ();

		if (onDragStart != null)
			onDragStart.Invoke ();
	}

	public void OnPointerUp(PointerEventData data) {

		if (onDrop != null)
			onDrop.Invoke ();
	}

	public void OnDrag(PointerEventData data) {
	
		Vector2 delta = data.delta;
		delta.x *= canvasWidth/screenWidth;
		delta.y *= canvasHeight/screenHeight;
		target.rectTransform.anchoredPosition += delta;
	}
}
