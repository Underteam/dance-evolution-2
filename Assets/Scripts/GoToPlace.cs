﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPlace : MonoBehaviour {

    public Transform targetPlace;
    public float speed = 10;

    public System.Action onFinish;

    private bool finished;

    private Vector3 targetScale;

    private float startDist;

	// Use this for initialization
	void Start () {

        Vector3 dir = targetPlace.position - transform.position;
        startDist = dir.magnitude;
        targetScale = transform.localScale * 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
	
        if (targetPlace != null && !finished)
        {
            Vector3 dir = targetPlace.position - transform.position;
            float dist = dir.magnitude;
            float delta = speed * Time.deltaTime;
            dir.Normalize();

            if (dist <= delta)
            {
                delta = dist;
                finished = true;
            }

            transform.position += delta * dir;

            transform.localScale = Vector3.Lerp(targetScale, transform.localScale, dist / startDist);
        }
        else
        {
            if (onFinish != null) onFinish();
            Destroy(gameObject);
        }
	}
}
