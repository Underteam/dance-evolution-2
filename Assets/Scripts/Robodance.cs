﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteInEditMode]
public class Robodance : MonoBehaviour, ISaveable {

    public int danceID;

    public List<DanceCard> cards;

    private List<bool> cardsStates = new List<bool>();

    public RobodancerMenu menu;

    public Image btnPlay;

    public ProgressBar progress;
    public Text lblProgress;

    public Text lblCoinsReward;
    public Text lblCrystalsReward;

    public string _coinsReward;
    public string _crystalsReward;

    public Numerics.BigInteger coinsReward;
    public Numerics.BigInteger crystalsReward;

    void Awake ()
    {
        Saver.Instance().Add(this);
    }

    // Use this for initialization
    void Start () {

        coinsReward = new Numerics.BigInteger(_coinsReward);
        crystalsReward = new Numerics.BigInteger(_crystalsReward);

        lblCoinsReward.text = coinsReward.Short();
        lblCrystalsReward.text = crystalsReward.Short();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (btnPlay == null) btnPlay = transform.Find("Play").GetComponent<Image>();	
	}

    public void Play ()
    {
        bool full = true;
        for (int i = 0; i < cardsStates.Count; i++)
        {
            if (!cardsStates[i])
            {
                full = false;
                cards[i].GetComponentInChildren<Animator>(true).gameObject.SetActive(true);
            }
        }

        if (full)
        {
            menu.Play (this);
        }
        else
        {
            menu.Error();
        }
    }

    public void CheckCard(int danceID, int cardID)
    {
        Debug.Log("Check " + danceID + " " + cardID, this);

        if (danceID != this.danceID) return;
        if (cardID < 0 || cardID >= cardsStates.Count) return;
        if (cardsStates[cardID]) return;

        Debug.Log("ok");

        cardsStates[cardID] = true;

        Saver.Instance().SetValue<bool>("Dance" + danceID + "CardState", cardID, true);

        cards[cardID].SetSprite(Library.Instance().fullCard, true);

        int n = 0;
        for (int i = 0; i < cardsStates.Count; i++) n += cardsStates[i] ? 1 : 0;

        progress.SetValue((float)n / cardsStates.Count);
        lblProgress.text = n + "/" + cardsStates.Count;
    }

    public bool IsCardReady (int cardID)
    {
        if (cardID < 0 || cardID >= cardsStates.Count) return false;

        return cardsStates[cardID];
    }

    public void Load (SaveableData data)
    {
        bool isActive = false;

        cardsStates.Clear();
        for (int i = 0; i < cards.Count; i++) {
            isActive = false;
            data.GetValue<bool>("Dance" + danceID + "CardState", i, ref isActive);
            cardsStates.Add(isActive);
        }

        //Debug.Log("Load robodance " + danceID + " | " + cards.Count + " " + data.GetArrayCount("Dance" + danceID + "CardState"));

        for (int i = 0; i < cards.Count; i++)
        {
            if (cardsStates[i]) cards[i].SetSprite(Library.Instance().fullCard, true);
            else cards[i].SetSprite(Library.Instance().emptyCard, false);
        }

        int num = data.GetArrayCount("Dance" + danceID + "CardState");

        if (cards.Count >= num)
        {
            for (int i = num; i <= cards.Count; i++) data.AddValue<bool>("Dance" + danceID + "CardState", false);
        }

        int n = 0;
        for (int i = 0; i < cardsStates.Count; i++) n += cardsStates[i] ? 1 : 0;

        progress.SetValue((float)n / cardsStates.Count);
        lblProgress.text = n + "/" + cardsStates.Count;
    }

    public void PrepareForSaving (SaveableData data)
    {
        
    }

    public void Save (SaveableData data)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            bool isActive = cardsStates[i];
            data.AddValue<bool>("Dance" + danceID + "CardState", isActive);
        }
    }

    public int ReadyCardsNum ()
    {
        int n = 0;
        for (int i = 0; i < cardsStates.Count; i++) if (cardsStates[i]) n++;
        return n;
    }
}
