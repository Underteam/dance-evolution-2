﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickFinger : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(target != null) transform.position = target.position + 0.5f * Vector3.up;
	}
}
