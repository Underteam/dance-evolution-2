﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeFinger : MonoBehaviour {

    public Transform point1;
    public Transform point2;

    public SpriteRenderer finger;

    public Sprite fingerUp;
    public Sprite fingerDown;

    private int direction = 1;

    private Vector3 vel;

    private float timer;

	// Use this for initialization
	void Start () {

        direction = -1;
	}
	
	// Update is called once per frame
	void Update () {

        if (point1 == null || point2 == null) return;

        if(timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else if(direction == 1)
        {
            transform.position = Vector3.SmoothDamp(transform.position, point2.position + 0.5f * Vector3.up, ref vel, 0.15f);
            if (Vector3.Distance(transform.position, point2.position + 0.5f * Vector3.up) < 0.01f)
            {
                finger.sprite = fingerUp;
                timer = 0.5f;
                direction = -1;
            }
        }
        else
        {
            transform.position = Vector3.SmoothDamp(transform.position, point1.position + 0.5f * Vector3.up, ref vel, 0.15f);
            if (Vector3.Distance(transform.position, point1.position + 0.5f * Vector3.up) < 0.01f)
            {
                finger.sprite = fingerDown;
                timer = 0.2f;
                direction = 1;
            }
        }
	}
}
