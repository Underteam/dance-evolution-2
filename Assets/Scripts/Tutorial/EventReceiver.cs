﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventReceiver : MonoBehaviour {

    public UnityEvent onEvent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void HandleEvent ()
    {
        if (onEvent != null) onEvent.Invoke();
    }
}
