﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoSingleton<Tutorial>, ISaveable {

    public DanceCardChanceUpgrades upgrade;

    private int step;

    private bool passed;

    public SignPos dancersShopSign;

    public ClickFinger clickFinger;

    public SwipeFinger swipeFinger;

    public DownMenu downMenu;

    public DanceCardChanceUpgrades cardChanceUpgrade;

    public RobodancerMenu robodancer;

    public Animator cardAnim;

    public SignPos shopSign;
    public SignPos upgradesSign;
    public SignPos robotSign;

    public GameObject btnBuy;

    protected override void Init()
    {
        Saver.Instance().Add(this);
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartTutorial ()
    {
        StartCoroutine(TutorialCoroutine());
    }

    private IEnumerator TutorialCoroutine ()
    {
        //yield return null;

        //step = 0;

        int currentPage = 2;
        CameraTransition.Instance().onGoTo += (p) =>
        {
            if (currentPage == 1 && p != 1)
            {
                SignsManager.Instance().RemoveSign(shopSign);
            }
            if (currentPage == 4 && p != 4)
            {
                SignsManager.Instance().RemoveSign(upgradesSign);
            }
            if (currentPage == 0 && p != 0)
            {
                SignsManager.Instance().RemoveSign(robotSign);
            }
            currentPage = p;
        };

        downMenu.SetState(0, false);
        downMenu.SetState(1, false);
        downMenu.SetState(3, false);
        downMenu.SetState(4, false);

        GameController.Instance().cardChance = 0;

        float spawnTime = GameController.Instance().spawnTime;
        GameController.Instance().spawnTime = -1;

        while (step == 0)
        {
            GameController.Instance().NewGame();
            Debug.LogWarning("Set -1");
            GameController.Instance().Spawn(0);
            DancersShop.Instance().lastAvaliable = 0;

            step++;
            yield return null;
        }

        clickFinger.gameObject.SetActive(true);
        clickFinger.transform.position = btnBuy.transform.position + 0.0f * Vector3.up;

        while (step == 1)
        {
            if (GameController.Instance().floors[0].DancersCount() == 2) step++;
            yield return null;
        }

        clickFinger.gameObject.SetActive(false);

        if (step == 2)
        {
            var dancer1 = GameController.Instance().floors[0].GetDancer(1);
            dancer1.block = true;

            clickFinger.gameObject.SetActive(true);

            clickFinger.transform.position = dancer1.transform.position + 0.5f * Vector3.up;
            //clickFinger.target = dancer.transform;

            while (!dancer1.gameObject.activeSelf)
            {
                yield return null;
            }

            clickFinger.gameObject.SetActive(false);

            var dancer0 = GameController.Instance().floors[0].GetDancer(0);
            dancer0.block = true;

            swipeFinger.gameObject.SetActive(true);

            swipeFinger.transform.position = dancer1.transform.position + 0.5f * Vector3.up;

            if (dancer0.transform.position.x < dancer1.transform.position.x)
            {
                swipeFinger.point1 = dancer0.transform;
                swipeFinger.point2 = dancer1.transform;
            }
            else
            {
                swipeFinger.point1 = dancer1.transform;
                swipeFinger.point2 = dancer0.transform;
            }

            while (dancer0 != null && dancer1 != null)
            {
                yield return null;
            }

            swipeFinger.gameObject.SetActive(false);
        }

        while (step == 2)
        {
            GameController.Instance().typeToSpawn = -1;
            if (DancersShop.Instance().lastDiscovered == 1)
            {
                SignsManager.Instance().RemoveSign(dancersShopSign);
                step++;
            }
            yield return null;
        }

        Debug.Log("step 3 " + step);
        GameController.Instance().spawnTime = spawnTime;

        if (step == 3)
        {
            GameController.Instance().typeToSpawn = 0;
        }

        while (step == 3)
        {
            if (DancersShop.Instance().lastDiscovered == 2) step++;
            if (step == 4) SignsManager.Instance().ShowSign(shopSign);
            yield return null;
        }

        downMenu.SetState(1, true);
        downMenu.SetState(3, true);

        Debug.Log("step 4 " + step);

        while (step == 4)
        {
            if (DancersShop.Instance().lastDiscovered == 3) step++;
            yield return null;
        }

        while (step == 5)
        {
            while (GameController.Instance().floors[0].DancersCount() == 0) yield return null;

            var i = GameController.Instance().floors[0].DancersCount();
            var dancer = GameController.Instance().floors[0].GetDancer(i-1);
            while (!dancer.gameObject.activeSelf && i >= 0)
            {
                dancer = GameController.Instance().floors[0].GetDancer(i);
                i++;
            }

            clickFinger.gameObject.SetActive(true);
            clickFinger.transform.position = dancer.transform.position + 0.5f * Vector3.up;
            clickFinger.target = dancer.transform;

            int n = 0;

            System.Action<Dancer> inc = (d) =>
            {
                if (d == dancer) n++;
            };

            GameController.Instance().onDancerClicked += inc;

            while (dancer != null && n < 5) yield return null;

            if (n >= 5) step++;

            clickFinger.gameObject.SetActive(false);

            GameController.Instance().onDancerClicked -= inc;
        }


        while(step == 6)
        {
            if (DancersShop.Instance().lastDiscovered == 6)
            {
                //открыть апгрейды
                step++;
            }
            if (step == 7) SignsManager.Instance().ShowSign(upgradesSign);
            yield return null;
        }

        downMenu.SetState(4, true);

        while (step == 7)
        {
            if (DancersShop.Instance().lastDiscovered == 12)
            {
                
                step++;
            }
            yield return null;
        }

        GameController.Instance().cardChance = 1f;

        Debug.Log("step 8 " + step);

        while (step == 8)
        {
            if (robodancer.ReadyCardsNum() > 0)
            {
                cardChanceUpgrade.Show();
                step++;
            }
            yield return null;
        }

        cardChanceUpgrade.Apply();

        downMenu.SetState(0, true);

        System.Action<int> onGoTo = (i) => { Debug.Log("Go to " + i);  if (i == 0) step++; };

        CameraTransition.Instance().onGoTo += onGoTo;

        if (step == 9) SignsManager.Instance().ShowSign(robotSign);

        while (step == 9)
        {
            yield return null;
        }

        CameraTransition.Instance().onGoTo -= onGoTo;

        if (step == 10)
        {
            cardAnim.enabled = true;
            cardAnim.SetTrigger("Appear");
        }

        while (step == 10)
        {
            yield return null;
        }

        cardAnim.enabled = false;

        Debug.Log("Finished");

        passed = true;
        Saver.Instance().Save();
    }

    public void CardPlaced ()
    {
        step++;
    }

    public void Load(SaveableData data)
    {
        passed = false;
        data.GetValue<bool>("TutorialPassed", ref passed);

        step = 0;
        data.GetValue<int>("TutorialStep", ref step);
    }

    public void PrepareForSaving(SaveableData data)
    {

    }

    public void Save(SaveableData data)
    {
        data.SetValue<bool>("TutorialPassed", passed);

        data.SetValue<int>("TutorialStep", step);
    }
}
