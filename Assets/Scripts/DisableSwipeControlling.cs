﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisableSwipeControlling : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

    void Awake ()
    {
        //EventTrigger tr = GetComponent<EventTrigger>();
        //if (tr == null) tr = gameObject.AddComponent<EventTrigger>();

        //EventTrigger.Entry entry = null;
        //for (int i = 0; i < tr.triggers.Count; i++)
        //{
        //    if (tr.triggers[i].eventID == EventTriggerType.PointerDown) {
        //        entry = tr.triggers[i];
        //        break;
        //    }
        //}

        //if (entry == null) {
        //    new EventTrigger.Entry();
        //    entry.eventID = EventTriggerType.PointerDown;
        //    tr.triggers.Add(entry);
        //}

        //entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>());
    }

    public void OnPointerDown (PointerEventData data)
    {
        if (!CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Add(gameObject);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    // Use this for initialization
    void Start () {
		
	}
}
