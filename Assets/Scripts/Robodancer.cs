﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Robodancer : MonoBehaviour {

    public SkeletonAnimation anim;

    private int current = 0;

    private float duration = 1f;

    public float progress { get { return timer / duration; } private set {} }

    private float timer;

	// Use this for initialization
	void Start ()
    {
        //anim.AnimationState.GetCurrent(0).
        //anim.skeletonDataAsset.GetSkeletonData()
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        while (timer > duration) timer -= duration;
	}

    public int Current ()
    {
        return current;
    }

    public void SetDance(int id)
    {
        var animations = anim.skeleton.data.animations;

        if (id < 0 || id >= animations.Count) return;

        current = id;

        anim.AnimationName = animations.Items[current].name;

        duration = animations.Items[current].duration;

        timer = 0;
    }

    public void Dance ()
    {
        var animations = anim.skeleton.data.animations;

        //int cnt = animations.Count;

        //current = (current + 1) % cnt;
        
        //for (int i = 0; i < cnt; i++)
        //{
        //    Debug.Log(animations.Items[i].name);
        //}

        anim.AnimationName = animations.Items[current].name;

        timer = 0;

        //Debug.Log(anim.AnimationName);
    }
}
