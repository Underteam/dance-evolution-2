﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignPos : MonoBehaviour, ISignable {

    public Transform signPos;

    public bool overlayed;

    // Use this for initialization
    void Start () {
		
	}

    public Transform GetSignPlace()
    {
        return signPos;
    }

    public bool Overlayed()
    {
        return overlayed;
    }
}
