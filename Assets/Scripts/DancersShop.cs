﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Numerics;

public class DancersShop : MonoSingleton<DancersShop>, ISaveable {

    public List<DanceFloor> floors;

    public Elevator elevatorPrefab;

    public int toBuy = 0;

    public int lastAvaliable = 0;//сколько тацоров доступно для покупки

    public int lastDiscovered = 0;

    public List<int> numBuyed;

    private List<int> marked = new List<int>();

    public Button btnBuy;
    public Image imgDancerHead;
    public Text lblbuyCost;

    public RectTransform topSpace;
    public RectTransform botSpace;
    public RectTransform parent;

    public DancerCard dancerCardPrefab;

    private List<DancerCard> cards = new List<DancerCard>();

    private List<Dancer.ExponentialValue> buyingCosts = new List<Dancer.ExponentialValue>();

    private Wallet wallet;

    public SignPos signPos;

    private int currentPage = 2;

    // Use this for initialization
    void Start () {

        wallet = Wallet.GetWallet(Wallet.CurrencyType.coins);

        CameraTransition.Instance().onGoTo += (p) =>
        {
            if (currentPage == 3 && p != 3)
            {
                SignsManager.Instance().RemoveSign(signPos);
            }
            currentPage = p;
        };
	}

    public string info;

	// Update is called once per frame
	void Update () {

        parent.sizeDelta = new Vector2(0, topSpace.anchoredPosition.y - botSpace.anchoredPosition.y);
    }

    private Dancer GetDancer (int id)
    {
        int fl = 0;
        int i = id;
        while (fl < floors.Count && i >= floors[fl].prefabs.Count)
        {
            i -= floors[fl].prefabs.Count;
            fl++;
        }

        if (fl >= floors.Count || i >= floors[fl].prefabs.Count)
        {
            Debug.LogError("Return null (" + id + ") " + fl + " " + i);
            return null;
        }

        return floors[fl].prefabs[i];
    }

    public bool Buy ()
    {
        //Debug.Log(toBuy + " " + lastAvaliable + " " + lastDiscovered);
        Debug.Log("Buy " + toBuy + " " + wallet.currency);

        if (toBuy > lastAvaliable) return false;

        BigInteger price = new BigInteger(0);

        if (wallet.currency == Wallet.CurrencyType.crystals) price = new BigInteger(9);
        else if (wallet.currency == Wallet.CurrencyType.ads) price = new BigInteger(0);
        else price = buyingCosts[toBuy].GetValue(numBuyed[toBuy]);

        Debug.Log("Try to buy " + toBuy + " " + wallet.amount + " " + price);
        if (wallet.amount < price) return false;

        //int fl = 0;
        //int i = toBuy;
        //while (fl < floors.Count && i >= floors[fl].prefabs.Count)
        //{
        //    i -= floors[fl].prefabs.Count;
        //    fl++;
        //}

        //if (fl < floors.Count && i < floors[fl].prefabs.Count && floors[fl].DancersCount() < floors[fl].capacity)
        {
            var dancer = GameController.Instance().Spawn(toBuy);

            if (dancer != null)
            {
                for (int n = numBuyed.Count; n < toBuy + 1; n++) numBuyed.Add(0);
                
                //if (wallet.currency == Wallet.CurrencyType.coins)

                numBuyed[toBuy]++;

                Debug.Log("Save DancersShop.NumBuyed" + toBuy + " " + numBuyed[toBuy]);

                Saver.Instance().SetValue<int>("DancersShop.NumBuyed" + toBuy, numBuyed[toBuy]);
                if (toBuy == 0 && numBuyed[toBuy] == 0) Debug.LogError("WTF???");

                dancer.buyed = true;
                dancer.gameObject.SetActive(false);
                Elevator elevator = Instantiate(elevatorPrefab);
                elevator.transform.SetParent(dancer.transform.parent);
                elevator.transform.localPosition = dancer.transform.localPosition;
                //Vector3 scale = elevator.transform.parent.lossyScale;
                //scale.x = 1f / scale.x;
                //scale.y = 1f / scale.y;
                //scale.z = 1f / scale.z;
                //scale.Scale(elevatorPrefab.transform.lossyScale);
                elevator.transform.localScale = elevatorPrefab.transform.localScale;
                elevator.dancer = dancer;
                dancer.elevator = elevator;

                wallet.Spend(price);

                GameController.Instance().DancerSpawned();
            }
        }

        lblbuyCost.text = buyingCosts[toBuy].GetValue(numBuyed[toBuy]).Short();

        cards[toBuy].lblCoinPrice.text = lblbuyCost.text;
        cards[toBuy].lblCrystalsPrice.text = "9";
        cards[toBuy].lblNumBuyed.text = "Куплено: " + numBuyed[toBuy];

        {
            var dancer = GetDancer (toBuy);

            if (dancer != null) imgDancerHead.sprite = dancer.dancerFace;
        }

        //Debug.Log("Try to remove sign from " + toBuy, cards[toBuy]);

        SignsManager.Instance().RemoveSign(cards[toBuy]);
        if (marked.Contains(toBuy)) marked.Remove(toBuy);
        if (marked.Count == 0) SignsManager.Instance().RemoveSign(signPos);

        return true;
    }

    public void BuyLast ()
    {
        Buy();
    }

    private bool waithingForAd;
    public bool Buy (int id, Wallet.CurrencyType currency)
    {
        if (id < 0 || id > lastAvaliable) return false;

        toBuy = id;

        if (currency == Wallet.CurrencyType.ads)
        {
            wallet = Wallet.GetWallet(Wallet.CurrencyType.ads);
            waithingForAd = true;
            AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
            {
                if (!waithingForAd) return;
                waithingForAd = false;

                if (!b) return;

                Buy();
            });
            return true;
        }
        else if (currency == Wallet.CurrencyType.coins) wallet = Wallet.GetWallet(Wallet.CurrencyType.coins);
        else if (currency == Wallet.CurrencyType.crystals) wallet = Wallet.GetWallet(Wallet.CurrencyType.crystals);

        return Buy();
    }

    public void PrepareForSaving(SaveableData data)
    {
        
    }

    public void Save(SaveableData data)
    {
        //Debug.Log("save last avaliable " + lastAvaliable);

        data.SetValue<int>("DancersShop.lastAvaliable", lastAvaliable);

        data.SetValue<int>("DancersShop.toBuy", toBuy);

        data.SetValue<int>("DancersShop.lastDiscovered", lastDiscovered);

        for (int i = 0; i < numBuyed.Count; i++)
        {
            data.SetValue<int>("DancersShop.NumBuyed" + i, numBuyed[i]);
            if (i == 0 && numBuyed[i] == 0) Debug.LogError("WTF???");
        }

        for (int i = 0; i < marked.Count; i++)
            data.SetValue<int>("DancersShop.Marked" + i, marked[i]);

        data.SetValue<int>("DancersShop.MarkedCount", marked.Count);
    }

    public void Load(SaveableData data)
    {
        buyingCosts.Clear();

        for(int i = 0; i < floors.Count; i++)
        {
            for(int j = 0; j < floors[i].prefabs.Count; j++)
            {
                buyingCosts.Add(floors[i].prefabs[j].buyingCost);
            }
        }

        data.GetValue<int>("DancersShop.lastAvaliable", ref lastAvaliable);

        data.GetValue<int>("DancersShop.toBuy", ref toBuy);

        data.GetValue<int>("DancersShop.lastDiscovered", ref lastDiscovered);

        NewDancerMenu.Instance().lastDiscovered = lastDiscovered;

        Debug.Log("Load DancersShop " + lastAvaliable + " " + lastDiscovered);

        numBuyed.Clear();
        for (int i = 0; i <= lastAvaliable; i++)
        {
            int n = 0;
            data.GetValue<int>("DancersShop.NumBuyed" + i, ref n);
            numBuyed.Add(n);
        }

        for (int n = numBuyed.Count; n < buyingCosts.Count + 1; n++) numBuyed.Add(0);

        Debug.Log ("Load DancersShop " + toBuy + " " + numBuyed.Count);

        //if (lastAvaliable >= 0) btnBuy.gameObject.SetActive(true);
        //else btnBuy.gameObject.SetActive(false);

        if (toBuy >= 0 && toBuy < numBuyed.Count)
        {
            //Debug.LogWarning(toBuy + " " + numBuyed[toBuy]);
            lblbuyCost.text = buyingCosts[toBuy].GetValue(numBuyed[toBuy]).Short();
        }

        {
            var dancer = GetDancer(toBuy);

            if (dancer != null) imgDancerHead.sprite = dancer.dancerFace;
        }

        marked.Clear();
        int num = 0;
        data.GetValue<int>("DancersShop.MarkedCount", ref num);
        for (int i = 0; i < num; i++)
        {
            int ind = -1;
            data.GetValue<int>("DancersShop.Marked" + i, ref ind);
            marked.Add(ind);
        }

        SpawnCards(false);

        CheckSigns();
    }

    public bool NewDancer (int ind)
    {
        bool isNew = false;
        if (ind > lastDiscovered)
        {
            lastDiscovered = ind;
            isNew = true;
        }

        lastAvaliable = Mathf.Max(lastAvaliable, lastDiscovered - 1);

        Debug.Log("set last avaliable " + lastAvaliable);

        Saver.Instance().SetValue<int>("DancersShop.lastAvaliable", lastAvaliable);

        //if (lastAvaliable >= 0) btnBuy.gameObject.SetActive(true);

        SpawnCards(isNew);

        return isNew;
    }

    private void SpawnCards (bool markLast)
    {
        //Debug.LogWarning ("Spawn " + cards.Count + " " + lastAvaliable);

        for (int i = cards.Count; i <= lastAvaliable; i++)
        {
            var card = Instantiate(dancerCardPrefab);
            card.transform.SetParent(dancerCardPrefab.transform.parent);
            card.transform.localScale = dancerCardPrefab.transform.localScale;
            card.gameObject.SetActive(true);
            card.dancerID = i;
            card.shop = this;
            cards.Add(card);

            var dancer = GetDancer(i);
            if(dancer != null)
            {
                //Debug.Log("Dancer" + i + " (" + numBuyed[i] + ")", dancer);
                card.portrait.sprite = dancer.dancerPortrait;
                card.lblName.text = dancer.dancerName;
                card.lblRate.text = dancer._earnings + "/сек";
                card.lblNumBuyed.text = "Куплено: " + numBuyed[i];

                //BigInteger.log = true;
                BigInteger price = buyingCosts[i].GetValue(numBuyed[i]);
                //BigInteger.log = false;
                card.lblCoinPrice.text = price.Short();
                card.lblCrystalsPrice.text = "9";

                //Debug.Log("Dancer " + i + " " + dancer, dancer);
            }
        }

        for(int i = 0; i < cards.Count; i++)
        {
            cards[i].SetPaymentMethods(true, false, false);
        }

        if(cards.Count > 2)
        {
            cards[cards.Count - 1].SetPaymentMethods(false, true, false);
            cards[cards.Count - 2].SetPaymentMethods(true, false, true);
        }
        else if(cards.Count > 1)
        {
            cards[cards.Count - 1].SetPaymentMethods(false, true, false);
        }

        botSpace.SetAsLastSibling();

        if (markLast)
        {
            SignsManager.Instance().ShowSign(cards[cards.Count - 1]);
            marked.Add(cards.Count - 1);
            if (marked.Count > 0) SignsManager.Instance().ShowSign(signPos);
        }
    }

    public void CheckSigns ()
    {
        int n = 0;
        for (int i = 0; i < marked.Count; i++)
        {
            if (marked[i] < 0 || marked[i] >= cards.Count)
            {
                Debug.Log(i + " " + marked[i] + " " + cards.Count);
                continue;
            }
            SignsManager.Instance().ShowSign(cards[marked[i]]);
            n++;
        }
        if (n > 0) SignsManager.Instance().ShowSign(signPos);
    }
}
