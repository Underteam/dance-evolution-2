﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Numerics;

public class Shop : MonoBehaviour {

    [System.Serializable]
    public class Product
    {
        public string name;
        public Wallet.CurrencyType priceType;
        public Wallet.CurrencyType rewardType;
        public string _price;
        public string _reward;
        public BigInteger price { get { return new BigInteger(_price); } private set { } }
        public BigInteger reward { get { return new BigInteger(_reward); } private set { } }
        public RewardProvider anotherReward;
        public FlyingCoin fcPrefab;
        public int n;
    }

    public List<Product> products;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private bool waitingForAd;
    public void Buy (int i)
    {
        Debug.Log("Buy " + i);

        if (i < 0 || i >= products.Count) return;

        var product = products[i];

        Wallet w1 = null;
        Wallet w2 = null;

        if(product.priceType == Wallet.CurrencyType.coins)
        {
            w1 = Wallet.GetWallet(Wallet.CurrencyType.coins);
        }
        else if(product.priceType == Wallet.CurrencyType.crystals)
        {
            w1 = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        }
        else w1 = Wallet.GetWallet(Wallet.CurrencyType.ads);

        if (product.rewardType == Wallet.CurrencyType.coins)
        {
            w2 = Wallet.GetWallet(Wallet.CurrencyType.coins);
        }
        else if (product.rewardType == Wallet.CurrencyType.crystals)
        {
            w2 = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        }

        if(w1.currency == Wallet.CurrencyType.ads)
        {
            waitingForAd = true;
            AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
            {
                Debug.Log("Ad finished " + b);

                if (!waitingForAd) return;
                waitingForAd = false;

                if (!b) return;

                if (product.anotherReward != null)
                    product.anotherReward.GetReward();
                else
                    w2.Add(product.reward);

                Debug.Log(product.n);
                GameController.Instance().AnimateFlyingCoins(product.fcPrefab, product.n, 0.4f, (a) => { /*w2.Add(n);*/ }, null);
            });
        }
        else if (product.price <= w1.amount)
        {
            w1.Spend(product.price);
            if (product.anotherReward != null)
                product.anotherReward.GetReward();
            else
                w2.Add(product.reward);

            Debug.Log(product.n);
            GameController.Instance().AnimateFlyingCoins(product.fcPrefab, product.n, 0.4f, (a) => { /*w2.Add(n);*/ }, null);
        }
    }
}
