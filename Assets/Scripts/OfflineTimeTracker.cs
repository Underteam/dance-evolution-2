﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfflineTimeTracker : MonoBehaviour {

    private static OfflineTimeTracker instance;

    public static OfflineTimeTracker Instance ()
    {
        if (instance == null) instance = FindObjectOfType<OfflineTimeTracker>();

        if(instance == null)
        {
            GameObject go = new GameObject("OfflineTimeTracker");
            instance = go.AddComponent<OfflineTimeTracker>();
        }

        return instance;
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(this);

        if (!HaveStamp(ref lastStamp)) NewStamp();
    }

    private System.DateTime lastStamp;

    public float autoStampInterval = -1;
    private float autoStampTimer;
    private bool wasOffline = true;

    public bool HaveStamp (ref System.DateTime stamp)
    {
        string strTime = "";
        if (PlayerPrefs.HasKey("LastTimeStamp"))
        {
            strTime = PlayerPrefs.GetString("LastTimeStamp", strTime);
            if (System.DateTime.TryParse(strTime, out stamp))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    public float GetSeconds ()
    {
        if (!HaveStamp(ref lastStamp)) NewStamp();

        if(offlineSeconds > 0)
        {
            offlineSeconds = 0;
            return offlineSeconds;
        }

        System.TimeSpan delta = System.DateTime.Now.Subtract(lastStamp);

        float totalSeconds = (float)delta.TotalSeconds;

        return totalSeconds;
    }

    public void NewStamp ()
    {
        lastStamp = System.DateTime.Now;

        PlayerPrefs.SetString("LastTimeStamp", lastStamp.ToString());

        offlineSeconds = 0;
    }

    private float offlineSeconds;
    private void AutoStamp()
    {
        if (wasOffline)
        {
            offlineSeconds = GetSeconds();
            wasOffline = false;
        }

        lastStamp = System.DateTime.Now;

        PlayerPrefs.SetString("LastTimeStamp", lastStamp.ToString());
    }

    private void Update()
    {
        if(autoStampInterval >= 0)
        {
            autoStampTimer -= Time.deltaTime;
            if(autoStampTimer <= 0)
            {
                AutoStamp();
                autoStampTimer = autoStampInterval;
            }
        }
    }
}
