﻿using Numerics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardProvider : MonoBehaviour {

    public float howManyHours;

    private Wallet wallet;

	// Use this for initialization
	void Start () {

        wallet = Wallet.GetWallet(Wallet.CurrencyType.coins);
	}

    public virtual string GetRewardDescription ()
    {
        return howManyHours.ToString("0.0") + "h";
    }

    public virtual void GetReward ()
    {
        BigInteger earn = GameController.Instance().earningsRate;
        earn = earn.Mult(howManyHours * 3600);

        Debug.Log("Get rewards for " + howManyHours + " -> " + GameController.Instance().earningsRate + " " + earn);

        wallet.Add(earn);
    }
}
