﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Merger : MonoSingleton<Merger> {

    private List<Dancer> dancers = new List<Dancer>();

    public float mergeDistance = 0.5f;

    private List<Pair> mergingPairs = new List<Pair>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        for(int i = 0; i < mergingPairs.Count; i++)
        {
            var pair = mergingPairs[i];

            var d1 = pair.dancer1;
            var d2 = pair.dancer2;

            Vector3 targetPos = (d1.transform.position + d2.transform.position) / 2;

            d1.transform.position = Vector3.Lerp(d1.transform.position, targetPos, 5 * Time.deltaTime);
            d2.transform.position = Vector3.Lerp(d2.transform.position, targetPos, 5 * Time.deltaTime);

            if(Vector3.Distance(d1.transform.position, d2.transform.position) < 0.1f)
            {
                if (!d1.danceFloor.Merge(d1, d2))
                {
                    d1.merging = false;
                    d1.GetComponent<DragNDrop>().disabled = false;
                    d2.merging = false;
                    d2.GetComponent<DragNDrop>().disabled = false;
                }
                mergingPairs.RemoveAt(i);
                i--;
            }
        }
	}

    public void RegisterDancer(Dancer dancer)
    {
        if (!dancers.Contains(dancer)) dancers.Add(dancer);
    }

    public void UnregisterDancer(Dancer dancer)
    {
        if (dancers.Contains(dancer)) dancers.Remove(dancer);
    }

    public void DancerDroped (Dancer dancer)
    {
        if (dancer == null) return;

        float min = float.MaxValue;
        int ind = -1;

        for (int i = 0; i < dancers.Count; i++)
        {
            if (dancers[i] == null) { dancers.RemoveAt(i); i--; continue; }
            if (dancers[i] == dancer) continue;
            if (dancers[i].merging) continue;
            if (dancers[i].danceFloor != dancer.danceFloor) continue;
            if (dancers[i].type != dancer.type) continue;

            float dist = Vector3.Distance(dancer.transform.position, dancers[i].transform.position);
            if (dist < min)
            {
                min = dist;
                ind = i;
            }
        }

        //Debug.Log("Found " + ind + " " + min);

        if (ind >= 0 && min <= 0.8f)
        {
            dancer.merging = true;
            dancers[ind].merging = true;

            dancer.GetComponent<DragNDrop>().disabled = true;
            dancers[ind].GetComponent<DragNDrop>().disabled = true;

            mergingPairs.Add(new Pair(dancer, dancers[ind]));
        }
        else
            GameController.Instance().DancerDroped(dancer);
    }

    private class Pair
    {
        public Dancer dancer1;
        public Dancer dancer2;

        public Pair(Dancer dancer1, Dancer dancer2)
        {
            this.dancer1 = dancer1;
            this.dancer2 = dancer2;
        }
    }
}
