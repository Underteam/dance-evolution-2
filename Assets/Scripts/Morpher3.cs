﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Morpher3 : MonoBehaviour {

    public Material mat;

    [Range(0f, 1f)]
    public float t;

    private float prevT = -1;

    public AnimationCurve curve;

	// Use this for initialization
	void Start () {

        mat.SetFloat("_PointsCount", 1000);
        float[] points = new float[1000];
        for(int i = 0; i < 1000; i++)
        {
            points[i] = curve.Evaluate(i / 999f);
        }
        mat.SetFloatArray("_Points", points);
	}
	
	// Update is called once per frame
	void Update () {
		
        if(t != prevT)
        {
            if(mat != null) mat.SetFloat("_T", t);
            prevT = t;
        }
	}
}
