﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownMenu : MonoBehaviour {

    private bool up;

    public List<Image> buttons;

    public List<Image> closedButtons;

    public List<Image> closedBlockedButtons;

    public List<Image> openedButtons;

    public List<RectTransform> discs;

    private int currentButton = 2;

    private Vector2 showPos = new Vector2(0, 100);

    private Vector2 hidePos = new Vector2(0, 0);

	// Use this for initialization
	void Start () {

        CameraTransition.Instance().onSwipeTo += OnSwipeTo;
	}
	
	// Update is called once per frame
	void Update () {
		
        for(int i = 0; i < discs.Count; i++)
        {
            if (i == currentButton) discs[i].anchoredPosition = Vector2.Lerp(discs[i].anchoredPosition, showPos, 10 * Time.deltaTime);
            else discs[i].anchoredPosition = Vector2.Lerp(discs[i].anchoredPosition, hidePos, 10 * Time.deltaTime);
        }
	}

    void LateUpdate ()
    {
        if(up)
        {
            if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
            up = false;
        }
    }

    public void GoTo (int i)
    {
        up = false;

        if (!CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Add(gameObject);

        GameController.Instance().GoToScreen(i);

        for (int j = 0; j < closedButtons.Count; j++) closedButtons[j].gameObject.SetActive(true);
        for (int j = 0; j < openedButtons.Count; j++) openedButtons[j].gameObject.SetActive(false);
        if (i >= 0 && i < openedButtons.Count) openedButtons[i].gameObject.SetActive(true);

        currentButton = i;
    }

    public void PointerUp ()
    {
        up = true;
    }

    public void OnSwipeTo (int i)
    {
        for (int j = 0; j < closedButtons.Count; j++) closedButtons[j].gameObject.SetActive(true);
        for (int j = 0; j < openedButtons.Count; j++) openedButtons[j].gameObject.SetActive(false);
        if (i >= 0 && i < openedButtons.Count) openedButtons[i].gameObject.SetActive(true);

        currentButton = i;
    }

    public void SetState(int i, bool state)
    {
        if (i < 0 || i >= buttons.Count) return;

        buttons[i].raycastTarget = state;

        closedButtons[i].gameObject.SetActive(state);

        closedBlockedButtons[i].gameObject.SetActive(!state);
    }
}
