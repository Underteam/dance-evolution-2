﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Morph : MonoBehaviour {

    public SpriteRenderer fromSR;

    public SpriteRenderer toSR;

    public SpriteRenderer resultSR;

    private System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

    private Sprite resultSprite;

    public Vector2 direction;

    [Range(0f, 1f)]
    public float t = 0;

    public AnimationCurve curve;

    public AnimationCurve curve2;

    public float speed = 1;

    public bool toLeft;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [EditorButton]
    public void Do ()
    {
        if (running) return;

        fromSR.enabled = (true);
        toSR.enabled = (false);
        resultSR.enabled = (false);

        StartCoroutine(Method3());
        //Method();
    }
    private bool running;
    //public IEnumerator Method()
    //{
    //    running = true;

    //    yield return new WaitForSeconds(0.5f);

    //    Sprite sprite1 = fromSR.sprite;

    //    Texture2D texture1 = sprite1.texture;
    //    Vector2Int offset = new Vector2Int((int)sprite1.rect.x, (int)sprite1.rect.y);

    //    int fullWidth = sprite1.texture.width;
    //    int w1 = (int)sprite1.textureRect.width;
    //    int h1 = (int)sprite1.textureRect.height;
    //    int w2 = 128;
    //    int h2 = 128;
    //    float ppu = 100;
    //    if (w1 > h1)
    //    {
    //        h2 = (int)((float)h1 * w2 / w1);
    //        ppu = sprite1.pixelsPerUnit * w2 / w1;
    //    }
    //    else
    //    {
    //        w2 = (int)((float)w1 * h2 / h1);
    //        ppu = sprite1.pixelsPerUnit * h2 / h1;
    //    }
    //    Texture2D texture2 = new Texture2D(w2, h2, TextureFormat.RGBA32, true, true);

    //    var pixels = new Color[w2 * h2];
    //    for (int i = 0; i < w2; i++)
    //    {
    //        for (int j = 0; j < h2; j++)
    //        {
    //            int ind = j * w2 + i;
    //            pixels[ind] = Color.clear;
    //        }
    //    }
    //    texture2.SetPixels(pixels);
    //    texture2.Apply();

    //    var pixels1 = texture1.GetPixels();
    //    var pixels2 = texture2.GetPixels();

    //    //float t = 0;
    //    //while (t < 1)
    //    {
    //        for (int i = 0; i < w2; i++)
    //        {
    //            int x1 = (int)(offset.x + (float)i * w1 / w2);
    //            int x2 = i;

    //            for (int j = 0; j < h2; j++)
    //            {
    //                int y1 = (int)(offset.y + (float)j * h1 / h2);
    //                int y2 = j;

    //                int ind1 = y1 * fullWidth + x1;
    //                int ind2 = y2 * w2 + x2;

    //                pixels2[ind2] = pixels1[ind1];
    //            }
    //        }

    //        texture2.SetPixels(pixels2);
    //        texture2.Apply();

    //        Sprite sprite2 = Sprite.Create(texture2, new Rect(0, 0,  w2, h2), new Vector2(0.5f, 0.5f), ppu, 1, SpriteMeshType.Tight, Vector4.zero, true);
    //        sprite2.name = "New Sprite";

    //        resultSR.sprite = sprite2;

    //        yield return null;

    //        //t += 0.2f * Time.deltaTime;
    //    }

    //    running = false;
    //}

    //private void Lerp (Color[] pixels1, Color[] pixels2, float t, int w1, int h1, int w2, int h2)
    //{

    //    //float x1Start = 0;
    //    //float x2Start = w2-1;
    //    //float x1End = w2;
    //    //float x2End = 2 * w2 - 1;

    //    //float x1 = Mathf.Lerp(x1Start, x1End, t);
    //    //float x2 = Mathf.Lerp(x2Start, x2End, t*2);

    //    for (int i = 0; i < w2; i++)
    //    {
    //        int x1 = (int)((float)i * w1 / w2);
    //        int x2_ = w2 + i;

    //        for (int j = 0; j < h2; j++)
    //        {
    //            int x2 = x2_;
    //            if ((j / 4) % 2 == 0) x2 = (int)Mathf.Lerp(x2_, x2_ + w2, t * (1f + 2f * ((float)i / w2)));
    //            else x2 = (int)Mathf.Lerp(x2_, x2_ + w2, t * (1f + 1f * ((float)i / w2)));

    //            int y1 = (int)((float)j * h1 / h2);
    //            int y2 = h2 + j;

    //            int ind1 = y1 * w1 + x1;
    //            int ind2 = y2 * 3 * w2 + x2;

    //            pixels2[ind2] = pixels1[ind1];
    //        }
    //    }
    //}

    //public IEnumerator Method2()
    //{
    //    running = true;

    //    Sprite sprite1 = sr1.sprite;

    //    Texture2D texture1 = sprite1.texture;
    //    Vector2Int offset = new Vector2Int((int)sprite1.rect.x, (int)sprite1.rect.y);

    //    int w1 = (int)sprite1.textureRect.width;
    //    int h1 = (int)sprite1.textureRect.height;
    //    int w2 = 128;
    //    int h2 = 128;
    //    float ppu = 100;
    //    if (w1 > h1)
    //    {
    //        h2 = (int)((float)h1 * w2 / w1);
    //        ppu = sprite1.pixelsPerUnit * w2 / w1;
    //    }
    //    else
    //    {
    //        w2 = (int)((float)w1 * h2 / h1);
    //        ppu = sprite1.pixelsPerUnit * h2 / h1;
    //    }
    //    Texture2D texture2 = new Texture2D(w2, h2, texture1.format, false, true);

    //    var pixels = new Color[w2 * h2];
    //    for (int i = 0; i < w2; i++)
    //    {
    //        for (int j = 0; j < h2; j++)
    //        {
    //            int ind = j * w2 + i;
    //            pixels[ind] = Color.clear;
    //        }
    //    }
    //    texture2.SetPixels(pixels);
    //    texture2.Apply();

    //    var pixels1 = texture1.GetPixels();
    //    var pixels2 = texture2.GetPixels();

    //    Vector3 pos = sr2.transform.position;

    //    float t = 0;
    //    while (t < 1)
    //    {
    //        if (t < 0.5f) Lerp2(pixels1, pixels2, 2 * t, w1, h1, w2, h2);
    //        else Lerp2(pixels1, pixels2, 2 * (1 - t), w1, h1, w2, h2);

    //        texture2.SetPixels(pixels2);
    //        texture2.Apply();

    //        sprite2 = Sprite.Create(texture2, new Rect(0, 0, w2, h2), new Vector2(0.5f, 0.5f), ppu, 1, SpriteMeshType.Tight, Vector4.zero, true);
    //        sprite2.name = "New Sprite";

    //        sr2.sprite = sprite2;

    //        sr2.transform.position = Vector3.Lerp(pos, sr1.transform.position, t);

    //        yield return null;

    //        t += 0.2f * Time.deltaTime;
    //    }

    //    running = false;
    //}

    //private void Lerp2(Color[] pixels1, Color[] pixels2, float t, int w1, int h1, int w2, int h2)
    //{
    //    for(int j = 0; j < h2; j++)
    //    {
    //        int y1 = (int)((float)j * h1 / h2);
    //        int y2 = j;

    //        float k = Mathf.Max(curve.Evaluate((float)j / (h2 - 1)), Mathf.Clamp(1 - t, 0, 1));

    //        int w = (int)(w2 * k);

    //        for(int i = 0; i < w; i++)
    //        {
    //            int x1 = (int)((float)i * w1 / w);
    //            int x2 = i;

    //            int ind1 = y1 * w1 + x1;
    //            int ind2 = y2 * w2 + x2;

    //            pixels2[ind2] = pixels1[ind1];
    //        }

    //    }
    //}

    public IEnumerator Method3()
    {
        running = true;

        yield return new WaitForSeconds(0.1f);

        Sprite fromSprite = fromSR.sprite;

        Texture2D fromTexture = fromSprite.texture;//оригинальный спрайт
        Vector2Int offset1 = new Vector2Int((int)fromSprite.rect.x, (int)fromSprite.rect.y);//положение спрайта в атласе
        int w1 = (int)fromSprite.rect.width;//ширина
        int h1 = (int)fromSprite.rect.height;//и высота спрайта (в пикселях?)

        int w2 = 128;//ширина
        int h2 = 128;//и высота текстуры для анимации
        float ppu = 100;
        if (w1 > h1)
        {
            h2 = (int)((float)h1 * w2 / w1);
            ppu = toSR.sprite.pixelsPerUnit * w2 / w1;
        }
        else
        {
            w2 = (int)((float)w1 * h2 / h1);
            ppu = toSR.sprite.pixelsPerUnit * h2 / h1;
        }
        Texture2D resultTexture = new Texture2D(2 * w2, h2, TextureFormat.RGBA32, false, true);//создаем текстуру для анимации в два раза длиннее чем оригинальный спрайт

        Sprite toSprite = toSR.sprite;//в какой спрайт будет превращаться изначальный
        Texture2D toTexture = toSprite.texture;
        Vector2Int offset3 = new Vector2Int((int)toSprite.rect.x, (int)toSprite.rect.y);//его позиция в атласе
        int w3 = (int)toSprite.rect.width;
        int h3 = (int)toSprite.rect.height;

        Color clear = Color.clear;
        var pixels = new Color[2 * w2 * h2];
        //Debug.Log(w2 + " " + h2 + " -> " + pixels.Length);
        for (int i = 0; i < 2 * w2; i++)
        {
            for (int j = 0; j < h2; j++)
            {
                int ind = j * 2 * w2 + i;
                pixels[ind] = clear;
            }
        }
        resultTexture.SetPixels(pixels);
        resultTexture.Apply();//заполняем текстуру для анимации прозрачным цветом

        var fromPixels = fromTexture.GetPixels((int)fromSprite.rect.x, (int)fromSprite.rect.y, (int)fromSprite.rect.width, (int)fromSprite.rect.height);//получаем пиксели исходной текстуры
        var resultPixels = resultTexture.GetPixels();
        var toPixels = toTexture.GetPixels((int)toSprite.rect.x, (int)toSprite.rect.y, (int)toSprite.rect.width, (int)toSprite.rect.height);//пиксели целевой текстуры

        Tex fromTex = new Tex();
        fromTex.width = w1;
        fromTex.height = h1;
        fromTex.fullWidth = w1;
        fromTex.fullHeight = h1;
        fromTex.offsetX = 0;
        fromTex.offsetY = 0;

        Tex toTex = new Tex();
        toTex.width = w3;
        toTex.height = h3;
        toTex.fullWidth = w3;
        toTex.fullHeight = h3;
        toTex.offsetX = 0;
        toTex.offsetY = 0;

        Tex resTex = new Tex();
        resTex.width = w2;
        resTex.height = h2;
        resTex.fullWidth = 2 * w2;
        resTex.fullHeight = h2;
        resTex.offsetX = toLeft ? w2 : 0;
        resTex.offsetY = 0;

        //Debug.Log((int)fromSprite.rect.x + " " + (int)fromSprite.rect.y + " " + (int)fromSprite.rect.width + " " + (int)fromSprite.rect.height);
        //Debug.Log(fromTex.offsetX + " " + fromTex.offsetY + " " + fromTex.width + " " + fromTex.height);
        //Debug.Log(fromPixels.Length);

        fromSR.enabled = (false);
        resultSR.enabled = (true);

        Vector2 pivot = new Vector2(0.75f, 0.5f);
        if (toLeft) pivot.x = 0.25f;
        Rect rect = new Rect(0, 0, 2 * w2, h2);

        float t = 0;//при t от 0 до 1 - ; при t от 1 до 2 - 
        while (t < 2)
        {
            Lerp3(fromPixels, resultPixels, toPixels, t, fromTex, resTex, toTex);//расчет анимации перетекания

            resultTexture.SetPixels(resultPixels);
            resultTexture.Apply();//устонавливаем расчитанные пиксели

            resultSprite = Sprite.Create(resultTexture, rect, pivot, ppu, 1, SpriteMeshType.Tight);
            resultSprite.name = "New Sprite";

            resultSR.sprite = resultSprite;

            yield return null;

            t += speed * Time.deltaTime;
        }

        resultSR.enabled = (false);
        toSR.enabled = (true);

        running = false;
    }

    struct Tex
    {
        public int width;
        public int height;
        public int fullWidth;
        public int fullHeight;
        public int offsetX;
        public int offsetY;
    }

    //pixels1 - пиксели исходной текстуры (спрайта)
    //pixels2 - пиксели текстуры для анимации
    //pixels3 - пиксели целевой текстуры


    private void Lerp3(Color[] pixels1, Color[] pixels2, Color[] pixels3, float t, Tex fromTex, Tex resTex, Tex toTex)
    {
        Color c = Color.white;
        for (int j = 0; j < resTex.height; j++)
        {
            //from и res имеют разное разрешение (ширину и высоту), поэтому делаем пересчет координат
            int y1 = (int)((float)j * fromTex.height / resTex.height);//y-координата точки в from
            int y2 = j;//y - координата точки в result
            int y3 = (int)((float)j * toTex.height / resTex.height);//y-координата точки в to

            float k = 1 - t;// Mathf.Clamp(1 - t, -1, 1);//k может принимать значения от 1 до -1
            float curv = curve.Evaluate((float)j / (resTex.height - 1));//приведение y координаты в res текстуре к диапазону (0, 1), и взяте точки на анимационной кривой
            if (toLeft) curv = Mathf.Abs(curv - 1);
            k = Mathf.Max(curv, k) - k;
            k = Mathf.Clamp(k, 0, 1);//расчет подтеков, на сколько подтек вылез за пределы текстуры

            int delta = (int)(resTex.width * k);//на сколько пикселей подтек вылез за пределы текстуры
            int w = resTex.width + delta;//(здесь resTex.width это только половина истинной ширины)
            int start = 0;//
            if (t > 1) start = (int)(resTex.width * (t - 1));//расчитываем где находится левая сторона подтека

            if (toLeft)
            {
                w = 2 * resTex.width;
                start = resTex.width - delta;
                if (t > 1) w = 2 * resTex.width - (int)(resTex.width * (t - 1));
            }
            
            //делаем расчет в resTex начиная от start заканчивая w
            for (int i = start; i < w; i++)
            {
                int x1 = (int)((float)(i - start) * fromTex.width / (w - start));//позиция в исходной текстуре
                int x2 = toLeft ? i : i;//растянутая позиция в result текстуре
                int x3 = (int)((float)(i - start) * toTex.width / (w - start));//позиция в target текстуре

                int ind1 = (fromTex.offsetY + y1) * fromTex.fullWidth + (fromTex.offsetX + x1);//позиция пикселя в массиве
                int ind2 = y2 * resTex.fullWidth + x2;
                int ind3 = (toTex.offsetY + y3) * toTex.fullWidth + (toTex.offsetX + x3);

                
                int x2_2 = (int)((float)(i - start) * resTex.width / (w - start));//нерастянутая позиция в result текстуре
                float distToTarget = 0;
                if (x2_2 > 0) distToTarget = ((float)i - resTex.width) / (x2_2);
                else if ((float)i - resTex.width >= 0) distToTarget = 1f;

                float m = 0;
                m = Mathf.Clamp(distToTarget, 0, 1);
                //if (t > 1.5f && t < 1.6f && j == 64) Debug.Log(i + " " + start + " " + w + " " + x2_2 + " " + distToTarget + " -> " + m);
                if (toLeft) m = 1 - m;

                //m = 0;
                //if (t > 1.5f) m = 2 * (t - 1.5f);

                c = pixels1[ind1];
                c.r *= (1 - m);
                c.g *= (1 - m);
                c.b *= (1 - m);
                c.a *= (1 - m);

                c.r += m * pixels3[ind3].r;
                c.g += m * pixels3[ind3].g;
                c.b += m * pixels3[ind3].b;
                c.a += m * pixels3[ind3].a;

                //pixels2[ind2] = ((1 - m) * pixels1[ind1] + m * pixels3[ind3]);

                try
                {
                    pixels2[ind2] = c;
                }
                catch
                {
                    Debug.Log(t + " " + k + " " + start + " " + w + " " + resTex.fullWidth);
                    Debug.Log(i + " " + j + " -> " + x2 + " " + y2 + " -> " + ind2 + " (" + pixels2.Length + ")");

                    //UnityEditor.EditorApplication.isPaused = true;
                }
            }

            curv = curve2.Evaluate((float)j / (resTex.height - 1));
            //if (toLeft) curv = Mathf.Abs(curv - 1);

            start = resTex.width + (int)(curv * resTex.width) + 1;
            int end = resTex.fullWidth;
            if (toLeft)
            {
                start = 0;
                end = (int)(curv * resTex.width) - 1;
            }

            for (int i = start; i < end; i++)
            {
                int x2 = toLeft ? i : i;//растянутая позиция в result текстуре
                int ind2 = y2 * resTex.fullWidth + x2;
                pixels2[ind2] = Color.clear;
            }
        }
    }
}
