﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpingText : MonoBehaviour {

    public Text lblText;

    public string text { get { return lblText.text; } set { lblText.text = value; } }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnFinish ()
    {
        Destroy(transform.parent.gameObject);
    }
}
