﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TransparencyPanel : MonoBehaviour {

	[Range (0, 1)]
	public float transparency = 0;
	public Gradient colorGradient;
	public bool useColorGradient;
	public float alpha {
		set {
			transparency = value;
			UpdateTransparency ();
		}
	}

	public List<Transform> ignoreList = new List<Transform> ();
	public MaskableGraphic[] savedGraphics;

	// private void OnValidate () {
	// 	UpdateTransparency (transform.GetComponentsInChildren<MaskableGraphic> ());
	// }

	private void Awake ()
    {
		UpdateTransparency ();
	}

	public void UpdateTransparency ()
    {
		if (savedGraphics == null)
			savedGraphics = transform.GetComponentsInChildren<MaskableGraphic> (true);

		for (int i = 0; i < savedGraphics.Length; i++)
        {
			if (savedGraphics[i] != null)
				if (!ignoreList.Contains (savedGraphics[i].transform)) {
					Color color = (!useColorGradient) ? savedGraphics[i].color : colorGradient.Evaluate (transparency);
					savedGraphics[i].color = new Color (color.r, color.g, color.b, transparency);
				}
		}
	}

	private void OnValidate ()
    {
		UpdateTransparency ();
	}

	public void SetAlpha (float val)
    {
		alpha = val;
	}

	public void SetAlwaysUpdate (bool mode)
    {
		alwaysUpdate = mode;
	}

	public bool alwaysUpdate = true;
	float lastTrancparency;
	private void Update ()
    {

		if (alwaysUpdate) {
			// if (Application.isPlaying) {
			if (transparency != lastTrancparency) {
				UpdateTransparency ();
				// }
				lastTrancparency = transparency;
			}
		}
	}
}