﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UIAnchored : MonoBehaviour {

    public RectTransform anchor;

    public Vector3 shift;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (anchor == null) return;
        //RectTransformUtility.ScreenPointToRay(anchor.posi)
        transform.position = anchor.position + shift;
	}
}
