﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ProgressBar : MonoBehaviour {

    public float minVal = 0f;

    public float maxVal = 1f;

	public Image fill;

    public float val;

	public void SetValue(float val) {

        this.val = val;

		val = Mathf.Clamp (val, 0, 1);
		fill.fillAmount = minVal + val * (maxVal-minVal);
	}

    private void Update()
    {
        SetValue(val);
    }
}
