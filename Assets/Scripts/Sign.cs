﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sign : MonoBehaviour {

    public Transform target;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void SetTarget(Transform t)
    {
        target = t;
        transform.SetParent(target);
        transform.localPosition = Vector3.zero;
    }
}
