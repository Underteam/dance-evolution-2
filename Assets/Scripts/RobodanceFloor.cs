﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobodanceFloor : MonoBehaviour {

    public List<SpriteRenderer> renderers;

    public List<Sprite> onStates;

    public List<Sprite> offStates;

    private int step;

    private float timer;

    private bool isOn;

    public float bpm = 120;

	// Use this for initialization
	void Start () {

        Off();
        On();
	}
	
	// Update is called once per frame
	void Update () {
		
        if(isOn)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                timer = 30f / bpm;

                if(step < renderers.Count)
                {
                    renderers[step].sprite = onStates[step];
                    step++;
                }
                else
                {
                    renderers[step - renderers.Count].sprite = offStates[step - renderers.Count];
                    step++;
                    if (step == 2 * renderers.Count) step = 0;
                }
            }
        }
	}

    public void Off ()
    {
        isOn = false;
        for (int i = 0; i < renderers.Count; i++) renderers[i].sprite = offStates[i];
    }

    public void On ()
    {
        isOn = true;
        timer = 30f / bpm;
        step = 0;
    }
}
