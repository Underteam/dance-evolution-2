﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraTransition : MonoSingleton<CameraTransition> {

    public List<DanceFloor> floors;

    private int currentFloor;
    //private int targetFloor;

    private Camera cam;

    private bool inTransition;

    public BGFitter fitter1;

    public BGFitter fitter2;

    public int position = 1;

    public float xPos;

    public List<GameObject> disablers;

    public List<MenuPanel> panels;

    public System.Action<int> onSwipeTo;

    public System.Action<int> onGoTo;

    public Transform blackCloud;

    private float blackCloudSize;

    // Use this for initialization
    void Start () {

        //disablers.Add(gameObject);

        cam = GetComponent<Camera>();

        blackCloudSize = 10;

        blackCloud.gameObject.SetActive(false);

        targetPosition = position;

        position = 2;
        targetPosition = 2;
        transitTo = 2;
        Vector3 pos = panels[position].transform.position;
        pos.z = cam.transform.position.z;
        cam.transform.position = pos;

        for (int i = 0; i < floors.Count; i++) floors[i].music.volume = 0f;
        floors[0].music.volume = 1f;
    }

    private bool drag;
    private Vector2 tapPos;
    [SerializeField]
    private int targetPosition;

    public int transitTo;
    private bool swiped = false;
    private bool bounced;
    private float targetXPos;

    private bool waiting;

    private float sens = 20;
    private float maxSens = 20;

	// Update is called once per frame
	void Update () {

        for(int i = 0; i < disablers.Count; i++)
        {
            if(disablers[i] == null)
            {
                disablers.RemoveAt(i);
                i--;
            }
        }

        if(disablers != null && disablers.Count > 0)
        {
            drag = false;
        }

        if(!waiting && Input.GetMouseButtonDown (0) && disablers.Count == 0 && !inTransition)
        {
            drag = true;
            tapPos = new Vector3(xPos * Screen.width, 0, 0) + Input.mousePosition;
            swiped = false;
            bounced = false;
            blackCloud.gameObject.SetActive(true);
        }
        else if (Input.GetMouseButtonUp (0) && drag)
        {
            targetXPos = 0;

            drag = false;
            
            if(swiped)
            {
                if (xPos < 0)
                {
                    targetXPos = -2 * blackCloudSize / sens;
                }
                else
                {
                    targetXPos = 2 * blackCloudSize / sens;
                }

                if (onSwipeTo != null) onSwipeTo(targetPosition);
            }

            waiting = true;
        }

        if (drag)
        {
            xPos = (tapPos.x - Input.mousePosition.x) / Screen.width;

            if (xPos < 0)
            {
                transitTo = position - 1;
                if (transitTo < 0) sens = maxSens * Mathf.Clamp(0.03f / (Mathf.Abs(xPos) + 0.03f), 0, 1);
                else sens = maxSens;

                if (sens * xPos < -1.5f)
                {
                    swiped = true;
                    targetPosition = transitTo;
                }
                else
                {
                    swiped = false;
                    targetPosition = position;
                }
            }
            else if (xPos > 0)
            {
                transitTo = position + 1;
                if (transitTo >= panels.Count) sens = maxSens * Mathf.Clamp(0.03f / (Mathf.Abs(xPos) + 0.03f), 0, 1);
                else sens = maxSens;

                if (sens * xPos > 1.5f)
                {
                    swiped = true;
                    targetPosition = transitTo;
                }
                else
                {
                    swiped = false;
                    targetPosition = position;
                }
            }
            else
            {
                //if (ind == position)
                //    blackCloud.gameObject.SetActive(false);
                //else
                //    blackCloud.gameObject.SetActive(true);
                blackCloud.gameObject.SetActive(false);
            }

            if (Mathf.Abs(sens * xPos) > blackCloudSize && !bounced)
            {
                Vector3 pos = panels[transitTo].transform.position;
                pos.z = cam.transform.position.z;
                cam.transform.position = pos;

                bounced = true;

                Debug.Log("Bounce");
            }
            else if(Mathf.Abs(sens * xPos) < blackCloudSize && bounced)
            {
                Vector3 pos = panels[position].transform.position;
                pos.z = cam.transform.position.z;
                cam.transform.position = pos;

                bounced = false;

                Debug.Log("Unbounce");
            }

            {
                Vector3 pos = cam.transform.position;
                pos.z = blackCloud.position.z;
                pos.x += blackCloudSize * Mathf.Sign(xPos) - sens * xPos;
                blackCloud.position = pos;
            }

        }
        else if (Mathf.Abs(xPos) > 0)
        {
            xPos = Mathf.Lerp(xPos, targetXPos, 10 * Time.deltaTime);

            if (Mathf.Abs(xPos - targetXPos) > 0.01f)
            {
                if (swiped)
                {
                    if (Mathf.Abs(sens * xPos) >= blackCloudSize && !bounced)
                    {
                        Vector3 campos = panels[targetPosition].transform.position;
                        campos.z = cam.transform.position.z;
                        cam.transform.position = campos;

                        bounced = true;
                    }
                }

                Vector3 pos = cam.transform.position;
                pos.z = blackCloud.position.z;
                pos.x += blackCloudSize * Mathf.Sign(xPos) - sens * xPos;
                blackCloud.position = pos;
            }
            else
            {
                position = targetPosition;
                xPos = 0;
                //Debug.Log("Finish transition to " + position);
            }
        }
        else
        {
            waiting = false;
            swiped = false;
            bounced = false;
        }
    }

    public void GoTo (int i)
    {
        if (i < 0 || i > panels.Count) return;

        if (position == i) return;

        //Debug.Log("Go to " + i);
        targetPosition = i;
        xPos = 0.01f;
        swiped = true;
        bounced = false;
        targetXPos = 0;
        if(i < position)
            targetXPos = -2 * blackCloudSize / sens;
        else
            targetXPos = 2 * blackCloudSize / sens;

        waiting = true;

        if (onGoTo != null) onGoTo(i);

        blackCloud.gameObject.SetActive(true);

        //position = i;
        //Vector3 campos = panels[position].transform.position;
        //campos.z = cam.transform.position.z;
        //cam.transform.position = campos;
    }

    private IEnumerator TransitingUp2 (DanceFloor from, DanceFloor to, System.Action onFinish)
    {
        var music1 = from.music;
        var music2 = to.music;

        //Debug.Log("Transit up " + from + " " + to, from);
        //UnityEditor.EditorApplication.isPaused = true;

        inTransition = true;//чтобы не запускать переход во время текущего

        Vector3 originalScale = to.bg.transform.localScale;

        List<SpriteRenderer> renderers = new List<SpriteRenderer>(to.bg.GetComponentsInChildren<SpriteRenderer>());
        List<int> sortings = new List<int>();
        for(int i = 0; i < renderers.Count; i++)
        {
            sortings.Add(renderers[i].sortingOrder);
            renderers[i].sortingOrder += 100;
        }

        Color c = Color.white;
        c.a = 0;
        for (int i = 0; i < renderers.Count; i++) renderers[i].color = c;

        to.bg.transform.position = from.bg.transform.position;

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;

            music1.volume = 1f - t;
            music2.volume = t;

            float s = (1 - t);
            if (s < 0.01f) s = 0.01f;
            from.dancersOrigin.localScale = s * Vector3.one;
            to.bg.transform.localScale = (1 + 2 * s) * originalScale;
            c.a = t;

            for (int i = 0; i < renderers.Count; i++) renderers[i].color = c;

            yield return null;
        }

        c.a = 1;

        from.dancersOrigin.localScale = Vector3.one;

        to.bg.transform.localScale = originalScale;

        to.bg.transform.localPosition = Vector3.zero;

        for (int i = 0; i < renderers.Count; i++)
        {
            renderers[i].sortingOrder = sortings[i];
            renderers[i].color = c;
        }

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private IEnumerator TransitingDown2(DanceFloor from, DanceFloor to, System.Action onFinish)
    {
        var music1 = from.music;
        var music2 = to.music;

        //Debug.Log("Transit down " + from + " " + to, from);
        //UnityEditor.EditorApplication.isPaused = true;

        inTransition = true;//чтобы не запускать переход во время текущего

        Vector3 originalScale = from.bg.transform.localScale;

        List<SpriteRenderer> renderers = new List<SpriteRenderer>(to.bg.GetComponentsInChildren<SpriteRenderer>());
        //renderers.AddRange(to.dancersOrigin.GetComponentsInChildren<SpriteRenderer>());
        List<int> sortings = new List<int>();
        for (int i = 0; i < renderers.Count; i++)
        {
            sortings.Add(renderers[i].sortingOrder);
            renderers[i].sortingOrder += 100;
        }

        Color c = Color.white;
        c.a = 0;
        for (int i = 0; i < renderers.Count; i++) renderers[i].color = c;

        to.bg.transform.position = from.bg.transform.position;
        //to.dancersOrigin.position = from.bg.transform.position;

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;

            music1.volume = 1f - t;
            music2.volume = t;

            float s = (1 - t);
            if (s < 0.01f) s = 0.01f;
            from.dancersOrigin.localScale = (1 + 2 * t) * Vector3.one;
            from.bg.transform.localScale = (1 + 2 * t) * originalScale;

            c.a = t;

            for (int i = 0; i < renderers.Count; i++) renderers[i].color = c;

            yield return null;
        }

        c.a = 1;

        from.dancersOrigin.localScale = Vector3.one;
        from.bg.transform.localScale = originalScale;

        //to.dancersOrigin.localPosition = Vector3.zero;

        to.bg.transform.localPosition = Vector3.zero;

        for (int i = 0; i < renderers.Count; i++)
        {
            renderers[i].sortingOrder = sortings[i];
            renderers[i].color = c;
        }

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private IEnumerator TransitingUp (SpriteRenderer fl1, SpriteRenderer fl2, System.Action onFinish)
    {
        //Debug.Log("Transit up " + fl1 + " " + fl2, fl1);
        //UnityEditor.EditorApplication.isPaused = true;

        inTransition = true;//чтобы не запускать переход во время текущего

        Color color = fl1.color;
        color.a = 0;
        fl1.color = color;//просто скрываем оригинал пока

        fitter1.gameObject.SetActive(true);

        fitter2.gameObject.SetActive(true);

        fitter1.back.sprite = fl1.sprite;
        fitter1.back.sortingOrder = fl1.sortingOrder;

        fitter2.back.sprite = fl2.sprite;

        color = fitter1.back.color;
        color.a = 1;
        fitter1.back.color = color;

        color = fitter2.back.color;
        color.a = 0;
        fitter2.back.color = color;

        fitter2.back.sortingOrder = fitter1.back.sortingOrder + 100;

        fitter1.partOfScreen = new Vector2(1f, 1f);

        fitter1.partOfScreen = new Vector2(3f, 3f);

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;
            cam.orthographicSize = 5 + 20 * t;
            color = fitter2.back.color;
            color.a = t;
            fitter2.back.color = color;
            fitter2.partOfScreen = new Vector2(3f - 2*t, 3f - 2*t);
            yield return null;
        }

        fitter1.gameObject.SetActive(false);

        fitter2.gameObject.SetActive(false);

        color = fl1.color;
        color.a = 1;
        fl1.color = color;

        cam.orthographicSize = 5;

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private IEnumerator TransitingDown (SpriteRenderer fl1, SpriteRenderer fl2, System.Action onFinish)
    {
        inTransition = true;

        Color color = fl1.color;
        color.a = 0;
        fl1.color = color;

        fitter1.gameObject.SetActive(true);

        fitter2.gameObject.SetActive(true);

        fitter1.back.sprite = fl1.sprite;
        fitter1.back.sortingOrder = fl1.sortingOrder;

        fitter2.back.sprite = fl2.sprite;

        color = fitter1.back.color;
        color.a = 1;
        fitter1.back.color = color;

        color = fitter2.back.color;
        color.a = 0;
        fitter2.back.color = color;

        fitter2.back.sortingOrder = fitter1.back.sortingOrder + 100;

        fitter1.partOfScreen = new Vector2(1f, 1f);

        fitter1.partOfScreen = new Vector2(1f, 1f);

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Lerp(t, 1.05f, 5f * Time.deltaTime);
            if (t > 1) t = 1;
            cam.orthographicSize = 5 - 4 * t;
            color = fitter2.back.color;
            color.a = t;
            fitter2.back.color = color;
            fitter1.partOfScreen = new Vector2(1f + 2 * t, 1f + 2 * t);
            yield return null;
        }

        fitter1.gameObject.SetActive(false);

        fitter2.gameObject.SetActive(false);

        color = fl1.color;
        color.a = 1;
        fl1.color = color;

        cam.orthographicSize = 5;

        if (onFinish != null) onFinish();

        inTransition = false;
    }

    private void SetCamPosition (Vector3 pos)
    {
        pos.z = cam.transform.position.z;
        cam.transform.position = pos;
    }

    //[EditorButton]
    //public void Transit2()
    //{
    //    StartCoroutine(TransitingUp(floors[0].bg, floors[1].bg, () => { SetCamPosition(floors[1].transform.position);}));
    //}

    //[EditorButton]
    //public void Transit3()
    //{
    //    StartCoroutine(TransitingDown(floors[1].bg, floors[0].bg, () => { SetCamPosition(floors[0].transform.position); }));
    //}

    public void Transit(int to)
    {
        if (waiting) return;

        if (inTransition) return;

        if (currentFloor == to) return;

        if(to > currentFloor)
        {
            //StartCoroutine(TransitingUp(floors[currentFloor].bg, floors[to].bg, () => { SetCamPosition(floors[to].transform.position); }));
            StartCoroutine(TransitingUp2(floors[currentFloor], floors[to], () => { SetCamPosition(floors[to].transform.position); }));
        }
        else
        {
            //StartCoroutine(TransitingDown(floors[currentFloor].bg, floors[to].bg, () => { SetCamPosition(floors[to].transform.position); }));
            StartCoroutine(TransitingDown2(floors[currentFloor], floors[to], () => { SetCamPosition(floors[to].transform.position); }));
        }

        currentFloor = to;
    }

    //[EditorButton]
    //public void Transit ()
    //{
    //    if (inTransition) return;

    //    targetFloor = (currentFloor + 1) % floors.Count;

    //    StartCoroutine(Transition());
    //}

    //private IEnumerator Transition ()
    //{
    //    inTransition = true;

    //    yield return null;

    //    Vector3 targetPos = floors[targetFloor].transform.position;
    //    targetPos.z = transform.position.z;

    //    float size = cam.orthographicSize;

    //    float s = size;

    //    while(s > 0.01f)
    //    {
    //        s = Mathf.Lerp(s, 0f, 15 * Time.deltaTime);
    //        if (s < 0.01f) s = 0.01f;
    //        cam.orthographicSize = s;
    //        yield return null;
    //    }

    //    cam.transform.position = targetPos;

    //    while(s < size-0.1f)
    //    {
    //        s = Mathf.Lerp(s, size, 15 * Time.deltaTime);
    //        if (s > size) s = size;
    //        cam.orthographicSize = s;
    //        yield return null;
    //    }

    //    cam.orthographicSize = size;

    //    currentFloor = targetFloor;

    //    inTransition = false;
    //}
}
