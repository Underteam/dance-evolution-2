﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LootBox : MonoBehaviour, IPointerDownHandler {

    public GameObject light1;
    public GameObject light2;
    public GameObject light3;
    public GameObject blueLight;

    public GameObject closedBox;
    public GameObject openedBox;

    public GameObject coins;
    public GameObject crystals;
    public GameObject card;

    public Text lblReward;

    public Transform coinsPlace;
    public Transform crystalsPlace;
    public Transform cardPlace;

    private Numerics.BigInteger reward;
    private enum RewardType
    {
        Coins,
        Crystals,
        Card,
    }

    private RewardType rewardType;

    // Use this for initialization
    void Start () {

        //Show();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show (float cardChance)
    {
        Debug.LogError("Card chance " + cardChance);

        light1.SetActive(true);
        light2.SetActive(true);
        light3.SetActive(false);
        blueLight.SetActive(false);

        closedBox.SetActive(true);
        openedBox.SetActive(false);

        gameObject.SetActive(true);

        float coinsChance = 2f * (1 - cardChance) / 3f;
        //float crystalsChance = 1f * (1 - cardChance) / 3f;
        float rnd = Random.Range(0f, 1f);
        if (rnd < cardChance)
        {
            rewardType = RewardType.Card;
            reward = 1;

            coins.SetActive(false);
            crystals.SetActive(false);
            card.SetActive(true);
            lblReward.gameObject.SetActive(false);
        }
        else if (rnd < cardChance + coinsChance)
        {
            rewardType = RewardType.Coins;
            reward = GameController.Instance().earningsRate.Mult(60);
            Debug.Log("Reward: " + reward + " " + GameController.Instance().earningsRate);

            coins.SetActive(true);
            crystals.SetActive(false);
            card.SetActive(false);
            lblReward.gameObject.SetActive(true);
        }
        else
        {
            rewardType = RewardType.Crystals;
            reward = 2;

            coins.SetActive(false);
            crystals.SetActive(true);
            card.SetActive(false);
            lblReward.gameObject.SetActive(true);
        }

        lblReward.text = "+" + reward.Short();
    }

    public void StartHiding ()
    {
        GameObject clone = null;
        Transform clonePlace = null;

        if (rewardType == RewardType.Coins)
        {
            clone = GetClone(coins);
            clonePlace = coinsPlace;
            coins.SetActive(false);
        }
        else if (rewardType == RewardType.Crystals)
        {
            clone = GetClone(crystals);
            clonePlace = crystalsPlace;
            crystals.SetActive(false);
        }
        else if (rewardType == RewardType.Card)
        {
            clone = GetClone(card);
            clonePlace = cardPlace;
            card.SetActive(false);
        }

        lblReward.gameObject.SetActive(false);

        clone.SetActive(true);
        var mover = clone.AddComponent<GoToPlace>();
        mover.targetPlace = clonePlace;
    }

    private GameObject GetClone (GameObject go)
    {
        var clone = Instantiate(go);
        clone.transform.SetParent(go.transform.parent);
        clone.transform.localScale = go.transform.localScale;
        clone.transform.SetParent(transform.parent);
        clone.transform.position = go.transform.position;

        return clone;
    }

    public void Hided ()
    {
        gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GetComponent<Animator>().SetTrigger("Open");

        GameController.Instance().OpenChest();

        if(rewardType == RewardType.Coins)
        {
            Wallet.GetWallet(Wallet.CurrencyType.coins).Add(reward);
        }
        else if(rewardType == RewardType.Crystals)
        {
            Wallet.GetWallet(Wallet.CurrencyType.crystals).Add(reward);
        }
        else if(rewardType == RewardType.Card)
        {
            GameController.Instance().GenerateCard();
        }
    }
}
