﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GameData/GameData", order = 1)]
public class SaveableData : ScriptableObject {

    public uint stamp;
    public uint confirm;

    private class Helper<T>
    {
        public T field;
    }

    public void Clear ()
    {
        stamp = 0;
        confirm = 0;

        valuesNames.Clear();
        values.Clear();

        arraysNames.Clear();
        arrays.Clear();
    }

    public List<string> valuesNames;
    public List<string> values;

    public List<string> arraysNames;
    public List<DataBatch> arrays;

    public bool GetValue<T> (string valueName, ref T v)
    {
        int ind = valuesNames.IndexOf(valueName);
        if(ind < 0) return false;

        //Debug.Log("Get " + valueName + " " + v + " " + typeof(T).IsClass);

        if (!typeof(T).IsClass || typeof(T) == typeof(string))
        {
            Helper<T> helper = JsonUtility.FromJson<Helper<T>>(values[ind]);
            v = helper.field;
            //Debug.Log("Get " + valueName + " " + values[ind] + " -> " + v);
        }
        else
            v = JsonUtility.FromJson<T>(values[ind]);

        return true;
    }

    public void SetValue<T>(string valueName, T v)
    {
        int ind = valuesNames.IndexOf(valueName);
        if (ind < 0)
        {
            ind = valuesNames.Count;
            valuesNames.Add(valueName);
            values.Add("");
        }

        //Debug.Log("Set " + valueName + " " + v + " " + typeof(T).IsClass + " " + JsonUtility.ToJson(v));

        if(!typeof(T).IsClass || typeof(T) == typeof(string))
        {
            Helper<T> helper = new Helper<T>();
            helper.field = v;
            values[ind] = JsonUtility.ToJson(helper);
            //Debug.Log("Set " + valueName + " " + v + " -> " + values[ind]);
        }
        else
            values[ind] = JsonUtility.ToJson(v);
    }

    public bool GetValue<T>(string arrayName, int index, ref T v)
    {
        int ind = arraysNames.IndexOf(arrayName);
        if (ind < 0) return false;

        DataBatch array = arrays[ind];

        return array.GetField<T>("element" + index, ref v);
    }

    public void SetValue<T>(string arrayName, int index, T v)
    {
        int ind = arraysNames.IndexOf(arrayName);
        if (ind < 0)
        {
            ind = arraysNames.Count;
            arraysNames.Add(arrayName);
            var batch = new DataBatch();
            arrays.Add(batch);
        }

        DataBatch array = arrays[ind];

        if (index < 0 || index >= array.GetFieldsCount())
        {
            Debug.Log(arrayName + " " + index + " -> " + v + " " + array.GetFieldsCount());
            throw new System.IndexOutOfRangeException();
        }

        array.SetField<T>("element" + index, v);
    }

    public void AddValue<T>(string arrayName, T v)
    {
        int ind = arraysNames.IndexOf(arrayName);
        if (ind < 0)
        {
            ind = arraysNames.Count;
            arraysNames.Add(arrayName);
            var batch = new DataBatch();
            arrays.Add(batch);
        }

        DataBatch array = arrays[ind];

        int index = array.GetFieldsCount();

        array.SetField<T>("element" + index, v);

        //Debug.Log("Add value " + arrayName + " " + v + " | " + array.GetFieldsCount());
    }

    public void RemoveValue<T>(string arrayName, int i)
    {
        int ind = arraysNames.IndexOf(arrayName);
        if (ind < 0)
        {
            ind = arraysNames.Count;
            arraysNames.Add(arrayName);
            var batch = new DataBatch();
            arrays.Add(batch);
        }

        DataBatch array = arrays[ind];

        int index = array.GetFieldsCount();

        array.RemoveField ("element" + i);

        //Debug.Log("Add value " + arrayName + " " + v + " | " + array.GetFieldsCount());

    }
    public int GetArrayCount(string arrayName)
    {
        int ind = arraysNames.IndexOf(arrayName);
        if (ind < 0)
        {
            ind = arraysNames.Count;
            arraysNames.Add(arrayName);
            var batch = new DataBatch();
            arrays.Add(batch);
        }

        return arrays[ind].GetFieldsCount();
    }
}

//Выступает в роли класса с произвольными полями
[System.Serializable]
public class DataBatch
{
    public List<string> keys = new List<string>();
    public List<string> values = new List<string>();

    public int GetFieldsCount ()
    {
        return keys.Count;
    }

    public bool GetField<T>(string key, ref T v)
    {
        int ind = keys.IndexOf(key);
        if (ind < 0) return false;

        if (!typeof(T).IsClass || typeof(T) == typeof(string))
        {
            Helper<T> helper = JsonUtility.FromJson<Helper<T>>(values[ind]);
            v = helper.field;
        }
        else
            v = JsonUtility.FromJson<T>(values[ind]);

        return true;
    }

    public T GetField<T>(int ind)
    {
        int count = keys.Count;

        if (ind < 0 || ind >= count) throw new System.IndexOutOfRangeException();

        if (!typeof(T).IsClass || typeof(T) == typeof(string))
        {
            Helper<T> helper = JsonUtility.FromJson<Helper<T>>(values[ind]);
            return helper.field;
        }
        else
            return JsonUtility.FromJson<T>(values[ind]);
    }

    public void SetField<T>(string key, T v)
    {
        int ind = keys.IndexOf(key);
        if (ind < 0)
        {
            ind = keys.Count;
            keys.Add(key);
            values.Add("");
        }
        
        if (!typeof(T).IsClass || typeof(T) == typeof(string))
        {
            Helper<T> helper = new Helper<T>();
            helper.field = v;
            values[ind] = JsonUtility.ToJson(helper);
        }
        else
            values[ind] = JsonUtility.ToJson(v);
    }

    public void RemoveField (string key)
    {
        int ind = keys.IndexOf(key);
        if (ind < 0)
        {
            return;
        }

        keys.RemoveAt(ind);
        values.RemoveAt(ind);
    }

    private class Helper<T>
    {
        public T field;
    }
}
