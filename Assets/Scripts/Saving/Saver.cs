﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public interface ISaveable
{
    void PrepareForSaving(SaveableData data);

    void Save(SaveableData data);

    void Load(SaveableData data);

    //void Clear(SaveableData data);
}

public class Saver : MonoSingleton<Saver> {

    //public List<DanceFloor> floors;

    public SaveableData data1;

    public SaveableData data2;

    private bool canBeSaved = false;

    public SaveableData current;//TODO

    public List<ISaveableType> _saveables;

    private List<ISaveable> saveables = new List<ISaveable>();

    protected override void Init ()
    {
        //Debug.Log("Init saver");

        saveables.Clear();
        for(int i = 0; i < _saveables.Count; i++)
        {
            saveables.AddRange(_saveables[i].GetInterfaces());
        }

        LoadData();

        current = WhatToLoad();

        inited = true;
    }

    public void Add(ISaveable saveable)
    {
        if (!saveables.Contains(saveable)) saveables.Add(saveable);
    }

    private float timer;
    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > 0.2f)
        {
            timer = 0;
            SaveData();
        }
    }

    private void SaveData ()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 0));
        string json = JsonUtility.ToJson(data1);
        bf.Serialize(file, json);
        file.Close();

        bf = new BinaryFormatter();
        file = File.Create(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 1));
        json = JsonUtility.ToJson(data2);
        bf.Serialize(file, json);
        file.Close();
    }

    private void LoadData ()
    {
        bool clear = false;

        if (File.Exists(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 0)))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 0), FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), data1);
            file.Close();
        }
        else
        {
            Debug.Log("File did not exists");
            clear = true;
        }

        if (File.Exists(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 1)))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + string.Format("/{0}_{1}.asset", "SaveableData", 1), FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), data2);
            file.Close();
        }
        else
        {
            Debug.Log("File did not exists");
            clear = true;
        }

        if(clear)
        {
            data1.Clear();
            data2.Clear();
            SaveData();
        }
    }

    private void OnDisable()
    {
        SaveData();
    }

    [EditorButton]
	public void Save ()
    {
        if (!canBeSaved) return;

        SaveableData data = WhereToSave();
        current = data;

        uint stamp = data1.stamp + 1 > data2.stamp + 1 ? data1.stamp + 1 : data2.stamp + 1;
        
        for (int i = 0; i < saveables.Count; i++) saveables[i].PrepareForSaving (data);

        data.Clear();

        data.stamp = stamp;

        for (int i = 0; i < saveables.Count; i++) saveables[i].Save (data);

        data.confirm = stamp;

        SaveData();
    }

    [EditorButton]
    public void Load()
    {
        LoadData();

        SaveableData data = WhatToLoad();
        current = data;

        for (int i = 0; i < saveables.Count; i++) saveables[i].Load (data);

        canBeSaved = true;
    }

    [EditorButton]
    public void Clear()
    {
        data1.Clear();
        data2.Clear();

        SaveData();
    }

    private SaveableData WhereToSave ()
    {

        if(data1.stamp != data1.confirm && data2.stamp == data2.confirm)
        {
            return data1;
        }

        if (data1.stamp == data1.confirm && data2.stamp != data2.confirm)
        {
            return data2;
        }

        SaveableData toSave = data1.stamp + 1 <= data2.stamp ? data1 : data2;

        return toSave;
    }

    private SaveableData WhatToLoad()
    {

        if (data1.stamp != data1.confirm && data2.stamp == data2.confirm)
        {
            return data2;
        }

        if (data1.stamp == data1.confirm && data2.stamp != data2.confirm)
        {
            return data1;
        }

        SaveableData toLoad = data1.stamp + 1 <= data2.stamp ? data2 : data1;

        return toLoad;
    }

    public void SetValue<T>(string valueName, T value)
    {
        current.SetValue<T>(valueName, value);
    }

    public void SetValue<T>(string arrayName, int index, T value)
    {
        current.SetValue<T>(arrayName, index, value);
    }

    public bool GetValue<T>(string valueName, ref T value)
    {
        return current.GetValue<T>(valueName, ref value);
    }

    public bool GetValue<T>(string arrayName, int index, ref T value)
    {
        return current.GetValue<T>(arrayName, index, ref value);
    }
}

