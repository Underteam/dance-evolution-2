﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disappear : MonoBehaviour {

    private float time = 1f;

    private float timer;

    private SpriteRenderer sr;

	// Use this for initialization
	void Start () {

        sr = GetComponent<SpriteRenderer>();

        timer = time;
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            Destroy(gameObject);
        }
        else if(sr != null)
        {
            float a = timer / time;
            Color c = sr.color;
            c.a = a;
            sr.color = c;
        }
	}
}
