﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    public Dancer dancer;

    public Animator anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Stand ()
    {
        anim.Play("Stand");
    }

    public void OnMouseDown()
    {
        //if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != null) return;

        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;

        dancer.transform.position = transform.position;
        dancer.gameObject.SetActive(true);
        GameController.Instance().DancerSpawned();
        Destroy(gameObject);

        NewDancerMenu.Instance().Check(dancer.type);

        if (!CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Add(gameObject);
    }

    public void OnMouseUp()
    {
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    public void OnMouseExit()
    {
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    private void OnDestroy ()
    {
        if (quit) return;
        if (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
    }

    private bool quit;
    private void OnApplicationQuit()
    {
        quit = true;
    }
}
