﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Numerics;

public class Dancer : MonoBehaviour {

    [System.Serializable]
    public class ExponentialValue
    {
        public string baseValue = "1";
        public float rate = 1;

        public BigInteger GetValue(int l)
        {
            BigInteger a = new BigInteger(baseValue);
            BigInteger b = BigInteger.FromExp(rate * l + Mathf.Log(10000));
            BigInteger c = new BigInteger(10000);
            //Debug.Log(a + " " + b + " " + (a * b / c) + " " + (a * b / c).Short());
            return a * b / c;
        }
    }

    public DanceFloor danceFloor;

    public Sprite dancerFace;
    public Sprite dancerPortrait;

    public string dancerName;

    public bool merging;

    public int type;

    public string _earnings;
    private BigInteger earnings;

    public ExponentialValue buyingCost;

    [HideInInspector]
    public int indexInSave;

    private float earningTimer = 1f;

    public BigInteger GetEarnings ()
    {
        if (!gameObject.activeSelf) return 0;
        if (earnings == null) earnings = new BigInteger(_earnings);
        return earnings;
    }

    private Animator anim;
    public float tempo;
    public string info;

    public bool buyed { get; set; }

    private float movingTimer;
    private Vector3 targetPosition;

    private bool canMove = true;
    public bool block;

    [HideInInspector]
    public Elevator elevator;

    // Use this for initialization
    void Start () {

        transform.localScale = 0.75f * Vector3.one;

        earningTimer = Random.Range(1f, 5f);

        anim = GetComponent<Animator>();

        if (earnings == null) earnings = new BigInteger(_earnings);

        Merger.Instance().RegisterDancer(this);

        DragNDrop dnd = GetComponent<DragNDrop>();
        if(dnd != null)
        {
            //dnd.dragStartsOnTap = true;
            dnd.onTap.AddListener(new UnityEngine.Events.UnityAction(() => 
            {
                if(!CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Add(gameObject);
            }));

            dnd.onDrop.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                while (CameraTransition.Instance().disablers.Contains(gameObject)) CameraTransition.Instance().disablers.Remove(gameObject);
            }));

            dnd.onBeginDrag.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                canMove = false;
            }));

            dnd.onDrop.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                canMove = true;
            }));
        }

        movingTimer = Random.Range(10f, 20f);
        GetTargetPosition();
    }
	
    public float targettempo = 144;

	// Update is called once per frame
	void Update () {
		
        if(canMove && !merging && !block)
        {
            if(movingTimer >= 0)
            {
                movingTimer -= Time.deltaTime;
            }
            else
            {
                Vector3 dir = targetPosition - transform.localPosition;
                dir.z = 0;
                float d = dir.magnitude;
                if (d < 0.01f)
                {
                    movingTimer = Random.Range(10f, 20f);
                    GetTargetPosition();
                }
                else
                {
                    float speed = 1f;
                    float delta = speed * Time.deltaTime;
                    if (delta > d) transform.localPosition = targetPosition;
                    else transform.localPosition += delta * dir.normalized;
                }
            }
        }
        if(anim != null)
        {
            var clipinfo = anim.GetCurrentAnimatorClipInfo(0);

            var stateinfo = anim.GetCurrentAnimatorStateInfo(0);

            info = "";
            info += clipinfo[0].clip.apparentSpeed + " | " + clipinfo[0].clip.averageDuration + " | " + clipinfo[0].clip.averageSpeed + " | " + clipinfo[0].clip.frameRate + " | " + clipinfo[0].clip.length;

            tempo = clipinfo[0].clip.length;
            tempo = 60 / tempo;
            while (tempo < 100) tempo *= 2;
            while (tempo > 200) tempo /= 2;

            float s = (targettempo / tempo);
            info += " | " + s;

            anim.speed = s;
        }

        earningTimer -= Time.deltaTime;
        if(earningTimer <= 0)
        {
            earningTimer = 5f;
            GameController.Instance().DancerEarned(this, 5f);
            StartCoroutine(Scale());
        }
	}

    private void GetTargetPosition ()
    {
        Vector3 shift = new Vector3(Random.Range(-1.5f, 1.5f), Random.Range(-1.5f, 1.5f), 0);
        targetPosition = shift;
        targetPosition.z = transform.localPosition.z;
        //shift = targetPosition - transform.position;
        //float d = shift.magnitude;
        //targetPosition = transform.position + shift.normalized * Mathf.Min(d, Random.Range(0.2f, 1f));
    }

    private bool scaling;
    public IEnumerator Scale ()
    {
        if (scaling) yield break;

        scaling = true;

        Vector3 scale = transform.localScale;
        float ys = scale.y;

        float t = 0f;

        while(t < 1f)
        {
            t += 20 * Time.deltaTime;
            scale.y = ys * (1 - 0.1f * t);
            transform.localScale = scale;
            yield return null;
        }

        while (t > 0f)
        {
            t -= 20 * Time.deltaTime;
            scale.y = ys * (1 - 0.1f * t);
            transform.localScale = scale;
            yield return null;
        }

        scale.y = ys;

        transform.localScale = scale;

        scaling = false;
    }

    public void Droped ()
    {
        float height = Camera.main.orthographicSize;
        float width = height * Screen.width / Screen.height;

        height -= 1;
        width -= 0.5f;
        height = width;

        Vector3 pos = transform.position - danceFloor.dancersOrigin.position;
        pos.x = Mathf.Clamp(pos.x, -width, width);
        pos.y = Mathf.Clamp(pos.y, -height, height);

        transform.position = danceFloor.dancersOrigin.position + pos;

        movingTimer = Random.Range(10f, 20f);

        Merger.Instance().DancerDroped(this);
    }

    public void Clicked ()
    {
        movingTimer = Random.Range(10f, 20f);
        StartCoroutine(Scale());
        GameController.Instance().DancerClicked(this);
    }

    [EditorButton]
    public void Test ()
    {
        Debug.Log(earnings.ToString());
        Debug.Log(earnings.Short());

        earnings *= 1000;
    }
}
