﻿using UnityEngine;
using System.Collections;

public class AnimOff : MonoBehaviour {
    //Animator anim;
	// Use this for initialization

    Animator anim;
	void Start () {
        anim = GetComponent<Animator>(); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Off()
    {
        anim.enabled = false;
		//gameObject.SetActive (false);
		Destroy (gameObject);
    }
}
