﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Numerics;

public class GameController : MonoSingleton<GameController>, ISaveable {

    public CameraTransition cam;

    public Wallet coinsBalance;
    public Wallet crystalsBalance;

    public string _balance;

    [HideInInspector]public BigInteger earningsRate = new BigInteger();

    public string _earnings;

    public List<DanceFloor> floors;

    private float unpaid;

    public Text lblEarnings;

    public Text lblBalance;
    public Text lblCrystals;

    public int numMerges;
    private int _mergesNeeded = 10;
    private float _mergesBonus = 0;
    public ProgressBar merges;

    public int mergesNeeded
    {
        get { return _mergesNeeded - (int)(_mergesNeeded * _mergesBonus + 0.5f); }
        private set { _mergesNeeded = value; if (_mergesNeeded < 10) _mergesNeeded = 10; }
    }
    public float mergesBonus {
        get { return _mergesBonus; }
        set { _mergesBonus = Mathf.Clamp(value, 0f, 1f); }
    }

    [HideInInspector]
    public float cardChance = 0;

    public GameObject offlineEarningsMenu;
    public GameObject btnTake;
    public GameObject btnX2;
    public FlyingCoin flyingCoin;
    public Text lblOfflineEarnings;
    public Text lblAbsent;
    [HideInInspector]
    public float clubWorkingTime;

    public RobodancerMenu robodancer;

    public LootBox lootBox;

    private bool started;

    public Canvas canvas;
    public GameObject jumpingText;

    public bool developerMode;

	// Use this for initialization
	void Start ()
    {
        //for (int i = 0; i < robodances.Count; i++) robodances[i].danceID = i;
        //BigInteger ten = new BigInteger(10);
        //for (int i = 0; i < 10; i++)
        //{
        //    float rnd = Random.Range(0f, 2f);
        //    Debug.Log(rnd + " -> " + ten.Mult(rnd));
        //}

        TinySauce.OnGameStarted();

        Saver.Instance().Load ();

        merges.SetValue((float)numMerges / _mergesNeeded);
        if (numMerges >= _mergesNeeded) ActivateChest();

        coinsBalance = Wallet.GetWallet(Wallet.CurrencyType.coins);
        coinsBalance.onValueChanged += (w) =>
        {
            lblBalance.text = w.amount.Short();
        };
        if (developerMode) coinsBalance.developerMode = true;
        if (developerMode) coinsBalance.Add(1000);

        _balance = coinsBalance.amount.Short();

        RecalculateEarnings();

        lblBalance.text = coinsBalance.amount.Short();

        crystalsBalance = Wallet.GetWallet(Wallet.CurrencyType.crystals);
        crystalsBalance.onValueChanged += (w) =>
        {
            lblCrystals.text = w.amount.Short();
        };
        if (developerMode) crystalsBalance.developerMode = true;
        if (developerMode) crystalsBalance.Add(20);

        lblCrystals.text = crystalsBalance.amount.Short();

        CheckOfflineEarnings();

        started = true;

        Tutorial.Instance().StartTutorial();
    }
	
    public void NewGame ()
    {
        RemoveAllDancers();

        //coinsBalance.Spend(coinsBalance.amount);
        //coinsBalance.Add(1000);

        //crystalsBalance.Spend(crystalsBalance.amount);
        //crystalsBalance.Add(5);
    }

    //[ContextMenu("ClearPrefs")]
    [EditorButton]
    public void ClearPrefs ()
    {
        PlayerPrefs.DeleteAll();
        Saver.Instance().Clear();
        PlayerPrefs.Save();
    }

    private void RecalculateEarnings ()
    {
        earningsRate = 0;

        for (int i = 0; i < floors.Count; i++)
        {
            earningsRate += floors[i].GetEarningsRate();
        }

        if (doubleEarning) earningsRate *= 2;

        lblEarnings.text = earningsRate.Short() + "/s";

        _earnings = earningsRate.ToString();

        //Debug.Log("RecalculateEarnings " + _earnings);
    }

	// Update is called once per frame
	void Update () {

        //BigInteger add = earningsRate * ((int)(10000f * Time.deltaTime));
        //add /= 10000;

        //if (add > 1000)
        //{
        //    coinsBalance.Add(add);
        //}
        //else
        //{
        //    float fadd = earningsRate;
        //    fadd *= Time.deltaTime;
        //    unpaid += fadd;
        //    coinsBalance.Add((int)unpaid);
        //    unpaid -= (int)unpaid;
        //}

        //OfflineTimeTracker.Instance().NewStamp();

        Spawning();

        if(doubleEarning)
        {
            for (int i = 0; i < knobs.Count; i++) knobs[i].sprite = sKnobOff;

            if (doubleEarningsEnd <= System.DateTime.Now)
            {
                Debug.Log("Off DE");
                doubleEarning = false;
                imgDoubleEarnings.sprite = sDOOff;
                RecalculateEarnings();
            }
            else
            {
                var s = doubleEarningsEnd - System.DateTime.Now;
                int m = (int)s.TotalMinutes;
                m /= 30;

                for(int i = 0; i <= m && i < knobs.Count; i++) knobs[i].sprite = sKnobOn;

                for (int i = 0; i < deTimers.Count; i++) deTimers[i].text = TimeToString(s);
            }
        }
        else
        {
            for (int i = 0; i < deTimers.Count; i++) deTimers[i].text = "00:00";
        }

        //Saver.Instance().UpdateBalance(balance);
        //Saver.Instance().SetValue<string>("GameController.balance", balance.ToString());
    }

    public void DancerSpawned ()
    {
        RecalculateEarnings();

        Saver.Instance().Save();
    }

    public void DancersMerged (Dancer newDancer)
    {
        RecalculateEarnings();

        if (numMerges < _mergesNeeded)
        {
            numMerges++;
            if (numMerges >= _mergesNeeded) ActivateChest();
            merges.SetValue((float)numMerges / _mergesNeeded);
        }

        //int ind = 0;
        //for(int i = 0; i < floors.Count; i++)
        //{
        //    if (newDancer.danceFloor == floors[i]) break;
        //    ind += floors[i].prefabs.Count;
        //}
        //ind += newDancer.type;
        //DancersShop.Instance().NewDancer(ind);

        if (DancersShop.Instance().NewDancer(newDancer.type))
        {
            lastDiscoveredDancer = newDancer.type;
            Invoke("ShowNewDancer", 0.5f);
        }

        Saver.Instance().Save();
    }

    private int lastDiscoveredDancer;
    public void ShowNewDancer()
    {
        NewDancerMenu.Instance().Show(lastDiscoveredDancer);
    }

    public void ActivateChest ()
    {
        merges.GetComponent<Animator>().SetBool("Ready", true);

        lootBox.Show(cardChance);
    }

    public void OpenChest ()
    {
        Debug.Log("OpenChest " + numMerges + " " + _mergesNeeded);

        if (numMerges < _mergesNeeded) return;

        numMerges = 0;
        merges.SetValue((float)numMerges / _mergesNeeded);
        merges.GetComponent<Animator>().SetBool("Ready", false);

        //GenerateCard();

        Saver.Instance().Save();
    }

    //TODO
    public void DancerDroped (Dancer dancer)
    {
        dancer.danceFloor.UpdateDancer(dancer);
    }

    public System.Action<Dancer> onDancerClicked;
    public void DancerClicked (Dancer dancer)
    {
        //Debug.Log("Clicked " + dancer.GetEarnings(), dancer);

        //coinsBalance.Add(dancer.GetEarnings());

        DancerEarned(dancer, 1);

        if (onDancerClicked != null) onDancerClicked(dancer);

        //Saver.Instance().SetValue<string>("GameController.balance", balance.ToString());
    }

    public void DancerEarned(Dancer dancer, float time)
    {
        var go = Instantiate(jumpingText);
        go.SetActive(true);
        var jt = go.GetComponentInChildren<JumpingText>();
        BigInteger e = dancer.GetEarnings();
        e = e.Mult(time * (doubleEarning ? 2 : 1));
        jt.text = "+" + e.Short();
        go.transform.SetParent(jumpingText.transform.parent);
        go.transform.localScale = Vector3.one;
        go.transform.position = WorldToUISpace(canvas, dancer.transform.position + 1.5f * Vector3.up);
        go.transform.SetSiblingIndex(jumpingText.transform.GetSiblingIndex() + 1);

        coinsBalance.Add(e);
    }

    public void PrepareForSaving (SaveableData data)
    {

    }

    public void Save (SaveableData data)
    {
        //data.SetValue<string>("GameController.balance", balance.ToString());

        data.SetValue<int>("GameController.numMerges", numMerges);

        data.SetValue<bool>("GameController.doubleEarnings", doubleEarning);

        if (doubleEarning) data.SetValue<string>("GameController.doubleEarningsEnd", doubleEarningsEnd.ToString());
    }

    public void Load (SaveableData data)
    {
        //string str = "0";
        //if(data.GetValue<string>("GameController.balance", ref str))
        //{
        //    balance = new BigInteger(str);
        //}

        numMerges = 0; 
        data.GetValue<int>("GameController.numMerges", ref numMerges);

        bool doubleEarning = false;
        data.GetValue<bool>("GameController.doubleEarnings", ref doubleEarning);
        this.doubleEarning = doubleEarning;

        if (doubleEarning)
        {
            string str = "";
            data.GetValue<string>("GameController.doubleEarningsEnd", ref str);
            if(!System.DateTime.TryParse(str, out doubleEarningsEnd))
            {
                doubleEarning = false;
            }
        }

        if (doubleEarning)
            imgDoubleEarnings.sprite = sDEOn;
        else
            imgDoubleEarnings.sprite = sDOOff;
    }

    private void OnApplicationQuit()
    {
        AboutExit();
    }

    private void OnApplicationPause(bool pause)
    {
        if (!started) return;

        if (pause)
        {
            AboutExit();
        }
        else
        {
            CheckOfflineEarnings();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            AboutExit();
        }
        else
        {
            CheckOfflineEarnings();
        }
    }

    private void AboutExit()
    {
        string unpaid = "0";
        Saver.Instance().GetValue<string> ("OfflineEarnings", ref unpaid);
        BigInteger unpaidEarnings = new BigInteger(unpaid);
        unpaidEarnings += offlineEarnings;
        //Debug.Log("about exit " + offlineEarnings + " " + unpaidEarnings);
        offlineEarnings = 0;
        Saver.Instance().SetValue<string>("OfflineEarnings", unpaidEarnings.ToString());
        OfflineTimeTracker.Instance().NewStamp();

        Saver.Instance().Save();
    }

    private BigInteger offlineEarnings = new BigInteger();
    private int CheckOfflineEarnings()
    {
        //Debug.Log("check " + offlineEarnings);
        //Debug.Log("is doubling " + doubleEarning);

        float deSeconds = 0;
        if (doubleEarning && doubleEarningsEnd < System.DateTime.Now) {
            var s = System.DateTime.Now - doubleEarningsEnd;
            deSeconds = (float)s.TotalSeconds;
            doubleEarning = false;
            imgDoubleEarnings.sprite = sDOOff;
        }

        RecalculateEarnings();

        float seconds = OfflineTimeTracker.Instance().GetSeconds();
        float maxSeconds = clubWorkingTime * 60 * 60;
        if (seconds > maxSeconds) seconds = maxSeconds;
        if (deSeconds > maxSeconds) deSeconds = maxSeconds;
        string unpaid = "0";
        Saver.Instance().GetValue<string>("OfflineEarnings", ref unpaid);
        BigInteger unpaidEarnings = new BigInteger(unpaid);
        BigInteger totalEarnings = unpaidEarnings + (int)(seconds) * earningsRate + (int)deSeconds * earningsRate;
        if (totalEarnings < 0) totalEarnings = 0;

        //Debug.Log(seconds + " (" + deSeconds + ") " + earningsRate + " " + unpaidEarnings + " " + totalEarnings);

        if (seconds > 5 && totalEarnings > 0)
        {
            offlineEarningsMenu.SetActive(true);
            btnTake.SetActive(true);
            btnX2.SetActive(true);
            Debug.Log("Earned " + totalEarnings);
            Saver.Instance().SetValue<string>("OfflineEarnings", "0");
            OfflineTimeTracker.Instance().NewStamp();
            coinsBalance.Add(totalEarnings);
            lblOfflineEarnings.text = totalEarnings.Short();// + "$";
            drop = true;
            lblAbsent.text = "Вы отсутствовали " + TimeToString(new System.TimeSpan(0, 0, (int)seconds), 1) + "\nЗа это время мы заработали:";
        }
        else
        {
            Saver.Instance().SetValue<string>("OfflineEarnings", totalEarnings.ToString());
            OfflineTimeTracker.Instance().NewStamp();
            totalEarnings = 0;
        }

        offlineEarnings += totalEarnings;

        return totalEarnings;
    }

    private bool drop;
    void LateUpdate ()
    {
        if(drop)
        {
            lblOfflineEarnings.transform.parent.gameObject.SetActive(false);
            lblOfflineEarnings.transform.parent.gameObject.SetActive(true);
            drop = false;
        }
    }

    public bool GenerateCard ()
    {
        List<KeyValuePair<int, int>> list = new List<KeyValuePair<int, int>>();

        for (int i = 0; i < robodancer.dances.Count; i++) {
            for (int j = 0; j < robodancer.dances[i].cards.Count; j++)
            {
                if (!robodancer.dances[i].IsCardReady(j))
                {
                    list.Add(new KeyValuePair<int, int>(i, j));
                }
            }
        }

        if (list.Count <= 0) return false;

        var kvp = list[Random.Range(0, list.Count)];

        int danceID = kvp.Key;// Random.Range(0, robodancer.dances.Count);
        int cardID = kvp.Value;// Random.Range(0, robodancer.dances[danceID].cards.Count);

        if (robodancer.ReadyCardsNum() == 0)
        {
            danceID = 0;
            cardID = 0;
        }

        Debug.Log("Dance: " + danceID + " card: " + cardID);

        for (int i = 0; i < robodancer.dances.Count; i++) robodancer.dances[i].CheckCard(robodancer.dances[danceID].danceID, cardID);

        return true;
    }

    public void GoToScreen (int i)
    {
        cam.GoTo (i);
    }

    private bool waitingForAdEnd;
    public void DoubleOfflineEarnings()
    {
        waitingForAdEnd = true;
        //AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            if (!waitingForAdEnd) return;
            waitingForAdEnd = false;
            //if (!b) return;

            //adShowed = true;
            offlineEarnings *= 2;
            TakeOfflineEarnings();
        }
        //);
    }

    public void TakeOfflineEarnings()
    {
        if (coroutineRunning) return;

        btnTake.SetActive(false);
        btnX2.SetActive(false);

        StartCoroutine(TakeOfflineEarningsCoroutine());
    }

    private bool coroutineRunning;
    public IEnumerator TakeOfflineEarningsCoroutine()
    {
        coroutineRunning = true;

        offlineEarningsMenu.SetActive(false);

        float time = offlineEarnings / 50f;

        time = Mathf.Clamp(time, 0.1f, 0.5f);

        float rate = offlineEarnings / time;

        int added = 0;

        GameObject lastGenerated = null;

        int siblingIndex = flyingCoin.transform.GetSiblingIndex() + 1;

        while (time > 0)
        {
            int toAdd = (int)(rate * Time.unscaledDeltaTime);
            if (toAdd < 1) toAdd = 1;
            if (toAdd + added > offlineEarnings) toAdd = offlineEarnings - added;

            coinsBalance.Add(toAdd);

            added += toAdd;

            lblOfflineEarnings.text = "" + (offlineEarnings - added).Short();
            lblOfflineEarnings.gameObject.SetActive(false);
            lblOfflineEarnings.gameObject.SetActive(true);

            time -= Time.unscaledDeltaTime;

            for (int i = 0; i < 2; i++)
            {
                FlyingCoin coin = (FlyingCoin)Instantiate(flyingCoin);
                coin.gameObject.SetActive(true);
                coin.transform.SetParent(flyingCoin.transform.parent);
                coin.transform.localScale = Vector3.one;
                coin.transform.position = flyingCoin.transform.position;
                coin.transform.SetSiblingIndex(siblingIndex);
                lastGenerated = coin.gameObject;
            }

            yield return null;
        }

        if (added < offlineEarnings) coinsBalance.Add((offlineEarnings - added));

        lblOfflineEarnings.text = "0";
        lblOfflineEarnings.gameObject.SetActive(false);
        lblOfflineEarnings.gameObject.SetActive(true);

        offlineEarnings = 0;

        while (lastGenerated != null) yield return null;

        //offlineEarningsMenu.SetActive(false);

        coroutineRunning = false;

        Debug.Log("Finished");
    }

    public void AnimateFlyingCoins(FlyingCoin prefab, int num, float time, System.Action<int> onAdded, System.Action onFinish)
    {
        StartCoroutine(AnimateFlyingCoinsCoroutine(prefab, num, time, onAdded, onFinish));
    }

    private IEnumerator AnimateFlyingCoinsCoroutine (FlyingCoin prefab, int num, float time, System.Action<int> onAdded, System.Action onFinish)
    {
        float rate = num / time;
        float deltaTime = time / num;

        int added = 0;

        GameObject lastGenerated = null;

        int siblingIndex = prefab.transform.GetSiblingIndex() + 1;

        float toAdd = 0;

        for(int i = 0; i < num; i++)
        {
            FlyingCoin coin = (FlyingCoin)Instantiate(prefab);
            coin.gameObject.SetActive(true);
            coin.transform.SetParent(prefab.transform.parent);
            coin.transform.localScale = Vector3.one;
            coin.transform.position = prefab.transform.position;
            (coin.transform as RectTransform).sizeDelta = (prefab.transform as RectTransform).sizeDelta;
            coin.transform.SetSiblingIndex(siblingIndex);
            lastGenerated = coin.gameObject;

            float t = deltaTime;
            while(t > 0)
            {
                t -= Time.unscaledDeltaTime;
                yield return null;
            }

            toAdd += (rate * deltaTime);
            if ((int)toAdd + added > num) toAdd = num - added;

            added += (int)toAdd;

            if (onAdded != null) onAdded((int)toAdd);

            toAdd -= (int)toAdd;
        }

        if (added < num) if (onAdded != null) onAdded (num - added);

        while (lastGenerated != null) yield return null;

        if (onFinish != null) onFinish();
    }

    public GameObject pnlDoubleEarnings;
    public List<Image> knobs;
    public Sprite sKnobOn;
    public Sprite sKnobOff;
    public void ShowDoubleEarnings (bool b)
    {
        Debug.Log("Here");
        pnlDoubleEarnings.SetActive(b);
    }

    public Image imgDoubleEarnings;
    public Sprite sDEOn;
    public Sprite sDOOff;
    private System.DateTime doubleEarningsEnd;
    public bool doubleEarning { get; private set; }
    public List<Text> deTimers;
    private bool waitingForAd;
    public void DoubleEarnings ()
    {
        waitingForAd = true;
        AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) =>
        {
            if (!waitingForAd) return;
            waitingForAd = false;

            if (!b) return;

            if (doubleEarningsEnd == null || doubleEarningsEnd < System.DateTime.Now)
            {
                doubleEarningsEnd = System.DateTime.Now;
            }

            doubleEarningsEnd = doubleEarningsEnd.AddMinutes(30);

            doubleEarning = true;

            imgDoubleEarnings.sprite = sDEOn;

            Debug.Log("On DE");

            RecalculateEarnings();
        });
    }

    public string TimeToString(System.TimeSpan time, int format = 0)
    {
        int d = time.Days;
        int h = time.Hours;
        int m = time.Minutes;
        int s = time.Seconds;

        if (format == 0)
        {
            //string str_d = "" + d;
            //if (d < 10) str_d = "0" + d;

            string str_h = "" + h;
            if (h < 10) str_h = "0" + h;

            string str_m = "" + m;
            if (m < 10) str_m = "0" + m;

            //string str_s = "" + s;
            //if (s < 10) str_s = "0" + s;

            return str_h + ":" + str_m + "";
        }
        else
        {
            //string str_d = "" + d;
            //if (d < 10) str_d = "0" + d;

            string str_h = "" + h;
            if (h < 10) str_h = "0" + h;

            string str_m = "" + m;
            if (m < 10) str_m = "0" + m;

            //string str_s = "" + s;
            //if (s < 10) str_s = "0" + s;

            return str_h + "ч " + str_m + "м";
        }
    }

    //public SecondsToString ()
    //{

    //}

    public static Vector3 WorldToUISpace(Canvas canvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, screenPos, canvas.worldCamera, out movePos);
        //Convert the local point to world point
        return canvas.transform.TransformPoint(movePos);
    }

    //public int GetLocalID (int type, DanceFloor floor)
    //{
    //    DanceFloor fl = floors[0];

    //    while(fl != null && fl != floor)
    //    {
    //        type -= fl.prefabs.Count;
    //        fl = fl.nextFloor;
    //    }

    //    return type;
    //}

    //public int GetLocalID(int type, int floor)
    //{
    //    int fl = 0;

    //    while (fl < floors.Count && fl != floor)
    //    {
    //        type -= floors[fl].prefabs.Count;
    //        fl = fl++;
    //    }

    //    return type;
    //}

    public void RemoveAllDancers ()
    {
        for(int i = 0; i < floors.Count; i++)
        {
            floors[i].RemoveAllDancers();
        }

        Saver.Instance().Save();
    }

    public Elevator elevatorPrefab;
    public ProgressBar spawnBar;
    public int typeToSpawn;
    public float spawnTime;
    public float spawnTimer;
    public Dancer Spawn(int dancerID, bool ignoreCapacity = false)
    {
        int t = dancerID;

        int fl = 0;

        //Debug.Log("try to spawn " + t);

        while (fl < floors.Count && t >= floors[fl].prefabs.Count)
        {
            t -= floors[fl].prefabs.Count;
            fl++;
        }

        //Debug.Log("Found " + fl + " " + t);

        if (fl < floors.Count)
        {
            //Debug.Log(ignoreCapacity + " " + floors[fl].DancersCount());
            if (ignoreCapacity || floors[fl].DancersCount() < floors[fl].capacity)
            {
                var dancer = floors[fl].Spawn(t);
                dancer.type = dancerID;
                return dancer;
            }
        }

        return null;
    }

    private void Spawning ()
    {
        if (typeToSpawn < 0) return;

        int t = typeToSpawn;

        int fl = 0;

        while (fl < floors.Count && t >= floors[fl].prefabs.Count)
        {
            t -= floors[fl].prefabs.Count;
            fl++;
        }

        if (fl > floors.Count) return;

        if (floors[fl].DancersCount() >= floors[fl].capacity) return;

        if (spawnTime > 0)
        {
            spawnTimer += Time.deltaTime;

            if (spawnTimer >= spawnTime)
            {
                spawnTimer = 0;

                Dancer dancer = Spawn(typeToSpawn);

                //Debug.Log("Spawn " + typeToSpawn + " " + dancer, dancer);

                dancer.gameObject.SetActive(false);
                Elevator elevator = Instantiate(elevatorPrefab);
                elevator.transform.SetParent(floors[fl].dancersOrigin);
                elevator.transform.localPosition = dancer.transform.localPosition;
                //Vector3 scale = elevator.transform.parent.lossyScale;
                //scale.x = 1f / scale.x;
                //scale.y = 1f / scale.y;
                //scale.z = 1f / scale.z;
                //scale.Scale(elevatorPrefab.transform.lossyScale);
                elevator.transform.localScale = elevatorPrefab.transform.localScale;
                elevator.dancer = dancer;
                dancer.elevator = elevator;

                DancerSpawned();
            }

            if (spawnBar != null) spawnBar.SetValue(spawnTimer / spawnTime);
        }
    }
}
