﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour {

    public Vector2 params1;
    public Vector2 params2;

	// Use this for initialization
	void Start () {

        float ar = (float)Screen.width / Screen.height;
        float k = (ar - params1.x) / (params2.x - params1.x);
        float s = Mathf.Lerp(params1.y, params2.y, k);

        transform.localScale = s * Vector3.one;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
