﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

    private Vector3 originalScale;

    // Use this for initialization
    void Start () {

        var button = GetComponent<Button>();
        if(button != null)
        {
            button.transition = Selectable.Transition.None;
        }

        originalScale = transform.localScale;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        transform.localScale = 0.85f * originalScale;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = 1f * originalScale;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = 1f * originalScale;
    }
}
