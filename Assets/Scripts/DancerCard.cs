﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DancerCard : MonoBehaviour, ISignable {

    public Image portrait;

    public Text lblName;

    public Text lblRate;

    public Text lblNumBuyed;

    public Button btnBuyWMoney;

    public Button btnBuyWCrystls;

    public Button btnBuyWAds;

    public Text lblCoinPrice;

    public Text lblCrystalsPrice;

    public int dancerID;

    [HideInInspector]
    public DancersShop shop;

    public Transform signPos;

	// Use this for initialization
	void Start () {
		
	}
	
	public void BuyWCoins ()
    {
        if (shop != null) shop.Buy(dancerID, Wallet.CurrencyType.coins);
    }

    public void BuyWCrystals ()
    {
        Debug.Log("Buy w crystals " + dancerID);
        if (shop != null) shop.Buy(dancerID, Wallet.CurrencyType.crystals);
    }

    public void BuyWAds ()
    {
        if (shop != null) shop.Buy(dancerID, Wallet.CurrencyType.ads);
    }

    public void SetPaymentMethods (bool coins, bool crystals, bool ads)
    {
        //Debug.Log("Set " + coins + " " + crystals + " " + ads, this);

        btnBuyWMoney.gameObject.SetActive(coins);
        btnBuyWCrystls.gameObject.SetActive(crystals);
        btnBuyWAds.gameObject.SetActive(ads);
    }

    public Transform GetSignPlace()
    {
        return signPos;
    }

    public bool Overlayed ()
    {
        return false;
    }
}
