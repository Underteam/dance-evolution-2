// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "MaxG/Morphing"
{
	Properties
	{
		//[PerRendererData] 
		_MainTex ("From Texture", 2D) = "white" {}
		_ToTex ("To Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_T ("Time", Float) = 0

		_Direction ("Direction", Vector) = (1, 0, 0, 0)
		_UV1 ("UV", Vector) = (0, 0, 1, 1)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			float _T;
			float4 _UV1;
			float4 _Direction;

			int _PointsCount = 0;
			float _Points[1000];//������ ��������

			fixed4 frag(v2f IN) : SV_Target
			{
				half2 uv = 2 * IN.texcoord;//������ ���������� ������� � ������� ��������� from
				//uv.x = _UV1.x + (_UV1.z - _UV1.x) * uv.x;
				//uv.y = _UV1.y + (_UV1.w - _UV1.y) * uv.y;

				float2 dir;
				dir.x = _Direction.x;
				dir.y = _Direction.y;

				float m = sqrt(dir.x * dir.x + dir.y * dir.y);
				dir.x = dir.x / m;
				dir.y = dir.y / m;

				float2 perp;//������������� � ����������� ��������
				perp.x = -dir.y;
				perp.y = dir.x;

				float min = (perp.x * 1 + perp.y * 0);//�������� ������ �������� �� �������������
				float max = (perp.x * 0 + perp.y * 1);
				float dot = (perp.x * uv.x + perp.y * uv.y);//��� ����������� �������� �������� ��� �������� �������

				//���� ������� ������� ��������� �� ������� ������ ��������
				//clip (max - dot);
				//clip (dot - min);

				int ind = 999 * (max - dot) / (max - min);
				ind = clamp(ind, 0, 999);

				float vx1 = dir.x * _Points[ind];//�������� �������� �����
				float vy1 = dir.y * _Points[ind];//�������� �������� �����

				float vx2 = 0.0;// * _T * vx1;//�������� ������ �����
				float vy2 = 0.0;// * _T * vy1;//�������� ������ �����

				vx1 = vx1 + dir.x * _T * _T * _T;
				vy1 = vy1 + dir.y * _T * _T * _T;

				half2 uv2;//uv ���������� res ��������, � ��� ����� ���� ��� ��������

				float dist = uv.x * dir.x + uv.y * dir.y;//�� ������� ����� ��������� �������
				dist = clamp(dist, 0, 1);

				float vx = dist * vx1 + (1-dist) * vx2;
				float vy = dist * vy1 + (1-dist) * vy2;

				float2 v;
				m = sqrt(vx * vx + vy * vy);
				m = 1 + clamp(m-1, 0, m);
				v.x = vx / m;
				v.y = vy / m;
				
				uv.x = uv.x - v.x * _T;
				uv.y = uv.y - v.y * _T;

				clip(uv.x);
				clip(1 - uv.x);
				clip(uv.y);
				clip(1 - uv.y);

				fixed4 c = tex2D(_MainTex, uv) * IN.color;

				//c.r = 1;
				//c.g = 1;
				//c.b = 1;
				//c.a = max(uv.x, uv.y);
				//c.a = clamp(c.a, 0, 1);

				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
} 